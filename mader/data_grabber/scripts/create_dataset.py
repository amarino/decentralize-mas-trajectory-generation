#!/usr/bin/env python3

import struct

import rospy
from std_msgs.msg import String
import std_msgs.msg as std_msgs
from visualization_msgs.msg import MarkerArray
from rosgraph_msgs.msg import Clock
import sensor_msgs.msg as sensor_msgs
from sensor_msgs import point_cloud2
from sensor_msgs.msg import PointCloud2, PointField, Image
from std_msgs.msg import Header
from snapstack_msgs.msg import State, Goal
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import PoseStamped, PointStamped, Point, Vector3
from mader_msgs.msg import *
import cv2
from cv_bridge import CvBridge
import torch
import math

from threading import Thread
from time import sleep, perf_counter
import time
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.transform import Rotation
from scipy.interpolate import BSpline
from scipy import interpolate
import scipy.optimize as opt
from threading import RLock
import rospkg
import os.path
from os import makedirs

rospack = rospkg.RosPack()
# Device configuration
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class CreateDataset:
    def __init__(self):
        rospy.init_node("dataset_creation", anonymous=True)
        # self.sub_traj = rospy.Subscriber('/SQ01s/mader/Bspline', Spline, self.traj_cb)
        # self.sub_goal = rospy.Subscriber('/SQ01s/mader/point_G', PointStamped, self.goal_cb)
        self.sub_state = rospy.Subscriber("/SQ01s/state", State, self.state_cb)
        self.sub_obstacles_dyn = rospy.Subscriber("/shapes_dynamic_mesh", MarkerArray, self.dynamic_obst_cb)
        self.sub_onstacles_static = rospy.Subscriber("/shapes_static_mesh", MarkerArray, self.static_obst_cb)
        self.pub_clock = rospy.Publisher("/clock", Clock, queue_size=6)
        self.pub_point_cloud = rospy.Publisher("point_cloud2", PointCloud2, queue_size=6)
        self.goal_pub = rospy.Publisher("/SQ01s/term_goal", PoseStamped, queue_size=6)
        self.pub_image = rospy.Publisher("image", Image, queue_size=6)
        self.pub_goal_short = rospy.Publisher("/SQ01s/mader/point_G_old", PointStamped, queue_size=1)

        self.data_path = os.path.dirname(rospack.get_path("mader"))
        train_sas = "/trained_models/sas_traj_model.pt"

        self.model_sas = torch.jit.load(self.data_path + train_sas)
        self.model_sas.eval()

        self.model_sas.to(device)

        self.goal_2_send = Goal()
        self.spline_2_follow = []
        self.tf = []
        self.start_tf = []

        self.mode = "GOAL_REACHED"

        self.pub_goal = rospy.Publisher("/SQ01s/goal", Goal, queue_size=1)
        # self.sub_goal = rospy.Subscriber("/SQ01s/mader/goal_old", Goal, self.goal_old)

        # self.data_path = self.data_path + "/dataset/" + time.strftime("%Y-%m-%d_%H-%M-%S/")
        self.data_num = 1
        makedirs(self.data_path, exist_ok=True)
        self.point_cloud = np.array([])

        self.v_max = rospy.get_param("/SQ01s/mader/v_max")
        self.ra = rospy.get_param("/SQ01s/mader/Ra")

        self.dynamic_points = None
        self.static_points = None

        self.bridge = CvBridge()

        self.state = np.array([0, 0, 0])
        self.vel = None
        self.acc = None
        self.state_old = np.array([0.0, 0.0, 0.0])
        self.same_state = 0
        self.quat = np.array([0.0, 0.0, 0.0, 0.0])
        self.R = Rotation.from_quat(np.array([0.0, 0.0, 0.0, 1.0]))

        self.time_clock = Clock()
        self.time_clock.clock = rospy.Time.from_sec(time.time())
        self.dynamic_obst_xyz = []
        self.static_obst_xyz = []
        self.goal = np.array([0.0, 0.0, 1.0])
        self.goal_short = np.array([0.0, 0.0, 0.0])
        self.timer = rospy.Timer(rospy.Duration(0.04), self.point_cloud_cb)
        # self.timer_goal = rospy.Timer(rospy.Duration(0.01), self.goal_new_tensor)

    def from_vector3(self, msg):
        return np.array([msg.x, msg.y, msg.z])

    def goal_cb(self, msg):
        self.goal_short = np.array([msg.point.x, msg.point.y, msg.point.z])

    def goal_old(self, msg):
        self.goal_2_send = msg

    def state_cb(self, msg):
        self.state = np.array([msg.pos.x, msg.pos.y, msg.pos.z])
        self.quat = np.array([msg.quat.x, msg.quat.y, msg.quat.z, msg.quat.w])
        self.vel = np.array([msg.vel.x, msg.vel.y, msg.vel.z])
        self.acc = np.array([msg.abias.x, msg.abias.y, msg.abias.z])
        self.R = Rotation.from_quat(self.quat)

    def dynamic_obst_cb(self, msg):
        self.dynamic_points = msg.markers

    def static_obst_cb(self, msg):
        self.static_points = msg.markers

    def goal_new_tensor(self, timer):
        t = rospy.Time.now().to_sec()
        _, _, yaw = euler_from_quaternion(self.quat)

        if self.mode == "YAWING":
            desired_yaw = np.arctan2(self.goal_short[1] - self.state[1], self.goal_short[0] - self.state[0])
            diff = desired_yaw - yaw
            diff = max(min(diff, 0.01 * 2.5), -0.01 * 2.5)

            diff = math.fmod(diff + np.pi, 2 * np.pi)
            if diff < 0:
                diff += 2 * np.pi
            diff -= np.pi
            if np.abs(diff) < 0.02:
                self.mode = "TRAVELLING"

            dyaw = diff / (np.abs(diff) + 1e-12) * 2.5

            yaw = yaw + dyaw * 0.01

            print(f"yaw {yaw}")
            print(f"diff {diff}")
            print(f"dyaw {dyaw}")

            self.goal_2_send.dpsi = dyaw
            self.goal_2_send.psi = yaw

        self.goal_2_send.header.stamp = rospy.Time.now()
        self.goal_2_send.header.frame_id = "world"

        self.goal_2_send.power = True

        if self.tf and self.mode == "TRAVELLING":
            for _ in range(len(self.tf)):
                if len(self.start_tf) > 1:
                    if self.tf[0] < t or self.start_tf[1] < t:
                        self.start_tf.pop(0)
                        self.tf.pop(0)
                        self.spline_2_follow.pop(0)

            if self.tf[0] <= t:
                t = self.tf[0]

            if self.start_tf[0] < t:
                t = t - self.start_tf[0]
                T, dT, ddT, dddT = self.eval_T(t)
                trajx, trajy, trajz = (
                    self.spline_2_follow[0][0],
                    self.spline_2_follow[0][1],
                    self.spline_2_follow[0][2],
                )
                x = np.dot(trajx, T)
                dx = np.dot(trajx, dT)
                ddx = np.dot(trajx, ddT)
                dddx = np.dot(trajx, dddT)
                # print(f"x:{x}")
                # print(f"dx:{dx}")
                # print(f"ddx:{ddx}")
                # print(f"dddx:{dddx}")
                y = np.dot(trajy, T)
                dy = np.dot(trajy, dT)
                ddy = np.dot(trajy, ddT)
                dddy = np.dot(trajy, dddT)
                # print(f"y:{y}")
                # print(f"dy:{dy}")
                # print(f"ddy:{ddy}")
                # print(f"dddy:{dddy}")
                z = np.dot(trajz, T)
                dz = np.dot(trajz, dT)
                ddz = np.dot(trajz, ddT)
                dddz = np.dot(trajz, dddT)
                # print(f"z:{z}")
                # print(f"dz:{dz}")
                # print(f"ddz:{ddz}")
                # print(f"dddz:{dddz}")

                self.goal_2_send.p = Point(x, y, z)

                self.goal_2_send.v = Vector3(dx, dy, dz)
                self.goal_2_send.a = Vector3(ddx, ddy, ddz)
                self.goal_2_send.j = Vector3(dddx, dddy, dddz)

                self.goal_2_send.header.stamp = rospy.Time.now()
        elif self.mode == "GOAL-SEEN":
            error = self.goal_short - self.state
            en = np.linalg.norm(error)
            if en > 0.15:
                error = error / en * 0.01
            self.goal_2_send.p = Point(self.state[0] + error[0], self.state[1] + error[1], self.state[2] + error[2])
            error = 10 * error
            self.goal_2_send.v = Vector3(error[0], error[1], error[2])
            error = 2 * error
            self.goal_2_send.a = Vector3(error[0], error[1], error[2])
        else:
            self.goal_2_send.p = Point(self.state[0], self.state[1], self.state[2])

        self.pub_goal.publish(self.goal_2_send)

    def traj_cb(self, msg):
        knots = msg.knots
        ctrls = msg.ctrls
        dim = msg.dim_ctrls

        if self.point_cloud.size != 0 and dim > 0:
            t0 = rospy.Time.now().to_sec()

            # rospy.loginfo(f"coeff_x {ctrls[:dim]}")
            # rospy.loginfo(f"coeff_y {ctrls[dim:2*dim]}")
            # rospy.loginfo(f"coeff_z {ctrls[2*dim:3*dim]}")
            # rospy.loginfo(f"knots {knots}")
            lim = 5

            splinex = BSpline(knots, ctrls[:dim], k=3, extrapolate=False)
            spliney = BSpline(knots, ctrls[dim : 2 * dim], k=3, extrapolate=False)
            splinez = BSpline(knots, ctrls[2 * dim : 3 * dim], k=3, extrapolate=False)

            start_time = np.array([])
            if t0 > knots[0]:
                t = np.linspace(t0, knots[-1], 7)
                start_time = np.array([knots[-1] - t0])
            else:
                t = np.linspace(knots[0], knots[-1], 7)
                start_time = np.array([knots[-1] - knots[0]])

            sx = splinex(t)
            sy = spliney(t)
            sz = splinez(t)

            # rospy.loginfo(f"vx {splinex.derivative(1)(t)}")
            # rospy.loginfo(f"vy {spliney.derivative(1)(t)}")
            # rospy.loginfo(f"vz {splinez.derivative(1)(t)}")

            if np.isnan(sx).any() == False or np.isnan(sy).any() == False or np.isnan(sz).any() == False:
                traj = np.concatenate([sx, sy, sz, start_time], axis=0)
                goal = self.goal_short - self.state
                goal = goal / lim
                vel = self.vel / self.v_max
                """
                rospy.loginfo(f"time {traj}")
                rospy.loginfo(f"state {self.state}")
                
                rospy.loginfo(f"goal {goal}")
                rospy.loginfo(f"vel {vel}")
                rospy.loginfo(f"s_x {sx}")
                rospy.loginfo(f"s_y {sy}")
                rospy.loginfo(f"s_z {sz}")

                """
                tx, cx, kx = interpolate.splrep(t, sx, s=0, k=4)
                ty, cy, ky = interpolate.splrep(t, sy, s=0, k=4)
                tz, cz, kz = interpolate.splrep(t, sz, s=0, k=4)

                splinex = interpolate.BSpline(tx, cx, kx, extrapolate=False)
                spliney = interpolate.BSpline(ty, cy, ky, extrapolate=False)
                splinez = interpolate.BSpline(tz, cz, kz, extrapolate=False)
                """
                rospy.loginfo(f"cx {cx}")
                rospy.loginfo(f"cy {cy}")
                rospy.loginfo(f"cz {cz}")

                rospy.loginfo(f"x_again {splinex(t)}")
                rospy.loginfo(f"y_again {spliney(t)}")
                rospy.loginfo(f"z_again {splinez(t)}")

                rospy.loginfo(f"vx_again {splinex.derivative(1)(t)}")
                rospy.loginfo(f"vy_again {spliney.derivative(1)(t)}")
                rospy.loginfo(f"vz_again {splinez.derivative(1)(t)}")
                """
                rospy.loginfo("\n")
                """
                data_dir = "data" + str(self.data_num) + "/"
                makedirs(self.data_path + data_dir + 'Input', exist_ok=True)
                makedirs(self.data_path + data_dir + 'Label', exist_ok=True)

                with open(self.data_path + data_dir + 'Input/point_cloud.npy', 'wb') as f:
                    np.save(f, self.point_cloud)

                with open(self.data_path + data_dir + 'Input/vel.npy', 'wb') as f:
                    np.save(f, vel)

                with open(self.data_path + data_dir + 'Input/quat.npy', 'wb') as f:
                    np.save(f, self.quat)

                with open(self.data_path + data_dir + 'Input/goal.npy', 'wb') as f:
                    np.save(f, goal)

                with open(self.data_path + data_dir + 'Label/traj.npy', 'wb') as f:
                    np.save(f, traj)

                self.data_num += 1
            """

    def compute_pt(self, p, lim):
        # p = np.round(p, decimals=1)
        x = float(p[0])
        y = float(p[1])
        z = float(p[2])

        r = int(abs(x / lim) * 255.0)
        g = int(abs(y / lim) * 255.0)
        b = int(abs(z / lim) * 255.0)
        a = 255
        rgb = struct.unpack("I", struct.pack("BBBB", b, g, r, a))[0]
        pt = [x, y, z, rgb]
        return pt

    def eval_T(self, i):
        T = np.array([1, i, i**2, i**3, i**4, i**5])
        dT = np.array([0.0, 1, 2 * i, 3 * i**2, 4 * i**3, 5 * i**4])
        ddT = np.array([0.0, 0.0, 2, 6 * i, 12 * i**2, 20 * i**3])
        dddT = np.array([0.0, 0.0, 0, 6, 24 * i, 60 * i**2])
        return T, dT, ddT, dddT

    def obj(self, x, p, t, eval_T):
        out = 0.0
        for i in range(len(p) - 1):
            T, _, _, _ = eval_T(t[i])
            out = out + (p[i] - np.dot(x, T)) ** 2
        T, _, _, _ = eval_T(t[-1])
        out = out + 10 * (p[-1] - np.dot(x, T)) ** 2
        return out

    def traj_optimization(self, p, vel, acc, t):
        T, dT, ddT, _ = self.eval_T(0)
        Tf, dTf, ddTf, _ = self.eval_T(t[-1])

        def cons(x):
            c = []
            for i in range(len(t)):
                c.append(np.dot(x, self.eval_T(t[i])[1]))
                c.append(np.dot(x, self.eval_T(t[i])[2]))
            c.append(p[0] - np.dot(x, T))
            c.append(np.dot(x, dT) - vel)
            c.append(np.dot(x, ddT) - acc)
            # c.append(point_x[-1] - np.dot(x, Tf))
            c.append(np.dot(x, dTf))
            c.append(np.dot(x, ddTf))
            return np.array(c)

        lb = [-4.5, -30] * len(t) + [0] * 5
        ub = [4.5, 30] * len(t) + [0] * 5
        x0 = np.ones(6)
        results = opt.minimize(
            self.obj,
            x0,
            args=(p, t, self.eval_T),
            constraints=opt.NonlinearConstraint(cons, lb=np.array(lb), ub=np.array(ub)),
            method="trust-constr",
        )
        rospy.loginfo(f"opt_message {results.message}")
        return results.x

    def point_cloud_cb(self, timer):
        dist_goal = np.linalg.norm(self.state - self.goal)
        if np.linalg.norm(self.state - self.state_old) < 0.01:
            self.same_state += 1
        else:
            self.same_state = 0

        self.state_old = self.state

        if (dist_goal < 0.16) or self.same_state > 50:
            print("sending goal")
            self.same_state = 0
            self.goal = np.array([50, 10, 4]) * np.random.rand(3) + np.array([1, -5, 1])
            self.mode = "YAWING"

        msg = PoseStamped()
        msg.pose.position.x = self.goal[0]
        msg.pose.position.y = self.goal[1]
        msg.pose.position.z = self.goal[2]

        self.goal_pub.publish(msg)
        distA2TermGoal = np.linalg.norm(self.goal - self.state) + 1e-4
        ra = min((distA2TermGoal), self.ra)
        self.goal_short = self.state + ra * (self.goal - self.state) / distA2TermGoal

        if self.dynamic_points and self.static_points:
            ### pub Point Cloud
            self.obst_points = self.dynamic_points + self.static_points
            points = []
            lim = 5
            cv_image = np.zeros([51, 51, 1])
            positions_t = np.array([])
            scale_t = np.array([])
            for p in self.obst_points:
                scale = p.scale
                # print(p.pose.orientation.w)
                p = p.pose.position
                p_array = self.from_vector3(p) - self.state
                s_min = np.linalg.norm(p_array - np.array([scale.x / 2, scale.y / 2, scale.z / 2]))
                s_max = np.linalg.norm(p_array + np.array([scale.x / 2, scale.y / 2, scale.z / 2]))
                if s_min < lim or s_max < lim:
                    X, Y, Z = np.mgrid[
                        (p_array[0] - (scale.x) / 2) : (p_array[0] + (scale.x) / 2) : 0.1,
                        (p_array[1] - (scale.y) / 2) : (p_array[1] + (scale.y) / 2) : 0.1,
                        (p_array[2] - (scale.z) / 2) : (p_array[2] + (scale.z) / 2) : 0.1,
                    ]
                    positions = np.transpose(np.vstack([X.ravel(), Y.ravel(), Z.ravel()]))
                    positions = np.round(positions, decimals=3)
                    scale = np.linalg.norm(positions, axis=1, keepdims=True)
                    positions = positions / scale
                    for i, s in enumerate(scale):
                        if s < lim:
                            positions[i] = self.R.apply(positions[i], inverse=True)
                            if positions_t.size:
                                min_p = np.abs(np.array(np.dot(positions_t, positions[i])) - 1)
                                equals = min_p < 0.0004
                                idx_min = min_p.argmin()
                                idx = np.where(equals)[0]
                                if idx.size == 0:
                                    positions_t = np.concatenate([positions_t, positions[i : i + 1]], axis=0)
                                    scale_t = np.concatenate([scale_t, scale[i]], axis=0)
                                else:
                                    scale_t[idx_min] = min(s, scale_t[idx_min])
                            else:
                                positions_t = positions[i : i + 1]
                                scale_t = scale[i]
            if positions_t.size:
                self.point_cloud = positions_t * np.reshape(scale_t, (scale_t.shape[0], 1)) / lim

                for i, p in enumerate(positions_t):
                    pt_temp = p * scale_t[i] / lim
                    pt_temp = self.compute_pt(pt_temp, lim)

                    x_im = (pt_temp[0] + 1) * lim * 10 / 2
                    z_im = (pt_temp[2] + 1) * lim * 10 / 2
                    s = (scale_t[i] - 0.01) / (lim - 0.01)
                    cv_image[int(round(x_im)), int(round(z_im)), 0] = 255 - (s * 255)
                    points.append(pt_temp)

                fields = [
                    PointField("x", 0, PointField.FLOAT32, 1),
                    PointField("y", 4, PointField.FLOAT32, 1),
                    PointField("z", 8, PointField.FLOAT32, 1),
                    # PointField('rgb', 12, PointField.UINT32, 1),
                    PointField("rgba", 12, PointField.UINT32, 1),
                ]
                header = Header()
                header.frame_id = "world"
                pc2_msg = point_cloud2.create_cloud(header, fields, points)
                self.pub_point_cloud.publish(pc2_msg)

        gs = PointStamped()
        gs.header.frame_id = "world"
        gs.point.x = self.goal_short[0]
        gs.point.y = self.goal_short[1]
        gs.point.z = self.goal_short[2]
        self.pub_goal_short.publish(gs)

        """
        if np.linalg.norm(self.goal_short - self.state) < 0.5:
            self.mode = "GOAL-SEEN"
        
        if self.vel is not None and np.any(self.goal_short != np.zeros(3)) and self.mode == "TRAVELLING":
            lim = 5

            print(f"goal {self.goal_short}")

            goal = self.goal_short - self.state
            goal = goal / lim
            vel = self.vel / self.v_max

            # normalize
            goal = (goal - 0.0036711418) / 0.5209052
            vel = (vel - 0.19334013) / 0.51882267

            point_cloud = (
                torch.Tensor(self.point_cloud)
                .to(device)
                .reshape(1, self.point_cloud.shape[0], self.point_cloud.shape[1])
            )
            goal = torch.Tensor(goal).to(device).reshape(1, self.goal.shape[0])
            vel = torch.Tensor(vel).to(device).reshape(1, self.vel.shape[0])
            quat = torch.Tensor(self.quat).to(device).reshape(1, self.quat.shape[0])

            # print(f"goal {goal}")
            # print(f"vel {vel}")
            with torch.no_grad():
                out, _, _ = self.model_sas(point_cloud, goal, quat, vel)

            t0 = rospy.Time.now().to_sec()
            out = out[0, :].cpu().numpy()
            tf = out[-1] * 1.8267245

            t = np.linspace(0, tf, 8)

            x = np.concatenate((np.array([self.state[0]]), (out[:7] * lim + self.state[0])))
            y = np.concatenate((np.array([self.state[1]]), (out[7:14] * lim + self.state[1])))
            z = np.concatenate((np.array([self.state[2]]), (out[14:21] * lim + self.state[2])))

            # print(f"state {self.state}")
            # print(f"t {t}")
            # print(f"x {x}")
            # print(f"y {y}")
            # print(f"z {z}")

            traj_x = self.traj_optimization(x, self.vel[0], self.acc[0], t)
            traj_y = self.traj_optimization(y, self.vel[1], self.acc[1], t)
            traj_z = self.traj_optimization(z, self.vel[2], self.acc[2], t)

            print(f"trajx {traj_x}")
            print(f"trajy {traj_y}")
            print(f"trajz {traj_z}")

            self.tf.append(t[-1] + t0)
            self.start_tf.append(t[0] + t0)
            self.spline_2_follow.append([traj_x, traj_y, traj_z])
        """
        """ 
        ## pub image
        
        cv_image = cv2.GaussianBlur(cv_image, (5, 5), cv2.BORDER_DEFAULT)
        cv_image = np.rint(cv_image)
        cv_image = cv_image.astype(np.uint8)
        image_message = self.bridge.cv2_to_imgmsg(cv_image, encoding="mono8")

        self.pub_image.publish(image_message)
        """

    def pub_clock_timer(self):
        while not rospy.is_shutdown():
            self.time_clock.clock = self.time_clock.clock + rospy.Duration.from_sec(0.005)
            self.pub_clock.publish(self.time_clock)
            sleep(0.01)


if __name__ == "__main__":
    p = CreateDataset()
    thread_clock = Thread(target=p.pub_clock_timer())
    thread_clock.start()
    rospy.spin()
    thread_clock.join()
