#!/usr/bin/env python3

import struct

import rospy
from std_msgs.msg import String
import std_msgs.msg as std_msgs
from visualization_msgs.msg import MarkerArray
from rosgraph_msgs.msg import Clock
import sensor_msgs.msg as sensor_msgs
from sensor_msgs import point_cloud2
from sensor_msgs.msg import PointCloud2, PointField, Image
from std_msgs.msg import Header
from snapstack_msgs.msg import State, Goal
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import PoseStamped, PointStamped, Point, Vector3
from mader_msgs.msg import *
import cv2
from cv_bridge import CvBridge
import torch
from nav_msgs.msg import Path
import math

from threading import Thread
from time import sleep, perf_counter
import time
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.transform import Rotation
from scipy.interpolate import BSpline
from scipy import interpolate
import scipy.optimize as opt
import rospkg
import os.path
from os import makedirs
from sklearn.neighbors import radius_neighbors_graph


# from MASTrajModel import SASTrajModel

rospack = rospkg.RosPack()
# Device configuration
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

np.random.seed(3)


class CreateDataset:
    def __init__(self):
        rospy.init_node("dataset_creation", anonymous=True)
        # self.sub_traj = rospy.Subscriber("/SQ01s/mader/traj_safe", Path, self.traj_cb)
        # self.pub_traj = rospy.Publisher("/SQ01s/mader/traj_safe_shift", Path, queue_size=1)
        # self.sub_goal = rospy.Subscriber('/SQ01s/mader/point_G', PointStamped, self.goal_cb)
        self.sub_state = rospy.Subscriber("/SQ01s/state", State, self.state_cb)
        self.sub_obstacles_dyn = rospy.Subscriber("/shapes_dynamic_mesh", MarkerArray, self.dynamic_obst_cb)
        self.sub_onstacles_static = rospy.Subscriber("/shapes_static_mesh", MarkerArray, self.static_obst_cb)
        self.pub_clock = rospy.Publisher("/clock", Clock, queue_size=6)
        self.pub_point_cloud = rospy.Publisher("point_cloud2", PointCloud2, queue_size=6)
        self.goal_pub = rospy.Publisher("/SQ01s/term_goal", PoseStamped, queue_size=6)
        self.pub_image = rospy.Publisher("image", Image, queue_size=6)
        self.pub_goal_short = rospy.Publisher("/SQ01s/mader/point_G", PointStamped, queue_size=1)

        self.data_path = os.path.dirname(rospack.get_path("mader"))
        train_sas = "/trained_models/sas_traj_model_BS.pt"

        self.model_sas = torch.load(self.data_path + train_sas)
        self.model_sas.eval()
        self.model_sas.qp_enabled = True
        self.model_sas.use_cc = True
        self.model_sas.alpha = 0.05

        self.model_sas.to(device)

        self.goal_2_send = Goal()
        self.spline_2_follow = []
        self.tf = []
        self.start_tf = []

        self.mode = "GOAL_REACHED"

        self.pub_goal = rospy.Publisher("/SQ01s/goal", Goal, queue_size=1)
        # self.sub_goal = rospy.Subscriber("/SQ01s/mader/goal_old", Goal, self.goal_old)

        # self.data_path = self.data_path + "/dataset/" + time.strftime("%Y-%m-%d_%H-%M-%S/")
        # self.data_num = 1
        # makedirs(self.data_path, exist_ok=True)
        self.point_cloud = np.array([])

        self.v_max = rospy.get_param("/SQ01s/mader/v_max")
        self.a_max = rospy.get_param("/SQ01s/mader/a_max")
        self.ra = rospy.get_param("/SQ01s/mader/Ra")

        self.dynamic_points = None
        self.static_points = None

        self.bridge = CvBridge()

        self.data_save = {}
        self.state = np.array([0, 0, 0])
        self.vel = None
        self.acc = None
        self.state_old = np.array([0.0, 0.0, 0.0])
        self.same_state = 0
        self.quat = np.array([0.0, 0.0, 0.0, 1.0])
        self.R = Rotation.from_quat(np.array([0.0, 0.0, 0.0, 1.0]))

        self.time_clock = Clock()
        self.time_clock.clock = rospy.Time.from_sec(time.time())
        self.dynamic_obst_xyz = []
        self.static_obst_xyz = []
        self.goal = np.array([0.0, 0.0, 1.0])
        self.goal_short = np.array([0.0, 0.0, 0.0])
        self.timer = rospy.Timer(rospy.Duration(0.03), self.point_cloud_cb)
        self.timer_goal = rospy.Timer(rospy.Duration(0.01), self.goal_new_tensor)

    def from_vector3(self, msg):
        return np.array([msg.x, msg.y, msg.z])

    def goal_cb(self, msg):
        self.goal_short = np.array([msg.point.x, msg.point.y, msg.point.z])

    def goal_old(self, msg):
        self.goal_2_send = msg

    def state_cb(self, msg):
        self.state = np.array([msg.pos.x, msg.pos.y, msg.pos.z])
        self.quat = np.array([msg.quat.x, msg.quat.y, msg.quat.z, msg.quat.w])
        self.vel = np.array([msg.vel.x, msg.vel.y, msg.vel.z])
        self.acc = np.array([msg.abias.x, msg.abias.y, msg.abias.z])
        self.R = Rotation.from_quat(self.quat)

    def dynamic_obst_cb(self, msg):
        self.dynamic_points = msg.markers

    def static_obst_cb(self, msg):
        self.static_points = msg.markers

    def goal_new_tensor(self, timer):
        _, _, yaw = euler_from_quaternion(self.quat)

        if self.mode == "GOAL-SEEN" or self.mode == "TRAVELLING" or self.mode == "YAWING":
            desired_yaw = np.arctan2(self.goal_short[1] - self.state[1], self.goal_short[0] - self.state[0])
            diff = desired_yaw - yaw
            diff = max(min(diff, 0.01 * 2.5), -0.01 * 2.5)

            diff = math.fmod(diff + np.pi, 2 * np.pi)
            if diff < 0:
                diff += 2 * np.pi
            diff -= np.pi

            dyaw = diff / (np.abs(diff) + 1e-12) * 2.0

            yaw = yaw + dyaw * 0.01

            # print(f"yaw {yaw}")
            # print(f"diff {diff}")
            # print(f"dyaw {dyaw}")

            self.goal_2_send.dpsi = dyaw
            self.goal_2_send.psi = yaw

            if self.mode == "YAWING":
                if np.abs(diff) < 0.02:
                    self.mode = "TRAVELLING"

        t = rospy.Time.now()
        self.goal_2_send.header.stamp = rospy.Time.now()
        self.goal_2_send.header.frame_id = "world"
        t = t.to_sec()

        self.goal_2_send.power = True
        self.goal_2_send.p = Point(self.state[0], self.state[1], self.state[2])
        self.vel = np.array([0.0, 0.0, 0.0])
        self.acc = np.array([0.0, 0.0, 0.0])

        print(self.tf)
        print(self.start_tf)
        print(t)

        if len(self.tf) != 0 and self.mode == "TRAVELLING":
            for _ in range(len(self.tf)):
                if len(self.start_tf) > 1:
                    if self.tf[0] < t or self.start_tf[1] < t:
                        self.start_tf.pop(0)
                        self.tf.pop(0)
                        self.spline_2_follow.pop(0)

            if self.tf[0] <= t:
                t = self.tf[0]

            if self.start_tf[0] < t:
                t = t - self.start_tf[0]
                splinex, spliney, splinez = (
                    self.spline_2_follow[0][0],
                    self.spline_2_follow[0][1],
                    self.spline_2_follow[0][2],
                )
                x = splinex(t)
                dx = splinex.derivative(1)(t)
                ddx = splinex.derivative(2)(t)
                dddx = splinex.derivative(2)(t)
                # print(f"x:{x}")
                # print(f"dx:{dx}")
                # print(f"ddx:{ddx}")
                # print(f"dddx:{dddx}")
                y = spliney(t)
                dy = spliney.derivative(1)(t)
                ddy = spliney.derivative(2)(t)
                dddy = spliney.derivative(2)(t)
                # print(f"y:{y}")
                # print(f"dy:{dy}")
                # print(f"ddy:{ddy}")
                # print(f"dddy:{dddy}")
                z = splinez(t)
                dz = splinez.derivative(1)(t)
                ddz = splinez.derivative(2)(t)
                dddz = splinez.derivative(2)(t)
                # print(f"z:{z}")
                # print(f"dz:{dz}")
                # print(f"ddz:{ddz}")
                # print(f"dddz:{dddz}")

                self.goal_2_send.p = Point(x, y, z)
                self.goal_2_send.v = Vector3(dx, dy, dz)
                self.goal_2_send.a = Vector3(ddx, ddy, ddz)
                self.goal_2_send.j = Vector3(dddx, dddy, dddz)

                self.goal_2_send.header.stamp = rospy.Time.now()
        if self.mode == "GOAL-SEEN":
            error = self.goal_short - self.state
            en = np.linalg.norm(error)
            if en > 0.15:
                error = error / en * 0.01
            self.goal_2_send.p = Point(self.state[0] + error[0], self.state[1] + error[1], self.state[2] + error[2])
            error = 10 * error
            self.goal_2_send.v = Vector3(error[0], error[1], error[2])
            error = 2 * error
            self.goal_2_send.a = Vector3(error[0], error[1], error[2])

        self.pub_goal.publish(self.goal_2_send)

    def traj_cb(self, msg):
        poses = msg.poses
        points = []
        points = [self.state]
        dim = len(poses)

        if self.point_cloud.size != 0 and dim > 0:
            for i in range(len(poses)):
                points.append(np.array([poses[i].pose.position.x, poses[i].pose.position.y, poses[i].pose.position.z]))
                poses[i].pose.position.x = (poses[i].pose.position.x - self.state[0]) / 5
                poses[i].pose.position.y = (poses[i].pose.position.y - self.state[1]) / 5
                poses[i].pose.position.z = (poses[i].pose.position.z - self.state[2]) / 5

            msg.poses = poses
            self.pub_traj.publish(msg)
            t0 = msg.header.stamp.to_sec()
            tf = msg.header.stamp.to_sec() + len(points) * 0.01
            Tf = tf - t0

            points_samp = []

            # rospy.loginfo(f"time: {tf-t0}")
            # rospy.loginfo(f"shape: {len(points)}")
            # rospy.loginfo(f"state: {self.state}")
            # rospy.loginfo(f"quat: {self.quat}")
            # rospy.loginfo(f"goal_short: {self.goal_short}")

            if Tf < 1.7:
                incrmt = round((1.7 - Tf) / 0.01)
                points = points + [points[-1]] * incrmt
            else:
                points = points[:170]

            tt = np.linspace(0, 1.7, len(points))
            samp = np.linspace(0, len(points) - 1, 12, dtype=np.uint32)

            tt_samp = []
            for i in samp:
                points_samp.append(points[i])
                tt_samp.append(tt[i])

            points_samp = np.array(points_samp)
            points_samp = points_samp - self.state
            tt_samp = np.array(tt_samp)
            knots = np.linspace(0, 1.7, 8)

            tx, cx, _ = interpolate.splrep(tt_samp, points_samp[:, 0], s=0, k=3)  # , t=knots[1:-1], task=-1)
            ty, cy, _ = interpolate.splrep(tt_samp, points_samp[:, 1], s=0, k=3)  # , t=knots[1:-1], task=-1)
            tz, cz, _ = interpolate.splrep(tt_samp, points_samp[:, 2], s=0, k=3)  # , t=knots[1:-1], task=-1)

            cx = cx[:-3]
            cx[-4:] = cx[-2]
            cy = cy[:-3]
            cy[-4:] = cy[-2]
            cz = cz[:-3]
            cz[-4:] = cz[-2]

            lim = 5

            # cx = (cx - self.state[0])  #/ lim
            # cy = (cy - self.state[1])  #/ lim
            # cz = (cz - self.state[2])  #/ lim

            # rospy.loginfo(f"samp: {points_samp[:,0]}")
            # rospy.loginfo(f"tx: {tx}")
            # rospy.loginfo(f"cx: {cx}")
            # rospy.loginfo(f"cy: {cy[:-3]}")
            # rospy.loginfo(f"cz: {cz[:-3]}")

            # rospy.loginfo("\n")
            self.data_save = {}
            self.data_save["traj"] = np.concatenate([cx[:-3], cy[:-3], cz[:-3]])
            goal = self.goal_short - self.state
            self.data_save["goal"] = goal / lim
            self.data_save["vel"] = self.vel / self.v_max
            self.data_save["acc"] = self.acc / self.a_max

            """
            data_dir = "data" + str(self.data_num) + "/"
            makedirs(self.data_path + data_dir + 'Input', exist_ok=True)
            makedirs(self.data_path + data_dir + 'Label', exist_ok=True)

            with open(self.data_path + data_dir + 'Input/point_cloud.npy', 'wb') as f:
                np.save(f, self.point_cloud)

            with open(self.data_path + data_dir + 'Input/vel.npy', 'wb') as f:
                np.save(f, vel)

            with open(self.data_path + data_dir + 'Input/acc.npy', 'wb') as f:
                np.save(f, acc)

            with open(self.data_path + data_dir + 'Input/quat.npy', 'wb') as f:
                np.save(f, self.quat)

            with open(self.data_path + data_dir + 'Input/goal.npy', 'wb') as f:
                np.save(f, goal)

            with open(self.data_path + data_dir + 'Label/traj.npy', 'wb') as f:
                np.save(f, traj)

            self.data_num += 1
            """

    def compute_pt(self, p, lim):
        # p = np.round(p, decimals=1)
        x = float(p[0])
        y = float(p[1])
        z = float(p[2])

        r = int(abs(x / lim) * 255.0)
        g = int(abs(y / lim) * 255.0)
        b = int(abs(z / lim) * 255.0)
        a = 255
        rgb = struct.unpack("I", struct.pack("BBBB", b, g, r, a))[0]
        pt = [x, y, z, rgb]
        return pt

    def eval_T(self, i):
        T = np.array([1, i, i**2, i**3, i**4, i**5])
        dT = np.array([0.0, 1, 2 * i, 3 * i**2, 4 * i**3, 5 * i**4])
        ddT = np.array([0.0, 0.0, 2, 6 * i, 12 * i**2, 20 * i**3])
        dddT = np.array([0.0, 0.0, 0, 6, 24 * i, 60 * i**2])
        return T, dT, ddT, dddT

    def obj(self, x, p, t, eval_T):
        out = 0.0
        for i in range(len(p) - 1):
            T, _, _, _ = eval_T(t[i])
            out = out + (p[i] - np.dot(x, T)) ** 2
        T, _, _, _ = eval_T(t[-1])
        out = out + 10 * (p[-1] - np.dot(x, T)) ** 2
        return out

    def point_cloud_cb(self, timer):
        dist_goal = np.linalg.norm(self.state - self.goal)
        if np.linalg.norm(self.state - self.state_old) < 0.01:
            self.same_state += 1
        else:
            self.same_state = 0

        self.state_old = self.state

        if (dist_goal < 0.15) or self.same_state > 50:
            print("sending goal")
            self.same_state = 0
            self.goal = np.array([50, 10, 4]) * np.random.rand(3) + np.array([1, -5, 1])
            self.mode = "YAWING"

        msg = PoseStamped()
        msg.pose.position.x = self.goal[0]
        msg.pose.position.y = self.goal[1]
        msg.pose.position.z = self.goal[2]

        self.goal_pub.publish(msg)
        distA2TermGoal = np.linalg.norm(self.goal - self.state) + 1e-9
        ra = min((distA2TermGoal), self.ra + 0.75)
        self.goal_short = self.state + ra * (self.goal - self.state) / distA2TermGoal

        self.point_cloud = np.array([])
        if self.dynamic_points and self.static_points:
            ### pub Point Cloud
            self.obst_points = self.dynamic_points + self.static_points
            points = []
            lim = 5
            cv_image = np.zeros([51, 51, 1])
            positions_t = np.array([])
            scale_t = np.array([])
            for p in self.obst_points:
                scale = p.scale
                # print(p.pose.orientation.w)
                p = p.pose.position
                p_array = self.from_vector3(p) - self.state
                s_min = np.linalg.norm(p_array - np.array([scale.x / 2, scale.y / 2, scale.z / 2]))
                s_max = np.linalg.norm(p_array + np.array([scale.x / 2, scale.y / 2, scale.z / 2]))
                if s_min < lim or s_max < lim:
                    X, Y, Z = np.mgrid[
                        (p_array[0] - (scale.x) / 2) : (p_array[0] + (scale.x) / 2) : 0.1,
                        (p_array[1] - (scale.y) / 2) : (p_array[1] + (scale.y) / 2) : 0.1,
                        (p_array[2] - (scale.z) / 2) : (p_array[2] + (scale.z) / 2) : 0.1,
                    ]
                    positions = np.transpose(np.vstack([X.ravel(), Y.ravel(), Z.ravel()]))
                    positions = np.round(positions, decimals=3)
                    scale = np.linalg.norm(positions, axis=1, keepdims=True)
                    positions = positions / scale
                    for i, s in enumerate(scale):
                        if s < lim:
                            # positions[i] = self.R.apply(positions[i], inverse=True)
                            if positions_t.size:
                                min_p = np.abs(np.array(np.dot(positions_t, positions[i])) - 1)
                                equals = min_p < 0.0004
                                idx_min = min_p.argmin()
                                idx = np.where(equals)[0]
                                if idx.size == 0:
                                    positions_t = np.concatenate([positions_t, positions[i : i + 1]], axis=0)
                                    scale_t = np.concatenate([scale_t, scale[i]], axis=0)
                                else:
                                    scale_t[idx_min] = np.min(np.array([s[0], scale_t[idx_min]]))
                            else:
                                positions_t = positions[i : i + 1]
                                scale_t = scale[i]
            if positions_t.size:
                self.point_cloud = positions_t * np.reshape(scale_t, (scale_t.shape[0], 1)) / lim

        gs = PointStamped()
        gs.header.frame_id = "world"
        gs.point.x = self.goal_short[0]
        gs.point.y = self.goal_short[1]
        gs.point.z = self.goal_short[2]
        self.pub_goal_short.publish(gs)

        if np.linalg.norm(self.goal_short - self.state) < 0.3:
            self.mode = "GOAL-SEEN"

        if self.vel is not None and np.any(self.goal_short != np.zeros(3)) and (self.mode == "TRAVELLING"):
            lim = 5
            if self.point_cloud.size != 0 and self.point_cloud.size != 1:
                A = np.zeros((6, 6))
                self.point_cloud = np.ones((6, 3))
            else:
                A = radius_neighbors_graph(self.point_cloud, 0.2, mode="connectivity", include_self=True).toarray()
                D = np.diag(np.sum(A, axis=1) ** (-1 / 2))
                A = D * A * D
            # print(f"goal {self.goal_short}")

            goal = self.goal_short - self.state
            goal = goal / lim
            vel = self.vel / self.v_max  # np.clip(self.vel, -self.v_max[0], self.v_max[0]) / self.v_max
            # self.acc[:2] = np.clip(self.acc[:2], -self.a_max[0], self.a_max[0])
            # self.acc[2] = np.clip(self.acc[2], -self.a_max[2], self.a_max[2])
            acc = self.acc / self.a_max

            A = torch.Tensor(A).to(device).unsqueeze(0)
            point_cloud = (
                torch.Tensor(self.point_cloud)
                .to(device)
                .reshape(1, self.point_cloud.shape[0], self.point_cloud.shape[1])
            )
            goal = torch.Tensor(goal).to(device).reshape(1, self.goal.shape[0])
            vel = torch.Tensor(vel).to(device).reshape(1, self.vel.shape[0])
            acc = torch.Tensor(acc).to(device).reshape(1, self.acc.shape[0])
            quat = torch.Tensor(self.quat).to(device).reshape(1, self.quat.shape[0])

            # print(f"goal {goal}")
            # print(f"vel {vel}")
            with torch.no_grad():
                out, _, _ = self.model_sas(point_cloud, goal, quat, vel, acc, A, True)

            t0 = rospy.Time.now().to_sec()
            t = self.model_sas.knots.cpu().numpy()
            out = out.cpu().numpy() * lim

            cx = out[0, :10]
            cx = np.concatenate([cx, np.array([cx[-1]] * 3)]) + self.state[0]
            cy = out[0, 10:20]
            cy = np.concatenate([cy, np.array([cy[-1]] * 3)]) + self.state[1]
            cz = out[0, 20:]
            cz = np.concatenate([cz, np.array([cz[-1]] * 3)]) + self.state[2]

            splinex = interpolate.BSpline(t, cx, 3, extrapolate=False)
            spliney = interpolate.BSpline(t, cy, 3, extrapolate=False)
            splinez = interpolate.BSpline(t, cz, 3, extrapolate=False)

            rospy.loginfo("computing spline... ")
            # print(f"trajx {cx}")
            # print(f"trajy {cy}")
            # print(f"trajz {cz}")

            self.tf.append(t[-1] + t0)
            self.start_tf.append(t[0] + t0)
            self.spline_2_follow.append([splinex, spliney, splinez])

        """
        if self.data_save:
            data_dir = "data" + str(self.data_num) + "/"
            makedirs(self.data_path + data_dir + 'Input', exist_ok=True)
            makedirs(self.data_path + data_dir + 'Label', exist_ok=True)

            with open(self.data_path + data_dir + 'Input/point_cloud.npy', 'wb') as f:
                np.save(f, self.point_cloud)

            with open(self.data_path + data_dir + 'Input/vel.npy', 'wb') as f:
                np.save(f, self.data_save["vel"])

            with open(self.data_path + data_dir + 'Input/acc.npy', 'wb') as f:
                np.save(f, self.data_save["acc"])

            with open(self.data_path + data_dir + 'Input/quat.npy', 'wb') as f:
                np.save(f, self.quat)

            with open(self.data_path + data_dir + 'Input/goal.npy', 'wb') as f:
                np.save(f, self.data_save["goal"])

            with open(self.data_path + data_dir + 'Label/traj.npy', 'wb') as f:
                np.save(f, self.data_save["traj"])

            self.data_num += 1
        """
        """
        if self.dynamic_points and self.static_points:
            for i, p in enumerate(positions_t):
                pt_temp = p * scale_t[i] / lim
                pt_temp = self.compute_pt(pt_temp, lim)

                x_im = (pt_temp[0] + 1) * lim * 10 / 2
                z_im = (pt_temp[2] + 1) * lim * 10 / 2
                s = (scale_t[i] - 0.01) / (lim - 0.01)
                cv_image[int(round(x_im)), int(round(z_im)), 0] = 255 - (s * 255)
                points.append(pt_temp)
                
            fields = [
                PointField('x', 0, PointField.FLOAT32, 1),
                PointField('y', 4, PointField.FLOAT32, 1),
                PointField('z', 8, PointField.FLOAT32, 1),
                # PointField('rgb', 12, PointField.UINT32, 1),
                PointField('rgba', 12, PointField.UINT32, 1),
            ]
            header = Header()
            header.frame_id = "world"
            pc2_msg = point_cloud2.create_cloud(header, fields, points)
            self.pub_point_cloud.publish(pc2_msg)
        """
        """
            ## pub image
            
            cv_image = cv2.GaussianBlur(cv_image, (5, 5), cv2.BORDER_DEFAULT)
            cv_image = np.rint(cv_image)
            cv_image = cv_image.astype(np.uint8)
            image_message = self.bridge.cv2_to_imgmsg(cv_image, encoding="mono8")

            self.pub_image.publish(image_message)
        """

    def pub_clock_timer(self):
        while not rospy.is_shutdown():
            self.time_clock.clock = self.time_clock.clock + rospy.Duration.from_sec(0.0005)
            self.pub_clock.publish(self.time_clock)
            sleep(0.01)


if __name__ == "__main__":
    p = CreateDataset()
    thread_clock = Thread(target=p.pub_clock_timer())
    thread_clock.start()
    rospy.spin()
    thread_clock.join()
