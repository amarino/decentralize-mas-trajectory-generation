#!/usr/bin/env python3

import struct

import rospy
from std_msgs.msg import String
import std_msgs.msg as std_msgs
from visualization_msgs.msg import MarkerArray, Marker
from rosgraph_msgs.msg import Clock
import sensor_msgs.msg as sensor_msgs
from sensor_msgs import point_cloud2
from sensor_msgs.msg import PointCloud2, PointField, Image
from std_msgs.msg import Header
from snapstack_msgs.msg import State, Goal
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import PoseStamped, PointStamped, Point, Vector3
from mader_msgs.msg import *
import cv2
from cv_bridge import CvBridge
import torch
from nav_msgs.msg import Path
import math
import sys
from sklearn.neighbors import radius_neighbors_graph

from threading import Thread
from time import sleep, perf_counter
import time
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.transform import Rotation
from scipy.interpolate import BSpline
from scipy import interpolate
import scipy.optimize as opt
import rospkg
import os.path
from os import makedirs
from functools import partial
import open3d as o3d

from std_srvs.srv import Trigger, TriggerRequest

from MASTrajModel import saliency_map

rospack = rospkg.RosPack()
# Device configuration
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

np.random.seed(8)


class CreateDataset:
    def __init__(self, num: int):
        rospy.init_node("dataset_creation", anonymous=True)

        self.data_path = os.path.dirname(rospack.get_path("mader"))
        pcd = o3d.io.read_point_cloud(self.data_path + "/mader/meshes/quadrotor/crazyflie_pcl.ply")
        self.drone_points = np.asanyarray(pcd.points)
        self.pub_clock = rospy.Publisher("/clock", Clock, queue_size=6)

        self.num = num
        self.sub_traj = []
        self.pub_traj = []
        self.sub_goal = []
        self.sub_state = []
        self.goal_pub = []
        self.pub_goal_short = []
        self.pub_goal = []
        self.v_max = []
        self.a_max = []
        self.ra = []
        self.compute_goal = []
        self.pub_traj_safe = []

        self.pub_image = rospy.Publisher("image", Image, queue_size=6)

        self.knots = [
            0.0,
            0.0,
            0.0,
            0.0,
            0.63180516,
            0.95272206,
            1.26361032,
            1.58452722,
            1.90544413,
            2.22636103,
            2.53724928,
            2.85816619,
            3.5,
            3.5,
            3.5,
            3.5,
        ]

        self.v_max = [1.0, 1.0, 1.0]
        self.a_max = [2.0, 2.0, 2.0]

        self.restart_srv = rospy.ServiceProxy("restart_obstacles", Trigger)
        self.restart = [False for _ in range(self.num)]
        train_mas = "/trained_models/mas_traj_model_BS.pt"

        self.model_mas = torch.load(self.data_path + train_mas)
        self.model_mas.eval()
        self.model_mas.qp_enabled = True
        self.model_mas.use_cc = False
        self.model_mas.compute_cc = False
        self.model_mas.alpha = 0.0

        self.model_mas.to(device)

        self.goal_2_send = Goal()
        self.spline_2_follow = [[] for _ in range(self.num)]
        self.tf = [[] for _ in range(self.num)]
        self.start_tf = [[] for _ in range(self.num)]

        self.mode = ["GOAL_REACHED" for _ in range(self.num)]

        self.data_path = (
            self.data_path + "/dataset_multi/" + "2024-05-08_14-12-33/"
        )  # time.strftime("%Y-%m-%d_%H-%M-%S/")
        self.data_num = 1693
        makedirs(self.data_path, exist_ok=True)
        self.point_cloud = np.array([])

        self.dynamic_points = None
        self.static_points = None

        self.bridge = CvBridge()

        self.data_save = {}
        self.goal_2_send = [Goal() for _ in range(self.num)]
        self.state = [np.array([0, 0, 0]) for _ in range(self.num)]
        self.vel = [None for _ in range(self.num)]
        self.acc = [None for _ in range(self.num)]
        self.state_old = [np.array([0.0, 0.0, 0.0]) for _ in range(self.num)]
        self.same_state = [45 for _ in range(self.num)]
        self.quat = [np.array([0.0, 0.0, 0.0, 1.0]) for _ in range(self.num)]
        self.R = [Rotation.from_quat(np.array([0.0, 0.0, 0.0, 1.0])) for _ in range(self.num)]

        self.time_clock = Clock()
        self.time_clock.clock = rospy.Time.from_sec(time.time())
        self.dynamic_obst_xyz = []
        self.static_obst_xyz = []
        self.goal = [np.array([0.0, 0.0, 0.0]) for _ in range(self.num)]
        self.goal_old = [np.array([0.0, 0.0, 0.0]) for _ in range(self.num)]
        self.goal_short = [np.array([0.0, 0.0, 0.0]) for _ in range(self.num)]

        for i in range(num):
            self.sub_traj.append(
                rospy.Subscriber(f"/SQ0{i+1}s/mader/traj_safe", Path, partial(self.traj_cb, num=i))
            )
            self.pub_traj.append(
                rospy.Publisher(f"/SQ0{i+1}s/mader/traj_safe_shift", Path, queue_size=1)
            )
            # self.sub_goal.append(
            #    rospy.Subscriber(f"/SQ0{i+1}s/mader/point_G", PointStamped, partial(self.goal_cb, num=i))
            # )
            self.sub_state.append(
                rospy.Subscriber(f"/SQ0{i+1}s/state", State, partial(self.state_cb, num=i))
            )
            self.goal_pub.append(
                rospy.Publisher(f"/SQ0{i+1}s/term_goal", PoseStamped, queue_size=1)
            )
            self.pub_goal_short.append(
                rospy.Publisher(f"/SQ0{i+1}s/mader/point_G", PointStamped, queue_size=1)
            )
            self.pub_goal.append(rospy.Publisher(f"/SQ0{i+1}s/goal", Goal, queue_size=1))
            self.pub_traj_safe.append(
                rospy.Publisher(
                    f"/SQ0{i+1}s/mader/traj_safe_colored",
                    MarkerArray,
                    queue_size=1,
                )
            )
            self.v_max.append(rospy.get_param(f"/SQ0{i+1}s/mader/v_max"))
            self.a_max.append(rospy.get_param(f"/SQ0{i+1}s/mader/a_max"))
            self.ra.append(rospy.get_param(f"/SQ0{i+1}s/mader/Ra"))
            self.compute_goal.append(False)

        self.sub_obstacles_dyn = rospy.Subscriber(
            "/shapes_dynamic_mesh", MarkerArray, self.dynamic_obst_cb
        )
        self.sub_onstacles_static = rospy.Subscriber(
            "/shapes_static_mesh", MarkerArray, self.static_obst_cb
        )
        self.pub_point_cloud = rospy.Publisher("point_cloud2", PointCloud2, queue_size=6)

        self.timer = rospy.Timer(rospy.Duration(0.1), self.point_cloud_cb)
        # self.timer_goal = rospy.Timer(rospy.Duration(0.01), self.goal_new_tensor)

    def from_vector3(self, msg):
        return np.array([msg.x, msg.y, msg.z])

    def goal_cb(self, msg, num):
        self.goal_short[num] = self.from_vector3(msg.point)

    def state_cb(self, msg, num):
        self.state[num] = self.from_vector3(msg.pos)
        self.quat[num] = np.array([msg.quat.x, msg.quat.y, msg.quat.z, msg.quat.w])
        self.vel[num] = self.from_vector3(msg.vel) / self.v_max[num]
        self.acc[num] = self.from_vector3(msg.abias) / self.a_max[num]
        self.R[num] = Rotation.from_quat(self.quat[num])
        if self.compute_goal[num] == False:
            self.goal_old[num] = self.state[num]
            self.compute_goal[num] = True

    def dynamic_obst_cb(self, msg):
        self.dynamic_points = msg.markers

    def static_obst_cb(self, msg):
        self.static_points = msg.markers

    def goal_new_tensor(self, timer):
        for j in range(self.num):
            _, _, yaw = euler_from_quaternion(self.quat[j])
            self.goal_2_send[j].psi = yaw
            if self.mode[j] == "YAWING" or self.mode[j] == "TRAVELLING":
                desired_yaw = np.arctan2(
                    self.goal_short[j][1] - self.state[j][1],
                    self.goal_short[j][0] - self.state[j][0],
                )
                diff = desired_yaw - yaw
                diff = max(min(diff, 0.01 * 2.5), -0.01 * 2.5)

                diff = math.fmod(diff + np.pi, 2 * np.pi)
                if diff < 0:
                    diff += 2 * np.pi
                diff -= np.pi

                dyaw = diff / (np.abs(diff) + 1e-12) * 2.5

                yaw = yaw + dyaw * 0.01

                # print(f"yaw {yaw}")
                # print(f"diff {diff}")
                # print(f"dyaw {dyaw}")

                self.goal_2_send[j].dpsi = dyaw
                self.goal_2_send[j].psi = yaw

                if np.abs(diff) < 0.01:
                    rospy.loginfo(f"mode: TRAVELLING {diff}")
                    self.mode[j] = "TRAVELLING"

            self.goal_2_send[j].header.stamp = rospy.Time.now()
            self.goal_2_send[j].header.frame_id = "world"

            self.goal_2_send[j].power = True

            if self.tf[j] and self.mode[j] == "TRAVELLING":
                t = rospy.Time.now().to_sec()
                for _ in range(len(self.tf[j])):
                    if len(self.start_tf[j]) > 1:
                        if self.tf[j][0] < t or self.start_tf[j][1] < t:
                            self.start_tf[j].pop(0)
                            self.tf[j].pop(0)
                            self.spline_2_follow[j].pop(0)

                if self.tf[j][0] <= t:
                    t = self.tf[j][0]

                if self.start_tf[j][0] < t:
                    t = t - self.start_tf[j][0]
                    splinex, spliney, splinez = (
                        self.spline_2_follow[j][0][0],
                        self.spline_2_follow[j][0][1],
                        self.spline_2_follow[j][0][2],
                    )
                    x = splinex(t)
                    dx = np.clip(
                        splinex.derivative(1)(t),
                        a_min=-(self.v_max[j][0]),
                        a_max=(self.v_max[j][0]),
                    )
                    ddx = np.clip(
                        splinex.derivative(2)(t),
                        a_min=-(self.a_max[j][0] - 10),
                        a_max=(self.a_max[j][0] - 10),
                    )
                    dddx = splinex.derivative(3)(t)
                    # print(f"x{j}:{x}")
                    # print(f"dx:{dx}")
                    # print(f"{j} ddx:{ddx}")
                    # print(f"dddx:{dddx}")
                    y = spliney(t)
                    dy = np.clip(
                        spliney.derivative(1)(t),
                        a_min=-(self.v_max[j][1]),
                        a_max=(self.v_max[j][1]),
                    )
                    ddy = np.clip(
                        spliney.derivative(2)(t),
                        a_min=-(self.a_max[j][1] - 10),
                        a_max=(self.a_max[j][1] - 10),
                    )
                    dddy = spliney.derivative(3)(t)
                    # print(f"y{j}:{y}")
                    # print(f"dy:{dy}")
                    # print(f"{j} ddy:{ddy}")
                    # print(f"dddy:{dddy}")
                    z = splinez(t)
                    dz = np.clip(
                        splinez.derivative(1)(t),
                        a_min=-(self.v_max[j][2]),
                        a_max=(self.v_max[j][2]),
                    )
                    ddz = np.clip(
                        splinez.derivative(2)(t),
                        a_min=-(self.a_max[j][2] - 1),
                        a_max=(self.a_max[j][2] - 1),
                    )
                    dddz = splinez.derivative(3)(t)
                    # print(f"z{j}:{z}")
                    # print(f"dz:{dz}")
                    # print(f"{j} ddz:{ddz}")
                    # print(f"dddz:{dddz}")

                    self.goal_2_send[j].p = Point(x, y, z)
                    self.goal_2_send[j].v = Vector3(dx, dy, dz)
                    self.goal_2_send[j].a = Vector3(ddx, ddy, ddz)
                    self.goal_2_send[j].j = Vector3(dddx, dddy, dddz)

                    # goal_2_send.header.stamp = rospy.Time.now()
            elif self.mode[j] == "GOAL-SEEN":
                error = self.goal_short[j] - self.state[j]
                en = np.linalg.norm(error)
                if en > 0.15:
                    error = error / en * 0.01
                self.goal_2_send[j].p = Point(
                    self.state[j][0] + error[0],
                    self.state[j][1] + error[1],
                    self.state[j][2] + error[2],
                )
                error = 15 * error
                self.goal_2_send[j].v = Vector3(error[0], error[1], error[2])
                error = 2 * error
                self.goal_2_send[j].a = Vector3(error[0], error[1], error[2])
            else:
                self.goal_2_send[j].p = Point(self.state[j][0], self.state[j][1], self.state[j][2])

            self.pub_goal[j].publish(self.goal_2_send[j])

    def traj_cb(self, msg, num):
        poses = msg.poses
        points = [self.state[num]]
        dim = len(poses)

        if dim > 0:
            lim = 4.5
            for i in range(len(poses)):
                points.append(
                    np.array(
                        [
                            poses[i].pose.position.x,
                            poses[i].pose.position.y,
                            poses[i].pose.position.z,
                        ]
                    )
                )
                poses[i].pose.position.x = (poses[i].pose.position.x - self.state[num][0]) / lim
                poses[i].pose.position.y = (poses[i].pose.position.y - self.state[num][1]) / lim
                poses[i].pose.position.z = (poses[i].pose.position.z - self.state[num][2]) / lim

            t0 = msg.header.stamp.to_sec()
            tf = msg.header.stamp.to_sec() + len(points) * 0.01
            Tf = tf - t0

            points_samp = []
            rospy.loginfo(f"time: {Tf}")
            # rospy.loginfo(f"time: {tf-t0}")
            # rospy.loginfo(f"shape: {len(points)}")
            # rospy.loginfo(f"state: {self.state}")
            # rospy.loginfo(f"quat: {self.quat}")
            # rospy.loginfo(f"goal_short: {self.goal_short}")
            t_final = 3.5
            if Tf < t_final:
                incrmt = round((t_final - Tf) / 0.01)
                points = points + [points[-1]] * incrmt
            else:
                points = points[: int(t_final * 100)]

            tt = np.linspace(0, t_final, len(points))
            samp = np.linspace(0, len(points) - 1, 12, dtype=np.uint32)

            tt_samp = []
            poses = []
            for i in samp:
                points_samp.append(points[i])
                pt = PoseStamped()
                pt.pose.position.x = points[i][0]
                pt.pose.position.y = points[i][1]
                pt.pose.position.z = points[i][2]
                poses.append(pt)
                tt_samp.append(tt[i])

            msg.poses = poses
            self.pub_traj[num].publish(msg)

            points_samp = np.array(points_samp)
            points_samp = points_samp - self.state[num]
            tt_samp = np.array(tt_samp)

            tx, cx, _ = interpolate.splrep(
                tt_samp, points_samp[:, 0], s=0, k=3
            )  # , t=knots[1:-1], task=-1)
            ty, cy, _ = interpolate.splrep(
                tt_samp, points_samp[:, 1], s=0, k=3
            )  # , t=knots[1:-1], task=-1)
            tz, cz, _ = interpolate.splrep(
                tt_samp, points_samp[:, 2], s=0, k=3
            )  # , t=knots[1:-1], task=-1)

            cx = cx[:-3]
            cx[-4:] = cx[-2]
            cy = cy[:-3]
            cy[-4:] = cy[-2]
            cz = cz[:-3]
            cz[-4:] = cz[-2]

            # cx = (cx - self.state[0])  #/ lim
            # cy = (cy - self.state[1])  #/ lim
            # cz = (cz - self.state[2])  #/ lim

            # rospy.loginfo(f"samp: {points_samp[:,0]}")
            # rospy.loginfo(f"tx: {tx}")
            rospy.loginfo(f"num: {num}")

            # rospy.loginfo("\n")
            self.data_save[num] = {}
            self.data_save[num]["traj"] = np.concatenate([cx[:-3], cy[:-3], cz[:-3]])
            goal = self.goal_short[num] - self.state[num]
            self.data_save[num]["goal"] = goal / lim
            self.data_save[num]["vel"] = self.vel[num]
            self.data_save[num]["acc"] = self.acc[num]
            self.data_save[num]["state"] = self.state[num]
            self.data_save[num]["points"] = poses

            # print(f" {num} vel: {self.data_save[num]['vel']}")
            # print(f" {num} acc: {self.data_save[num]['acc']}")

    def compute_pt(self, p, lim, j, pt_disp, i):
        # p = np.round(p, decimals=1)

        x = float(p[0])
        y = float(p[1])
        z = float(p[2])

        if pt_disp.size == 0:
            a = 255
        else:
            a = int(255 * pt_disp[j, i])

        rgb = "{0:08b}".format(j)

        rgb = [int(rgb[-1]), int(rgb[-2]), int(rgb[-3])]
        if j > 6:
            rgb = [
                int(rgb[-1]) * 0.5,
                int(rgb[-2]) * 0.5,
                int(rgb[-3]) * 0.5 + 0.5,
            ]

        r = int(rgb[-1] * 255)
        g = int(rgb[-2] * 255)
        b = int(rgb[-3] * 255)

        rgb = struct.unpack("I", struct.pack("BBBB", b, g, r, a))[0]
        pt = [x, y, z, rgb]
        return pt

    def eval_T(self, i):
        T = np.array([1, i, i**2, i**3, i**4, i**5])
        dT = np.array([0.0, 1, 2 * i, 3 * i**2, 4 * i**3, 5 * i**4])
        ddT = np.array([0.0, 0.0, 2, 6 * i, 12 * i**2, 20 * i**3])
        dddT = np.array([0.0, 0.0, 0, 6, 24 * i, 60 * i**2])
        return T, dT, ddT, dddT

    def obj(self, x, p, t, eval_T):
        out = 0.0
        for i in range(len(p) - 1):
            T, _, _, _ = eval_T(t[i])
            out = out + (p[i] - np.dot(x, T)) ** 2
        T, _, _, _ = eval_T(t[-1])
        out = out + 10 * (p[-1] - np.dot(x, T)) ** 2
        return out

    def make_marker_array(self, splinex, spliney, splinez, t, j):
        marker_array = MarkerArray()

        tt = np.linspace(t / 15, t, 15)
        p_last = Point()
        p_last.x = splinex(0)
        p_last.y = spliney(0)
        p_last.z = splinez(0)

        rgb = "{0:08b}".format(j)
        rgb = [int(rgb[-1]), int(rgb[-2]), int(rgb[-3])]
        if j > 6:
            rgb = [
                int(rgb[-1]) * 0.5,
                int(rgb[-2]) * 0.5,
                int(rgb[-3]) * 0.5 + 0.5,
            ]

        for i, ti in enumerate(tt):
            m = Marker()
            m.type = Marker.ARROW
            m.header.frame_id = "world"
            m.header.stamp = rospy.Time.now()
            m.action = Marker.ADD
            m.id = 9000 + i
            m.ns = "SQ0" + str(j) + "s"

            m.color.r = rgb[-1]
            m.color.g = rgb[-2]
            m.color.b = rgb[-3]
            m.color.a = 1.0
            m.scale.x = 0.15
            m.scale.y = 0.0000001
            m.scale.z = 0.0000001
            m.pose.orientation.w = 1.0
            p = Point()
            p.x = splinex(ti)
            p.y = spliney(ti)
            p.z = splinez(ti)
            m.points.append(p_last)
            m.points.append(p)
            p_last = p
            marker_array.markers.append(m)

        return marker_array

    def point_cloud_cb(self, timer):
        for i in range(self.num):
            dist_goal = np.linalg.norm(self.state[i] - self.goal_old[i])
            if np.linalg.norm(self.state[i] - self.state_old[i]) < 0.01:
                self.same_state[i] += 1
            else:
                self.same_state[i] = 0

            self.state_old[i] = self.state[i]
            if dist_goal < 0.3 or self.same_state[i] > 15:
                if self.compute_goal[i]:
                    rospy.loginfo("sending goal")
                    self.restart[i] = True
                    self.same_state[i] = 0

            msg = PoseStamped()
            msg.pose.position.x = self.goal[i][0]
            msg.pose.position.y = self.goal[i][1]
            msg.pose.position.z = self.goal[i][2]

            self.goal_pub[i].publish(msg)

            distA2TermGoal = np.linalg.norm(self.goal[i] - self.state[i]) + 1e-9
            ra = min((distA2TermGoal), self.ra[i] + 0.25)
            self.goal_short[i] = (
                self.state[i] + ra * (self.goal[i] - self.state[i]) / distA2TermGoal
            )

        if all(self.restart):
            self.data_save = {}
            for i in range(self.num):
                self.restart[i] = False
                self.goal_old[i] = np.array(
                    [-self.state[i][0], -self.state[i][1], -self.state[i][2]]
                )
                self.goal_old[i] = self.goal_old[i] + (3 * np.random.rand(3) - 1.5)
                self.goal_old[i] = self.goal_old[i] * 10 / np.linalg.norm(self.goal_old[i])
                # self.goal_old[i] = np.array([30, self.state[i][1], -self.state[i][2]])
                self.goal[i] = self.goal_old[i]
                self.mode[i] = "YAWING"
            self.restart_srv(TriggerRequest())

        self.point_cloud = [np.array([]) for _ in range(self.num)]
        if self.dynamic_points or self.static_points:
            ### pub Point Cloud
            self.obst_points = self.dynamic_points + self.static_points
            points = []
            lim = 4.5
            cv_image = np.zeros([46, 46, 1])
            positions_t = [np.array([]) for _ in range(self.num)]
            scale_t = [np.array([]) for _ in range(self.num)]
            for p in self.obst_points:
                p_scale = p.scale
                # print(p.pose.orientation.w)
                p = p.pose.position
                for j in range(self.num):
                    p_array = self.from_vector3(p) - self.state[j]
                    s_min = np.linalg.norm(
                        p_array - np.array([p_scale.x / 2, p_scale.y / 2, p_scale.z / 2])
                    )
                    s_max = np.linalg.norm(
                        p_array + np.array([p_scale.x / 2, p_scale.y / 2, p_scale.z / 2])
                    )
                    if s_min < lim or s_max < lim:
                        X, Y, Z = np.mgrid[
                            (p_array[0] - (p_scale.x / 2) + 0.05) : (
                                p_array[0] + (p_scale.x / 2) - 0.05
                            ) : p_scale.x
                            / 0.1j,
                            (p_array[1] - (p_scale.y / 2) + 0.05) : (
                                p_array[1] + (p_scale.y / 2) - 0.05
                            ) : p_scale.y
                            / 0.1j,
                            (p_array[2] - (p_scale.z / 2) + 0.05) : (
                                p_array[2] + (p_scale.z / 2) - 0.05
                            ) : p_scale.z
                            / 0.1j,
                        ]
                        positions = np.transpose(np.vstack([X.ravel(), Y.ravel(), Z.ravel()]))
                        # positions = np.round(positions, decimals=3)
                        scale = np.linalg.norm(positions, axis=1, keepdims=True)
                        positions = positions / scale
                        for i, s in enumerate(scale):
                            if s < lim:
                                # positions[i] = self.R.apply(positions[i], inverse=True)
                                if positions_t[j].size:
                                    min_p = np.abs(
                                        1 - np.array(np.dot(positions_t[j], positions[i]))
                                    )
                                    equals = min_p < 0.0011
                                    idx_min = min_p.argmin()
                                    idx = np.where(equals)[0]
                                    if idx.size == 0:
                                        positions_t[j] = np.concatenate(
                                            [
                                                positions_t[j],
                                                positions[i : i + 1],
                                            ],
                                            axis=0,
                                        )
                                        scale_t[j] = np.concatenate([scale_t[j], s], axis=0)
                                    else:
                                        scale_t[j][idx_min] = np.min(
                                            np.array([s[0], scale_t[j][idx_min]])
                                        )
                                else:
                                    positions_t[j] = positions[i : i + 1]
                                    scale_t[j] = s

            # drones point cloud
            positions_p = [np.array([]) for _ in range(self.num)]
            scale_p = [np.array([]) for _ in range(self.num)]

            for j in range(self.num):
                for k in range(self.num):
                    if j != k:
                        p_array = self.state[k] - self.state[j]
                        p_scale = 0.30
                        s_min = np.linalg.norm(
                            p_array - np.array([p_scale / 2, p_scale / 2, p_scale / 2])
                        )
                        s_max = np.linalg.norm(
                            p_array + np.array([p_scale / 2, p_scale / 2, p_scale / 2])
                        )
                        if s_min < lim or s_max < lim:
                            positions = p_array + self.drone_points
                            positions = np.round(positions, decimals=2)
                            scale = np.linalg.norm(positions, axis=1, keepdims=True)
                            positions = positions / scale
                            for i, s in enumerate(scale):
                                if s < lim:
                                    if positions_p[j].size:
                                        min_p = np.abs(
                                            1 - np.array(np.dot(positions_p[j], positions[i]))
                                        )
                                        equals = min_p < 0.0011
                                        idx_min = min_p.argmin()
                                        idx = np.where(equals)[0]
                                        if idx.size == 0:
                                            positions_p[j] = np.concatenate(
                                                [
                                                    positions_p[j],
                                                    positions[i : i + 1],
                                                ],
                                                axis=0,
                                            )
                                            scale_p[j] = np.concatenate([scale_p[j], s], axis=0)
                                        else:
                                            scale_p[j][idx_min] = np.min(
                                                np.array([s[0], scale_p[j][idx_min]])
                                            )
                                    else:
                                        positions_p[j] = positions[i : i + 1]
                                        scale_p[j] = s
            max_num = 1
            max_num_drone = 1
            self.point_cloud_drone = [np.array([]) for _ in range(self.num)]
            for j in range(self.num):
                if positions_t[j].size or positions_t[j].size == 1:
                    self.point_cloud[j] = (
                        positions_t[j] * np.reshape(scale_t[j], (scale_t[j].shape[0], 1)) / lim
                    )
                    max_num = max([max_num, self.point_cloud[j].shape[0]])
                else:
                    self.point_cloud[j] = np.ones((1, 3))

                if positions_p[j].size or positions_p[j].size == 1:
                    self.point_cloud_drone[j] = (
                        positions_p[j] * np.reshape(scale_p[j], (scale_p[j].shape[0], 1)) / lim
                    )
                    max_num_drone = max([max_num_drone, self.point_cloud_drone[j].shape[0]])
                else:
                    self.point_cloud_drone[j] = np.ones((1, 3))

                # print(self.point_cloud[j].shape)

                gs = PointStamped()
                gs.header.frame_id = "world"
                gs.point.x = self.goal_short[j][0]
                gs.point.y = self.goal_short[j][1]
                gs.point.z = self.goal_short[j][2]
                self.pub_goal_short[j].publish(gs)

                if np.linalg.norm(self.goal_short[j] - self.state[j]) < 0.9:
                    self.mode[j] = "GOAL-SEEN"

            # [print(pt.shape) for pt in self.point_cloud]

            Apt = []
            goal = []
            success = True
            disp_out = None
            """
            for j in range(self.num):
                if self.point_cloud[j].size != 0:
                    self.point_cloud[j] = np.vstack(
                        (
                            self.point_cloud[j],
                            np.tile(
                                self.point_cloud[j][[-1], :],
                                [max_num - self.point_cloud[j].shape[0], 1],
                            ),
                        )
                    )
                else:
                    self.point_cloud[j] = np.ones((max_num, 3))

                if self.point_cloud_drone[j].size != 0:
                    self.point_cloud_drone[j] = np.vstack(
                        (
                            self.point_cloud_drone[j],
                            np.tile(
                                self.point_cloud_drone[j][[-1], :],
                                [
                                    max_num_drone - self.point_cloud_drone[j].shape[0],
                                    1,
                                ],
                            ),
                        )
                    )
                else:
                    self.point_cloud_drone[j] = np.ones((max_num_drone, 3))
            self.point_cloud = np.stack(self.point_cloud)
            self.point_cloud_drone = np.stack(self.point_cloud_drone)

            self.point_cloud = np.concatenate(
                (
                    self.point_cloud,
                    self.point_cloud_drone,
                ),
                axis=1,
            )
            for j in range(self.num):
                goal.append((self.goal_short[j] - self.state[j]) / lim)
                A = radius_neighbors_graph(
                    self.point_cloud[j, ...],
                    0.05,
                    mode="connectivity",
                    include_self=True,
                ).toarray()
                D = np.diag(np.sum(A, axis=1) ** (-1 / 2))
                Apt.append(D @ A @ D)

                if self.vel[j] is None:
                    success = False
                    break

            if success:
                Apt = torch.Tensor(np.stack(Apt)).to(device).unsqueeze(0)
                point_cloud = torch.Tensor(self.point_cloud).to(device).unsqueeze(0)
                goal = torch.Tensor(np.stack(goal)).to(device).unsqueeze(0)
                vel = torch.Tensor(np.stack(self.vel)).to(device).unsqueeze(0)
                acc = torch.Tensor(np.stack(self.acc)).to(device).unsqueeze(0)
                quat = torch.Tensor(np.stack(self.quat)).to(device).unsqueeze(0)
                A = radius_neighbors_graph(
                    self.state, 4, mode="connectivity", include_self=True
                ).toarray()
                D = np.diag(np.sum(A, axis=1) ** (-1))
                A = torch.Tensor(D @ A).to(device).unsqueeze(0)

                # print(f"A: {A}")
                # print(f"goal {goal}")
                # print(f"vel {vel}")
                # print(f"acc {acc}")
                vel = torch.clamp(vel, min=-0.99, max=0.99)
                acc = torch.clamp(acc, min=-0.99, max=0.99)

                with torch.no_grad():
                    out, _, _, disp_out = self.model_mas(point_cloud, goal, quat, vel, acc, Apt, A)

                t0 = rospy.Time.now().to_sec()
                t = self.model_mas.knots.cpu().numpy()
                out = out.cpu().numpy() * (lim)

                # print(f"state {self.state}")
                # print(f"trajx {cx}")
                # print(f"trajy {cy}")
                # print(f"trajz {cz}")

                for j in range(self.num):
                    cx = out[0, j, :10]
                    cy = out[0, j, 10:20]
                    cz = out[0, j, 20:]
                    cx = np.concatenate([cx, np.array([cx[-1]] * 3)]) + self.state[j][0]
                    cy = np.concatenate([cy, np.array([cy[-1]] * 3)]) + self.state[j][1]
                    cz = np.concatenate([cz, np.array([cz[-1]] * 3)]) + self.state[j][2]
                    splinex = interpolate.BSpline(t, cx, 3, extrapolate=False)
                    spliney = interpolate.BSpline(t, cy, 3, extrapolate=False)
                    splinez = interpolate.BSpline(t, cz, 3, extrapolate=False)

                    if self.mode[j] == "TRAVELLING" or self.mode[j] == "GOAL-SEEN":
                        marker_array = self.make_marker_array(splinex, spliney, splinez, t[-1], j)
                        self.pub_traj_safe[j].publish(marker_array)
                        self.tf[j].append(t[-1] + t0)
                        self.start_tf[j].append(t[0] + t0)
                        self.spline_2_follow[j].append([splinex, spliney, splinez])
        """

        if len(self.data_save) == self.num:
            data_dir = "data" + str(self.data_num) + "/"

            makedirs(self.data_path + data_dir + "Input", exist_ok=True)
            makedirs(self.data_path + data_dir + "Label", exist_ok=True)

            vel = []
            traj = []
            acc = []
            goal = []
            max_num = 1
            max_num_drone = 1

            for j in range(self.num):
                if self.point_cloud[j].size != 0:
                    max_num = max([max_num, self.point_cloud[j].shape[0]])
                else:
                    self.point_cloud[j] = np.ones((1, 3))

                if self.point_cloud_drone[j].size != 0:
                    max_num_drone = max([max_num_drone, self.point_cloud_drone[j].shape[0]])
                else:
                    self.point_cloud_drone[j] = np.ones((1, 3))

            for j in range(self.num):
                self.point_cloud[j] = np.vstack(
                    (
                        self.point_cloud[j],
                        np.tile(
                            self.point_cloud[j][[-1], :],
                            [max_num - self.point_cloud[j].shape[0], 1],
                        ),
                    )
                )
                self.point_cloud_drone[j] = np.vstack(
                    (
                        self.point_cloud_drone[j],
                        np.tile(
                            self.point_cloud_drone[j][[-1], :],
                            [max_num_drone - self.point_cloud_drone[j].shape[0], 1],
                        ),
                    )
                )
                vel.append(self.data_save[j]["vel"])
                traj.append(self.data_save[j]["traj"])
                goal.append(self.data_save[j]["goal"])
                acc.append(self.data_save[j]["acc"])

            with open(self.data_path + data_dir + "Input" + "/point_cloud.npy", "wb") as f:
                pc = np.stack(self.point_cloud, axis=0)
                np.save(f, pc)

            with open(self.data_path + data_dir + "Input" + "/point_cloud_drone.npy", "wb") as f:
                pc = np.stack(self.point_cloud_drone, axis=0)
                np.save(f, pc)

            with open(self.data_path + data_dir + "Input" + "/vel.npy", "wb") as f:
                vel = np.stack(vel, axis=0)
                np.save(f, vel)

            with open(self.data_path + data_dir + "Input" + "/acc.npy", "wb") as f:
                acc = np.stack(acc, axis=0)
                np.save(f, acc)

            with open(self.data_path + data_dir + "Input" + "/quat.npy", "wb") as f:
                quat = np.stack(self.quat, axis=0)
                np.save(f, quat)

            with open(self.data_path + data_dir + "Input" + "/state.npy", "wb") as f:
                state = np.stack(self.state, axis=0)
                np.save(f, state)

            with open(self.data_path + data_dir + "Input" + "/goal.npy", "wb") as f:
                goal = np.stack(goal, axis=0)
                np.save(f, goal)

            with open(self.data_path + data_dir + "Label" + "/traj.npy", "wb") as f:
                traj = np.stack(traj, axis=0)
                np.save(f, traj)

            self.data_num += 1
            if self.data_num > 5000:
                rospy.signal_shutdown("finished")

        """
        if self.dynamic_points or self.static_points:
            pt_disp = np.array([])
            if disp_out != None:
                pt_disp = saliency_map(disp_out)
                pt_disp = torch.unflatten(pt_disp, 0, (1, self.num))[0, ...]
                pt_disp = pt_disp.cpu().numpy()
                # print(pt_disp.shape)
            for j in range(self.num):
                for i, p in enumerate(positions_t[j]):
                    pt_temp = p * scale_t[j][i] / lim
                    pt_temp = self.compute_pt(pt_temp, lim, j, pt_disp, i)

                    x_im = (pt_temp[0] + 1) * lim * 10 / 2
                    z_im = (pt_temp[2] + 1) * lim * 10 / 2
                    s = (scale_t[j][i] - 0.01) / (lim - 0.01)
                    # cv_image[int(round(x_im)), int(round(z_im)), 0] = 255 - (s * 255)
                    pt_temp[0] = pt_temp[0] * lim + self.state[j][0]
                    pt_temp[1] = pt_temp[1] * lim + self.state[j][1]
                    pt_temp[2] = pt_temp[2] * lim + self.state[j][2]

                    points.append(pt_temp)
                for i, p in enumerate(positions_p[j]):
                    pt_temp = p * scale_p[j][i] / lim
                    pt_temp = self.compute_pt(pt_temp, lim, j, pt_disp, len(positions_t[j]) + i)

                    x_im = (pt_temp[0] + 1) * lim * 10 / 2
                    z_im = (pt_temp[2] + 1) * lim * 10 / 2
                    s = (scale_p[j][i] - 0.01) / (lim - 0.01)
                    # cv_image[int(round(x_im)), int(round(z_im)), 0] = 255 - (s * 255)
                    pt_temp[0] = pt_temp[0] * lim + self.state[j][0]
                    pt_temp[1] = pt_temp[1] * lim + self.state[j][1]
                    pt_temp[2] = pt_temp[2] * lim + self.state[j][2]

                    points.append(pt_temp)

            fields = [
                PointField("x", 0, PointField.FLOAT32, 1),
                PointField("y", 4, PointField.FLOAT32, 1),
                PointField("z", 8, PointField.FLOAT32, 1),
                # PointField('rgb', 12, PointField.UINT32, 1),
                PointField("rgba", 12, PointField.UINT32, 1),
            ]
            header = Header()
            header.frame_id = "world"
            pc2_msg = point_cloud2.create_cloud(header, fields, points)
            self.pub_point_cloud.publish(pc2_msg)
        """
        """
            ## pub image
            
            cv_image = cv2.GaussianBlur(cv_image, (4, 4), cv2.BORDER_DEFAULT)
            cv_image = np.rint(cv_image)
            cv_image = cv_image.astype(np.uint8)
            image_message = self.bridge.cv2_to_imgmsg(cv_image, encoding="mono8")

            self.pub_image.publish(image_message)
        """

    def pub_clock_timer(self):
        while not rospy.is_shutdown():
            self.time_clock.clock = self.time_clock.clock + rospy.Duration.from_sec(0.0002)
            self.pub_clock.publish(self.time_clock)
            sleep(0.01)


if __name__ == "__main__":
    p = CreateDataset(int(sys.argv[1]))
    thread_clock = Thread(target=p.pub_clock_timer())
    thread_clock.start()
    rospy.spin()
    thread_clock.join()
