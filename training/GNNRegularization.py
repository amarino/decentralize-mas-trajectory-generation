import torch
import torch.nn as nn
import torch.nn.functional as f
from torch.linalg import matrix_norm, norm
from GNNlib import GGL, GraphLayer
import math


class GNNRegularization(object):
    def __init__(self, model, weight_list=[], regularization=[], C=1.0):

        self.model = model
        self.weights = weight_list
        self.regularization = regularization
        self.C = C

        self.GNNlayers = []
        for name, module in self.model.named_modules():
            if isinstance(module, GGL):
                self.GNNlayers.append(name)

        self.hard_tanh = nn.Hardtanh(max_val=10)

        self.data = {"L_norm": 1}

    @staticmethod
    def smooth_norm_inf(x, matrix=True):

        if matrix:
            tensor = torch.abs(x)
            tensor = torch.sum(tensor, dim=len(tensor.shape) - 1)
        else:
            tensor = torch.abs(x)

        c = torch.max(tensor)

        if c > 1e-3:
            k = 81 / (c.detach())

            tensor = torch.log(torch.sum(torch.exp(k * tensor), dim=len(tensor.shape) - 1)) / (k + 1e-12)
        else:
            tensor = c

        if torch.isnan(tensor):
            print("porco_reg")
        return tensor

    @staticmethod
    def smooth_norm_inf_bs(x, sequenced=True, batched=True, matrix=True):
        if sequenced & len(x.shape) > 3:
            raise RuntimeError(f"Tensor dimension {x.shape} but not sequenced")
        result = []

        if sequenced:
            for xx in x:
                result.append(GNNRegularization.smooth_norm_inf(xx, matrix))

            result = torch.cat(result, dim=0)
            return GNNRegularization.smooth_norm_inf(result, matrix=False)

        else:
            if batched:
                for xx in x:
                    result.append(GNNRegularization.smooth_norm_inf(xx, matrix))
                result = torch.Tensor(result)
            else:
                result = GNNRegularization.smooth_norm_inf(x, matrix)

            return GNNRegularization.smooth_norm_inf(result, matrix=False)

    def update_L(self, L: torch.Tensor) -> None:
        self.L = L

    def point_net(self):
        m3x3 = self.m3x3
        m64x64 = self.m64x64

        batch = m3x3.size(0)
        id3x3 = torch.eye(m3x3.shape[1], requires_grad=True).to(m3x3.device).repeat(batch, 1, 1)
        id64x64 = torch.eye(m64x64.shape[1], requires_grad=True).to(m3x3.device).repeat(batch, 1, 1)

        diff3x3 = id3x3[:batch,:,:] - torch.bmm(m3x3, m3x3.transpose(1, 2))
        diff64x64 = id64x64[:batch,:,:] - torch.bmm(m64x64, m64x64.transpose(1, 2))

        return sum(matrix_norm(diff3x3, ord=2) + matrix_norm(diff64x64, ord=2)) / float(batch)

    def module_GGNN(self, i):
        gg_layer = getattr(self.model, self.GNNlayers[i])
        H = gg_layer.H

        module_l = 0  # norm Laplacian

        module_gs = []  # module state gate
        module_gi = []  # module input gate
        module_gss = []  # module state-state gate
        module_gsi = []  # module input-state gate
        module_gis = []  # module state-input gate
        module_gii = []  # module input-input gate
        module_s = []  # module state transiction
        module_i = []  # module input transiction

        for j in range(H):
            # compute norm for the gates
            module_gss.append(gg_layer.weight_A_hat[:, :, j].mT)
            module_gsi.append(gg_layer.weight_B_hat[:, :, j].mT)

            module_gis.append(gg_layer.weight_A_tilde[:, :, j].mT)
            module_gii.append(gg_layer.weight_B_tilde[:, :, j].mT)

            # compute norm state-state transiction
            module_s.append(gg_layer.weight_A[:, :, j].mT)
            # compute norm input-state transiction
            module_i.append(gg_layer.weight_B[:, :, j].mT)

            # compute laplacian norm
            if j == 0:
                module_l = torch.Tensor([1]).to(device=self.model.device)
            else:
                L = torch.pow(self.L, j)
                module_l = module_l + torch.linalg.matrix_norm(L, ord=float('inf'))

        module_l = torch.max(module_l)
        if self.data["L_norm"] < module_l:
            self.data["L_norm"] = module_l
        module_l = self.data["L_norm"]
        module_gs = [torch.cat(module_gss, dim=0), torch.cat(module_gsi, dim=0)]
        module_gi = [torch.cat(module_gis, dim=0), torch.cat(module_gii, dim=0)]
        module_gsi = torch.cat(module_gsi, dim=0)
        module_gss = torch.cat(module_gss, dim=0)
        module_gis = torch.cat(module_gis, dim=0)
        module_gii = torch.cat(module_gii, dim=0)
        module_s = torch.cat(module_s, dim=0)
        module_i = torch.cat(module_i, dim=0)

        module_gi = (
            self.smooth_norm_inf_bs(module_gi[0], sequenced=False, batched=False, matrix=True) +
            self.smooth_norm_inf_bs(module_gi[1], sequenced=False, batched=False, matrix=True)
        )
        module_gs = (
            self.smooth_norm_inf_bs(module_gs[0], sequenced=False, batched=False, matrix=True) +
            self.smooth_norm_inf_bs(module_gs[1], sequenced=False, batched=False, matrix=True)
        )
        module_gss = self.smooth_norm_inf_bs(module_gss, sequenced=False, batched=False, matrix=True)
        module_gsi = self.smooth_norm_inf_bs(module_gsi, sequenced=False, batched=False, matrix=True)
        module_gis = self.smooth_norm_inf_bs(module_gis, sequenced=False, batched=False, matrix=True)
        module_gii = self.smooth_norm_inf_bs(module_gii, sequenced=False, batched=False, matrix=True)
        module_s = self.smooth_norm_inf_bs(module_s, sequenced=False, batched=False, matrix=True)
        module_i = self.smooth_norm_inf_bs(module_i, sequenced=False, batched=False, matrix=True)
        module_b_h = self.smooth_norm_inf_bs(
            gg_layer.bias_hat.repeat(self.L.size(2), 1), sequenced=False, batched=False, matrix=True
        )
        module_b_t = self.smooth_norm_inf_bs(
            gg_layer.bias_tilde.repeat(self.L.size(2), 1), sequenced=False, batched=False, matrix=True
        )

        delta_q_hat = torch.sigmoid(module_gs * module_l + module_b_h)
        delta_q_tilde = torch.sigmoid(module_gi * module_l + module_b_t)

        module_Ad = module_l * (
            delta_q_hat * module_s + 0.25 * module_l * module_gss * module_s + 0.25 * module_l * module_gis * module_i
        )

        module_Bd = module_l * (
            delta_q_tilde * module_i + 0.25 * module_l * module_gsi * module_s + 0.25 * module_l * module_gii * module_i
        )

        return module_Ad, module_Bd

    def regularization_v2s(self):
        """ incremental v2 system stability

        Returns:
            torch.Tensor: regularization loss
        """
        loss = 0
        linear = 1.0
        for i in range(len(self.GNNlayers)):
            module_Ad, module_Bd = self.module_GGNN(i)
            linear = linear * module_Bd

            if module_Ad < 1:
                module_Ad = 0.01 * module_Ad

            loss = loss + module_Ad

        for name, module in self.model.named_modules():
            if "input" in name or "readout" in name:
                m = torch.linalg.matrix_norm(module.weight, ord=float("inf"))
                linear = linear * m

        loss = loss + linear
        if loss < 1:
            loss = 0.01 * loss

        return loss

    def dGGNNS(self):
        """ incremental Graph Gated Neural Network stability

        Returns:
            torch.Tensor: regularization loss
        """
        loss = 0.0
        for i in range(len(self.GNNlayers)):
            module_Ad, _ = self.module_GGNN(i)

            if module_Ad < 1.0:
                module_Ad = 0.01 * module_Ad

            loss = loss + module_Ad

        if torch.isnan(loss):
            print("porco_reg")

        return loss

    def DG(self):
        """ incremental Graph Gated Neural Network stability

        Returns:
            torch.Tensor: regularization loss
        """
        loss = 0.0
        for i in range(len(self.GNNlayers)):
            gg_layer: GGL = getattr(self.model, self.GNNlayers[i])

            L = -torch.ones(self.L.shape, device=self.model.device)
            d = -(L.shape[2]) * torch.diag(L[0, 0, :])
            L = L + d

            q_hat_norm = torch.matmul(gg_layer.q_hat, L)
            q_hat_norm = torch.linalg.vector_norm(q_hat_norm, dim=2, ord=2)
            q_hat_norm = self.smooth_norm_inf(q_hat_norm)
            q_tilde_norm = torch.matmul(gg_layer.q_tilde, L)
            q_tilde_norm = torch.linalg.vector_norm(q_tilde_norm, dim=2, ord=2)
            q_tilde_norm = self.smooth_norm_inf(q_tilde_norm)

            dloss = torch.relu(1 - (q_hat_norm + q_tilde_norm) / 40)
            loss += dloss

        return loss

    def GNND(self):
        """Graph neural network discriminability

        Returns:
            torch.Tensor: regularization loss
        """
        def norm_module(self, module):
            module = sum(module)
            module = self.smooth_norm_inf_bs(module, sequenced=False, batched=False, matrix=True)

            module = module / self.C
            if module < 1:
                module = 0 * module
            return module

        def module_diff(weight, H):
            return [(i + 1) * weight[:, :, i + 1] * 2**i for i in range(H - 1)]

        module = 0.0

        for name, param in self.model.named_modules():
            if isinstance(param, GraphLayer):
                module_w = module_diff(param.weight, param.H)
                module_w = norm_module(self, module_w)

                module = module + module_w

                if torch.any(torch.isnan(param.weight)):
                    print("porco_reg")

            if isinstance(param, GGL):
                module_A = module_diff(param.weight_A, param.H)
                module_B = module_diff(param.weight_B, param.H)
                module_A_tilde = module_diff(param.weight_A_tilde, param.H)
                module_B_tilde = module_diff(param.weight_B_tilde, param.H)
                module_A_hat = module_diff(param.weight_A_hat, param.H)
                module_B_hat = module_diff(param.weight_B_hat, param.H)

                module_A = norm_module(self, module_A)
                module_B = norm_module(self, module_B)
                module_A_tilde = norm_module(self, module_A_tilde)
                module_A_hat = norm_module(self, module_A_hat)
                module_B_tilde = norm_module(self, module_B_tilde)
                module_B_hat = norm_module(self, module_B_hat)

                module = module + module_A + module_B + module_A_tilde + module_A_hat + module_B_tilde + module_B_hat

        return module

    def GNNFM(self):
        """Graph neural network filter module

        Returns:
            torch.Tensor: regularization loss
        """
        module = [self.model.gnn_layer.weight[:, :, i] * 2**i for i in range(self.model.H)]
        module = sum(module)
        module = torch.linalg.norm(module, ord=float('inf'))
        if module < 1:
            module = 0 * module
        return module

    def RNNS(self, ln):
        loss = 0.0
        for i in range(ln):
            # ii,if,ig,io
            weight_h = getattr(self.model.lstm, 'weight_hh_l' + str(i))
            loss = loss + torch.linalg.matrix_norm(weight_h, ord=2)
        return loss

    def LSTMS(self, ln):
        loss = 0.0
        for i in range(ln):
            # ii,if,ig,io
            weight_i = getattr(self.model.lstm, 'weight_ih_l' + str(i))
            weight_h = getattr(self.model.lstm, 'weight_hh_l' + str(i))
            bias_i = getattr(self.model.lstm, 'bias_ih_l' + str(i))
            bias_h = getattr(self.model.lstm, 'bias_hh_l' + str(i))

            module_o = self.smooth_norm_inf_bs(
                torch.cat(
                    [
                        weight_i[3 * self.model.hidden_size:, :], weight_h[3 * self.model.hidden_size:, :],
                        (bias_h[3 * self.model.hidden_size:] +
                         bias_i[3 * self.model.hidden_size:]).view(self.model.hidden_size, 1)
                    ],
                    dim=1
                ),
                sequenced=False,
                batched=False,
                matrix=True
            )
            module_f = self.smooth_norm_inf_bs(
                torch.cat(
                    [
                        weight_i[1 * self.model.hidden_size:2 * self.model.hidden_size, :],
                        weight_h[1 * self.model.hidden_size:2 * self.model.hidden_size, :],
                        (
                            bias_h[1 * self.model.hidden_size:2 * self.model.hidden_size] +
                            bias_i[1 * self.model.hidden_size:2 * self.model.hidden_size]
                        ).view(self.model.hidden_size, 1)
                    ],
                    dim=1
                ),
                sequenced=False,
                batched=False,
                matrix=True
            )
            module_i = self.smooth_norm_inf_bs(
                torch.cat(
                    [
                        weight_i[:1 * self.model.hidden_size, :], weight_h[:1 * self.model.hidden_size, :],
                        (bias_h[:1 * self.model.hidden_size] +
                         bias_i[:1 * self.model.hidden_size]).view(self.model.hidden_size, 1)
                    ],
                    dim=1
                ),
                sequenced=False,
                batched=False,
                matrix=True
            )
            module_g = torch.linalg.matrix_norm(
                weight_h[2 * self.model.hidden_size:3 * self.model.hidden_size, :], ord=2
            )

            loss_1 = torch.sigmoid(module_f)
            loss_2 = loss_1 + torch.sigmoid(module_o) *\
                torch.sigmoid(module_i) * module_g
            if loss_2 < 1:
                loss_2 = 0.1 * loss_2

            loss = loss + loss_2
        return loss

    def LSTMdS(self, ln):
        loss = 0.0
        for i in range(ln):
            # ii,if,ig,io
            weight_i = getattr(self.model.lstm, 'weight_ih_l' + str(i))
            weight_h = getattr(self.model.lstm, 'weight_hh_l' + str(i))
            bias_i = getattr(self.model.lstm, 'bias_ih_l' + str(i))
            bias_h = getattr(self.model.lstm, 'bias_hh_l' + str(i))

            (Wi, Wf, Wc, Wo) = (
                weight_i[t:t + self.model.hidden_size, :]
                for t in range(0, 4 * self.model.hidden_size, self.model.hidden_size)
            )

            (Ui, Uf, Uc, Uo) = (
                weight_h[t:t + self.model.hidden_size, :]
                for t in range(0, 4 * self.model.hidden_size, self.model.hidden_size)
            )

            (bi, bf, bc, bo) = (
                bias_h[t:t + self.model.hidden_size] + bias_i[t:t + self.model.hidden_size]
                for t in range(0, 4 * self.model.hidden_size, self.model.hidden_size)
            )

            module_i = torch.cat([Wi, Ui, bi.view(self.model.hidden_size, 1)], dim=1)
            module_i = self.smooth_norm_inf_bs(module_i, sequenced=False, batched=False, matrix=True)
            # module_i = torch.linalg.matrix_norm(module_i, ord=float('inf'))
            module_i = torch.sigmoid(module_i)
            module_f = torch.cat([Wf, Uf, bf.reshape(self.model.hidden_size, 1)], dim=1)
            module_f = self.smooth_norm_inf_bs(module_f, sequenced=False, batched=False, matrix=True)
            # module_f = torch.linalg.matrix_norm(module_f, ord=float('inf'))
            module_f = torch.sigmoid(module_f)
            module_o = torch.cat([Wo, Uo, bo.reshape(self.model.hidden_size, 1)], dim=1)
            module_o = self.smooth_norm_inf_bs(module_o, sequenced=False, batched=False, matrix=True)
            # module_o = torch.linalg.matrix_norm(module_o, ord=float('inf'))
            module_o = torch.sigmoid(module_o)
            module_c = torch.cat([Wc, Uc, bc.reshape(self.model.hidden_size, 1)], dim=1)
            module_c = self.smooth_norm_inf_bs(module_c, sequenced=False, batched=False, matrix=True)
            # module_c = torch.linalg.matrix_norm(module_c, ord=float('inf'))
            module_c = torch.tanh(module_c)

            module_x = module_i * module_c / (1 - module_f + 1e-12)
            module_x = self.hard_tanh(module_x)
            module_x_tanh = torch.tanh(module_x)
            alpha = 0.25 * torch.linalg.matrix_norm(Uf, ord=2) * module_x + module_i * \
                torch.linalg.matrix_norm(Uc, ord=2) + 0.25 * \
                torch.linalg.matrix_norm(Ui, ord=2) * module_c

            loss_1 = -1 + module_f + alpha * module_o + \
                0.25 * module_x_tanh * torch.linalg.matrix_norm(Uo, ord=2)

            loss_2 = module_f * 0.25 * module_x_tanh * torch.linalg.matrix_norm(Uo, ord=2)

            if loss_1 < loss_2:
                loss_1 = 0.01 * loss_1

            if loss_1 < 0:
                loss_1 = 0 * loss_1

            if loss_2 < 1:
                loss_2 = 0.01 * loss_2

            loss = loss + loss_1 + loss_2
        return loss


    def __call__(self):
        assert (len(self.regularization) == len(self.weights))

        loss = torch.Tensor([0.]).to(self.model.device)
        for i, reg in enumerate(self.regularization):
            if reg == self.LSTMdS or reg == self.LSTMS or reg == self.RNNS:
                loss = loss + self.weights[i] * reg(self.model.lstm_layer)
            else:
                loss = loss + self.weights[i] * reg()

        if torch.isnan(loss):
            print("porco_reg")

        return loss
