#!/usr/bin/env python3
"""GNNlib

  Gaprh neural network library.

  This package defines graph neural network models, to estimate quantities
  distributed on graphs. GNN and GGNN have included an attention mechanisms on the 
  communicated quantites.  
"""

import torch
import torch.nn as nn
from typing import Optional, Tuple

# import torchvision.transforms as transforms

import torch.nn.init as init
from torch import Tensor
import torch.nn.functional as F
import math

from torch.nn import Sequential, Linear, ReLU
from torch_geometric.nn.dense import DenseGATConv
from torch_geometric.nn import PointNetConv


class PointNetpp(torch.nn.Module):
    def __init__(self, device, dim_out):
        super().__init__()

        self.input = nn.Sequential(Linear(3, 16), nn.LeakyReLU())
        self.classifier = nn.Sequential(Linear(16, dim_out))
        self.conv1 = PointNetConv(self.input)

    def forward(self, pc, A):
        # Compute the kNN graph:
        # Here, we need to pass the batch vector to the function call in order
        # to prevent creating edges between points of different examples.
        # We also add `loop=True` which will add self-loops to the graph in
        # order to preserve central point information.

        n = pc.shape[0]
        nodes = pc.shape[1]
        pc = torch.flatten(pc, start_dim=0, end_dim=1)
        h = self.conv1(None, pc, A.t())
        # h = h.relu()
        # h = self.conv2(h, A.t())
        # h = h.relu()

        # 4. Global Pooling.
        h = h.reshape(n, nodes, h.shape[1])
        h = F.max_pool1d(h.mT, kernel_size=nodes, stride=1)
        h = torch.flatten(h, start_dim=1)
        # 5. Classifier.
        return self.classifier(h), None, None


class Tnet(nn.Module):
    def __init__(self, device=torch.device("cpu"), k=3):
        super().__init__()
        self.k = k
        self.conv1 = GraphLinear(k, 32)  # nn.Conv1d(k, 64, 1)
        self.conv2 = GraphLinear(32, 64)  # nn.Conv1d(64, 128, 1)
        self.conv3 = GraphLinear(64, 512)  # nn.Conv1d(128, 512, 1)
        self.fc1 = nn.Linear(512, 64)
        self.fc2 = nn.Linear(64, 128)
        self.fc3 = nn.Linear(128, k * k)

        self.bn1 = nn.BatchNorm1d(64)
        self.bn2 = nn.BatchNorm1d(256)
        self.bn3 = nn.BatchNorm1d(1024)
        self.bn4 = nn.BatchNorm1d(256)
        self.bn5 = nn.BatchNorm1d(128)

        self.leaky_relu = nn.LeakyReLU()
        self.init = torch.eye(self.k, requires_grad=True).to(device)

    def forward(self, input: Tensor):
        # input.shape == (bs,n,3)
        bs = input.shape[0]
        n = input.shape[1]
        xb = self.leaky_relu(self.conv1(input))
        xb = self.leaky_relu(self.conv2(xb))
        # xb = self.leaky_relu(self.conv3(xb))
        xb = F.max_pool1d(xb.mT, kernel_size=n, stride=1)
        xb = torch.flatten(xb, start_dim=1)
        # xb = self.leaky_relu(self.fc1(xb))
        xb = self.leaky_relu(self.fc2(xb))

        # initialize as identity
        init = self.init.repeat(bs, 1, 1)

        # add identity to the output
        matrix = self.fc3(xb).view(-1, self.k, self.k) + init
        return matrix


class Transform(nn.Module):
    def __init__(self, device=torch.device("cpu")):
        super().__init__()
        self.input_transform = Tnet(device, k=3)
        self.feature_transform = Tnet(device, k=32)
        self.conv1 = GraphLinear(3, 32)  # nn.Conv1d(3, 64, 1)
        self.conv2 = GraphLinear(32, 64)  # nn.Conv1d(64, 128, 1)
        self.conv3 = GraphLinear(64, 128)  # nn.Conv1d(128, 512, 1)

        self.bn1 = nn.BatchNorm1d(64)
        self.bn2 = nn.BatchNorm1d(256)
        self.bn3 = nn.BatchNorm1d(512)

        self.leaky_relu = nn.LeakyReLU()

    def forward(self, input):
        disp_out = []
        disp_out.append(input)
        matrix3x3 = self.input_transform(input)  # .transpose(1, 2))
        # batch matrix multiplication
        xb = torch.bmm(input, matrix3x3)  # .transpose(1, 2)
        xb = self.leaky_relu(self.conv1(xb))
        disp_out.append(xb)
        matrix64x64 = self.feature_transform(xb)
        xb = torch.bmm(xb, matrix64x64)  # .transpose(1, 2)
        xb = self.leaky_relu(self.conv2(xb))
        disp_out.append(xb)
        xb = self.leaky_relu(self.conv3(xb))
        disp_out.append(xb)
        out = F.max_pool1d(xb.mT, kernel_size=xb.size(1), stride=1)
        out = torch.flatten(out, start_dim=1)
        return out, matrix3x3, matrix64x64, xb, disp_out


class PointNet(nn.Module):
    def __init__(self, device=torch.device("cpu"), dim_out=10):
        super().__init__()
        self.transform = Transform(device)

    def forward(self, input):
        xb, matrix3x3, matrix64x64, out_pmax, disp_out = self.transform(input)
        return xb, matrix3x3, matrix64x64, out_pmax, disp_out


class GraphLinear(nn.Module):
    def __init__(self, G: int, F: int, bias: bool = True) -> None:
        super(GraphLinear, self).__init__()
        self.use_bias = bias
        self.linear = nn.parameter.Parameter(torch.Tensor(G, F))
        if self.use_bias:
            self.bias = nn.parameter.Parameter(torch.Tensor(F))

    def initialize_parameters(self) -> None:
        """initialize_parameters

        Init layers weights and biases
        """
        stdv = 1.0 / math.sqrt(self.H)
        for name, param in self.named_parameters():
            if param.requires_grad:
                if len(param.shape) != 1:
                    torch.nn.init.orthogonal_(param, gain=1)
            if "bias" in name:
                param.data.fill_(0)

    def forward(self, x) -> Tensor:
        x = torch.matmul(x, self.linear) + self.bias
        return x


class GraphLayer(nn.Module):
    def __init__(self, H, G, F, bias=True, direct_input=False, attention=False, autoencoder=False) -> None:
        super(GraphLayer, self).__init__()

        self.G = G
        self.F = F
        self.H = H
        self.direct_input = direct_input
        self.attention = attention
        self.autoencoder = autoencoder
        self.use_bias = bias
        self.weight = nn.parameter.Parameter(torch.Tensor(F, G, H))
        self.encoding_size = 5

        # attention
        if self.autoencoder:
            self.a_l1 = nn.Linear(2 * self.encoding_size, 50)
        else:
            self.a_l1 = nn.Linear(G * 2, 50)
        self.a_l2 = nn.Linear(50, 50)
        self.a_l3 = nn.Linear(50, 1)

        # autoencoder
        self.ae_depth = 4
        self.e_input = nn.ModuleList(
            [GraphLinear(G, 128), GraphLinear(128, 64), GraphLinear(64, 32), GraphLinear(32, self.encoding_size)]
        )
        self.d_input = nn.ModuleList(
            [GraphLinear(self.encoding_size, 32), GraphLinear(32, 64), GraphLinear(64, 128), GraphLinear(128, G)]
        )

        self.softmax = torch.nn.Softmax(dim=2)
        self.leaky_relu = torch.nn.LeakyReLU()
        self.bias = nn.parameter.Parameter(torch.Tensor(F))

        self.initialize_parameters()

    def initialize_parameters(self) -> None:
        """initialize_parameters

        Init layers weights and biases
        """
        stdv = 1.0 / math.sqrt(self.H)
        for name, param in self.named_parameters():
            if param.requires_grad:
                if len(param.shape) != 1:
                    torch.nn.init.orthogonal_(param, gain=1)
            if "bias" in name:
                param.data.fill_(0)

    @staticmethod
    def reshape(x, x_com, w):
        """reshape

        reshape method to shape data before aggregation step

        Args:
            x (torch.Tensor): robot data size [batch x k x 1]
            x_com (torch.Tensor): neighbots data size [batch x k x N]

        Returns:
            torch.Tensor: tensor of size [batch x k x N]
        """
        return w(x - x_com)

    @staticmethod
    def combine(x):
        """combine function

        Args:
            x (torch.Tensor): data to combine size batch_size x k x N

        Returns:
            torch.Tensor: tensor of size batch_size x k x 1
        """
        return torch.sum(x, 2)

    def consensus(self, x: Tensor, x_com: Tensor):
        """consensus function

        Args:
            x (torch.Tensor): local data (batch_size x k)
            x_com (torch.Tensor): communicated data (batch_size x Ni x k)

        Returns:
            torch.Tensor: combined data (batch_size x k)
        """
        x = x.unsqueeze(dim=1)
        x = x.repeat(1, x_com.shape[1], 1)
        w = torch.ones_like(x) / x_com.shape[1]
        if self.attention:
            dl = torch.cat([x, x_com], dim=2)
            dl = torch.flatten(dl, start_dim=0, end_dim=1)
            dl = self.leaky_relu(self.a_l1(dl))
            dl = self.leaky_relu(self.a_l2(dl))
            dl = self.a_l3(dl)
            dl = torch.unflatten(dl, 0, (x.shape[0], x.shape[1]))
            w = torch.softmax(dl, dim=dl.shape[-1])
        x_share = GraphLayer.reshape(x, x_com, w)
        data = GraphLayer.combine(x_share)
        return data

    def attention_weight(self, data: Tensor, L: Tensor):
        batch = L.shape[0]
        N = L.shape[1]
        Lt = (L == 0).int()
        Ln = (L != 0).int()
        Ln = Ln.reshape(batch, N, N, 1)
        L = L.reshape(batch, N, N, 1)

        data_N = data.reshape(batch, 1, N, data.shape[2])
        data_N = data_N.repeat(1, N, 1, 1)

        data = data.reshape(batch, N, 1, data.shape[2])
        data = data.repeat(1, 1, N, 1)

        data_N = Ln * data_N
        data = Ln * data

        dl = torch.cat([data_N, data], dim=3)

        dl = torch.flatten(dl, start_dim=0, end_dim=2)

        dl = self.leaky_relu(self.a_l1(dl))
        dl = self.leaky_relu(self.a_l2(dl))
        dl = self.a_l3(dl)

        dl = torch.unflatten(dl, 0, (batch, N, N))
        dl = dl * L
        dl = dl.reshape(batch, N, N)

        dl = torch.exp(dl)
        dl = dl - Lt
        dl = dl / torch.sum(dl, dim=2, keepdim=True)
        L = dl

        return L

    def encoder(self, data):
        for i, ei in enumerate(self.e_input):
            if i == self.ae_depth - 1:
                data = ei(data)
            else:
                data = torch.relu(ei(data))
        return data

    def decoder(self, data):
        for i, di in enumerate(self.d_input):
            if i == self.ae_depth - 1:
                data = di(data)
            else:
                data = torch.relu(di(data))
        return data

    def graph_filter_step(
        self, data: Tensor, L: Tensor, i: int, out: Tensor, data_com: Optional[Tensor] = None
    ) -> Tuple[Tensor, Tensor]:
        if i > 0:
            if self.autoencoder:
                data = self.encoder(data)
            if data.size(1) > 1 or L.size(1) == 1:
                if i == 1 and self.attention:
                    L = self.attention_weight(data, L)
                data = torch.matmul(L, data)
            else:
                if data_com is not None:
                    data = GraphLayer.consensus(data, data_com)
                else:
                    raise RuntimeError(f"No communicated data for tap {i}")

            if self.autoencoder:
                data = self.decoder(data)

        out = out + torch.matmul(data, self.weight[:, :, i].mT)

        return data, out, L

    def forward(self, x: Tensor, L: Tensor) -> Tensor:
        out = torch.zeros((x.shape[0], x.shape[1], self.F), device=x.device)

        if self.direct_input:
            start = 0
        else:
            start = 1

        for i in range(start, self.H):
            x, out, L = self.graph_filter_step(x, L, i, out)

        if self.use_bias:
            out = out + self.bias.reshape(1, 1, self.F)

        return out


class GGL(nn.Module):
    def __init__(self, H: int = 0, G: int = 1, F: int = 1, attention=False, autoencoder=True):
        """Graph Gated Layer

        Args:
            H (int): number of aggregaton steps. Defaults to 0.
            F (int): number of output features. Defaults to 1
            G (int): number of input features. Defaults to 1
        """
        super(GGL, self).__init__()

        self.H = H
        self.F = F
        self.G = G
        self.attention = attention
        self.autoencoder = autoencoder

        self.weight_A = nn.parameter.Parameter(torch.Tensor(F, F, H))
        self.weight_B = nn.parameter.Parameter(torch.Tensor(F, G, H))
        self.weight_A_tilde = nn.parameter.Parameter(torch.Tensor(F, F, H))
        self.weight_B_tilde = nn.parameter.Parameter(torch.Tensor(F, G, H))
        self.weight_A_hat = nn.parameter.Parameter(torch.Tensor(F, F, H))
        self.weight_B_hat = nn.parameter.Parameter(torch.Tensor(F, G, H))

        self.bias_hat = nn.parameter.Parameter(torch.Tensor(F))
        self.bias_tilde = nn.parameter.Parameter(torch.Tensor(F))
        self.bias = nn.parameter.Parameter(torch.Tensor(F))

        # attention
        if self.autoencoder:
            self.prova = nn.parameter.Parameter(torch.Tensor(2, 2))
            self.prova_h = nn.parameter.Parameter(torch.Tensor(2, 2))
        else:
            self.prova = nn.parameter.Parameter(torch.Tensor(G, G))
            self.prova_h = nn.parameter.Parameter(torch.Tensor(F, F))

        if self.autoencoder:
            self.a_l1 = nn.Linear(4, 50)
        else:
            self.a_l1 = nn.Linear(G * 2, 50)
        self.a_l2 = nn.Linear(50, 50)
        self.a_l3 = nn.Linear(50, 1)

        # autoencoder
        self.ae_depth = 4
        self.e_input = nn.ModuleList(
            [GraphLinear(G, 512), GraphLinear(512, 128), GraphLinear(128, 32), GraphLinear(32, 2)]
        )
        self.d_input = nn.ModuleList(
            [GraphLinear(2, 32), GraphLinear(32, 128), GraphLinear(128, 512), GraphLinear(512, G)]
        )
        self.e_state = nn.ModuleList(
            [GraphLinear(F, 512), GraphLinear(512, 128), GraphLinear(128, 32), GraphLinear(32, 2)]
        )
        self.d_state = nn.ModuleList(
            [GraphLinear(2, 32), GraphLinear(32, 128), GraphLinear(128, 512), GraphLinear(512, F)]
        )

        self.leaky_relu = nn.LeakyReLU()
        self.softmax = torch.nn.Softmax(dim=2)

        self.gates = (
            torch.zeros((1, 1, self.F)),
            torch.zeros((1, 1, self.F)),
            torch.zeros((1, 1, self.F)),
            torch.zeros((1, 1, self.F)),
            torch.zeros((1, 1, self.F)),
            torch.zeros((1, 1, self.F)),
        )

        # init parameters
        self.initialize_parameters()

    def initialize_parameters(self) -> None:
        """initialize_parameters

        Init layers weights and biases
        """

        for name, param in self.named_parameters():
            if param.requires_grad:
                if len(param.shape) != 1:
                    torch.nn.init.orthogonal_(param, gain=0.1)
            if "bias" in name:
                param.data.fill_(0)

        for layer in self.modules():
            if isinstance(layer, nn.Linear):
                init.xavier_normal_(layer.weight.data)
                if layer.bias is not None:
                    init.normal_(layer.bias.data)

    def attention_weight(self, data: Tensor, L: Tensor):
        batch = L.shape[0]
        N = L.shape[1]
        G = data.shape[2]
        Lt = (L == 0).int()
        Ln = (L != 0).int()
        Ln = Ln.reshape(batch, N, N, 1)
        L = L.reshape(batch, N, N, 1)

        data_N = data.reshape(batch, 1, N, data.shape[2])
        data_N = data_N.repeat(1, N, 1, 1)

        data = data.reshape(batch, N, 1, data.shape[2])
        data = data.repeat(1, 1, N, 1)

        data_N = Ln * data_N
        data = Ln * data

        dl = torch.cat([data_N, data], dim=3)

        dl = torch.flatten(dl, start_dim=0, end_dim=2)

        dl = self.leaky_relu(self.a_l1(dl))
        dl = self.leaky_relu(self.a_l2(dl))
        dl = self.a_l3(dl)

        dl = torch.unflatten(dl, 0, (batch, N, N))
        dl = dl * L
        dl = dl.reshape(batch, N, N)

        dl = torch.exp(dl)
        dl = dl - Lt
        dl = dl / torch.sum(dl, dim=2, keepdim=True)
        L = dl

        return L

    def encoder(self, data, hidden):
        for i in range(self.ae_depth - 1):
            data = torch.relu(self.e_input[i](data))
            hidden = torch.relu(self.e_state[i](hidden))
        return self.e_input[self.ae_depth](data), self.e_state[self.ae_depth](hidden)

    def decoder(self, data, hidden):
        for i in range(self.ae_depth - 1):
            data = torch.relu(self.d_input[i](data))
            hidden = torch.relu(self.d_state[i](hidden))
        return self.d_input[self.ae_depth](data), self.d_state[self.ae_depth](hidden)

    def graph_filter_step(
        self,
        x: Tuple[Tensor, Tensor],
        L: Tensor,
        Lh: Tensor,
        i: int,
        gates: Tuple[Tensor, Tensor, Tensor, Tensor, Tensor, Tensor],
        data_com: Optional[Tuple[Tensor, Tensor]] = None,
    ) -> Tuple[Tuple[Tensor, Tensor], Tuple[Tensor, Tensor, Tensor, Tensor, Tensor, Tensor]]:
        """This method computes the communicated data and build up the filters components i
            of length H. The components are summed directly to the previous components 0...i-1
            the output of the graph filter is of dimension  [batch,F,Nr]

        Args:
            x Tuple(Tensor, Tensor) : input data, hidden state ([batch, G ,Nr],[batch, G ,Nr])
            L (torch.Tensor): Laplacian, [batch, Nr , Nr]
            i (int): filter tap
            gates Tuple(Tensor,Tensor,Tensor,Tensor,Tensor,Tensor) : gates of dimension [batch, F, Nr]
            data_com Optional(Tuple(Tensor,Tensor)): communicated data and hidden state

        Returns:
            Tuple(torch.Tensor, torch.Tensor) : data, hidden after communication step; i=0 they are equal to the input
            Tuple(Tensor, Tensor, Tensor, Tensor, Tensor, Tensor) : gates filters updates
        """

        data, hidden = x
        qth, qtu, qhh, qhu, hx, hu = gates

        if i > 0:
            if data.size(1) > 1 or L.size(1) == 1:
                if self.autoencoder:
                    data, hidden = self.encoder(data, hidden)
                if i == 1 and self.attention:
                    L = self.attention_weight(data, L)
                    Lh = self.attention_weight(hidden, Lh)
                data = torch.matmul(L, data)
                hidden = torch.matmul(Lh, hidden)
                if self.autoencoder:
                    data, hidden = self.decoder(data, hidden)
            else:
                if (data_com is not None) and data_com[0] and data_com[1]:
                    data = GraphLayer.consensus(data, data_com[0])
                    hidden = GraphLayer.consensus(hidden, data_com[1])
                else:
                    raise RuntimeError(f"No communicated data for tap {i}")

        qth = qth + torch.matmul(hidden, self.weight_A_tilde[:, :, i].mT)
        qtu = qtu + torch.matmul(data, self.weight_B_tilde[:, :, i].mT)
        qhh = qhh + torch.matmul(hidden, self.weight_A_hat[:, :, i].mT)
        qhu = qhu + torch.matmul(data, self.weight_B_hat[:, :, i].mT)
        hx = hx + torch.matmul(hidden, self.weight_A[:, :, i].mT)
        hu = hu + torch.matmul(data, self.weight_B[:, :, i].mT)

        return (data, hidden), (qth, qtu, qhh, qhu, hx, hu), L, Lh

    def input_cell(self, x: Tensor, hidden: Optional[Tensor] = None):
        if hidden is None:
            hidden = torch.zeros((x.shape[0], self.F, x.shape[2]), device=x.device)

        self.gates = (
            torch.zeros(hidden.size(), device=x.device),
            torch.zeros(hidden.size(), device=x.device),
            torch.zeros(hidden.size(), device=x.device),
            torch.zeros(hidden.size(), device=x.device),
            torch.zeros(hidden.size(), device=x.device),
            torch.zeros(hidden.size(), device=x.device),
        )

        return hidden

    def output_cell(self):
        qth, qtu, qhh, qhu, hx, hu = self.gates

        q_tilde = torch.sigmoid(qth + qtu + self.bias_tilde.reshape(1, 1, self.F))
        q_hat = torch.sigmoid(qhh + qhu + self.bias_hat.reshape(1, 1, self.F))

        hx = torch.mul(q_hat, hx)
        hu = torch.mul(q_tilde, hu)

        out = torch.tanh(hx + hu + self.bias.reshape(1, 1, self.F))

        return out

    def cell(self, x: Tensor, L: Tensor, hidden: Optional[Tensor] = None) -> Tensor:
        hidden = self.input_cell(x, hidden)
        Lh = L
        for i in range(self.H):
            (x, hidden), self.gates, L, Lh = self.graph_filter_step((x, hidden), L, Lh, i, self.gates)

        out = self.output_cell()

        return out

    def forward(self, x: Tensor, L: Tensor, hidden: Optional[Tensor] = None) -> Tensor:
        """forward

        Args:
            x (torch.Tensor): output of previous aggregation layers

            hidden (torch.Tensor): estimation average

        Returns:
           torch.Tensor: tensor of size batch_size x 1
        """
        if len(x.shape) < 4:
            x = torch.unsqueeze(x, dim=0)
            L = torch.unsqueeze(L, dim=0)
        else:
            x = x.transpose(0, 1)
            L = L.transpose(0, 1)

        results = []
        out = 0.0
        for i, xx in enumerate(x):
            hidden = self.cell(x=xx, L=L[i, :, :, :], hidden=hidden)
            results.append(hidden)

        out = torch.stack(results, 0)

        return out


class GraphNetLambda2(nn.Module):
    def __init__(self, device: torch.device = torch.device("cpu")):
        """_summary_

        Args:
            H (float, optional): number of aggregaton steps. Defaults to 0.0.
            device (torch.device, optional): device on which execute the model. Defaults to torch.device("cpu").
        """
        super(GraphNetLambda2, self).__init__()

        self.H = 3
        self.F = 30

        self.input_1 = nn.Linear(1, 150)
        self.input_2 = nn.Linear(150, 2 * self.F)

        self.linear_1 = nn.Linear(self.F, 150)
        self.linear_2 = nn.Linear(150, 2 * self.F)

        self.ggn_layer1 = GraphLayer(self.H, self.F, self.F, bias=True, direct_input=True)
        self.ggn_layer2 = GraphLayer(self.H, self.F, self.F, bias=True, direct_input=True)

        self.readout_1 = nn.Linear(self.F, 150)
        self.readout_2 = nn.Linear(150, 1)

        self.batch_norm = nn.LayerNorm(1, eps=1e-03, momentum=0.1)

        self.hardtanh = nn.Sigmoid()
        self.glu = nn.GLU()
        self.relu = nn.GELU()
        self.device = device
        # init parameters
        self.initialize_parameters()

    def initialize_parameters(self):
        """initialize_parameters

        Init layers weights and biases
        """
        for layer in self.modules():
            if isinstance(layer, nn.Linear):
                init.xavier_normal_(layer.weight.data)
                if layer.bias is not None:
                    init.normal_(layer.bias.data)

    def input_process(self, data):
        self.N = data.size(1)
        self.batch_size = data.size(0)

        data = torch.flatten(data, start_dim=0, end_dim=1)

        # data = self.batch_norm(data)

        data = self.relu(self.input_1(data))
        data = self.glu(self.input_2(data))

        data = torch.unflatten(data, 0, (self.batch_size, self.N))
        data = torch.permute(data, (0, 2, 1))

        return data

    def middle1_process(self, data):
        data = torch.permute(data, (2, 0, 1))
        data = torch.flatten(data, start_dim=0, end_dim=1)

        data = self.relu(self.linear_1(data))
        data = self.glu(self.linear_2(data))

        data = torch.unflatten(data, 0, (self.N, self.batch_size))
        data = torch.permute(data, (1, 2, 0))

        return data

    def output_process(self, data):
        data = torch.permute(data, (2, 0, 1))
        data = torch.flatten(data, start_dim=0, end_dim=1)

        data = self.relu(self.readout_1(data))
        data = 2 * self.hardtanh(self.readout_2(data))

        data = torch.unflatten(data, 0, (self.N, self.batch_size))
        data = torch.permute(data, (1, 0, 2))

        return data

    def forward(self, data, L):
        """forward

        Args:
            x_v2 (torch.Tensor): output of previous aggregation layers
            avg (torch.Tensor): estimation average
            avg_2 (torch.Tensor): square of the estimation average
            data (torch.Tensor): current estimation

        Returns:
           torch.Tensor: tensor of size batch_size x 1
        """

        data = self.input_process(data)
        data = self.relu(self.ggn_layer1(data, L))

        data = self.middle1_process(data)
        data = self.relu(self.ggn_layer2(data, L))

        # data = self.middle2_process(data)
        # data = torch.tanh(self.ggn_layer3(data, L))

        out = self.output_process(data)
        return out


class LogCoshLoss(torch.nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, y_t, y_prime_t):
        ey_t = y_t - y_prime_t
        return torch.mean(torch.log(torch.cosh(ey_t + 1e-12)))


class XTanhLoss(torch.nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, y_t, y_prime_t):
        ey_t = y_t - y_prime_t
        return torch.mean(ey_t * torch.tanh(ey_t))


class XSigmoidLoss(torch.nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, y_t, y_prime_t):
        ey_t = y_t - y_prime_t
        # return torch.mean(2 * ey_t / (1 + torch.exp(-ey_t)) - ey_t)
        return torch.mean(2 * ey_t * torch.sigmoid(ey_t) - ey_t)


class LossFunction(object):
    def __init__(self, label=None) -> None:
        self.update_label(label)
        self.criterion = nn.MSELoss(reduction="mean")
        self.criterionl2 = nn.SmoothL1Loss(reduction="mean", beta=0.01)
        self.criterionLog = LogCoshLoss()

    def update_label(self, label=None) -> None:
        self.label = label
        if label is not None:
            self.projector = torch.matmul(label[:, :, :1], label[:, :, :1].mT)

    def update_L(self, L: torch.Tensor) -> None:
        self.L = L
