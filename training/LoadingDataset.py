import numpy as np
import os
import random
from natsort import natsorted
from typing import Optional, Tuple, Any
from abc import abstractmethod
from tqdm import tqdm
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from torch import Tensor
import torch
import copy
from sklearn.neighbors import radius_neighbors_graph
from torch_geometric.nn import DMoNPooling
from torch_sparse import SparseTensor
from torch_geometric.utils import from_scipy_sparse_matrix, dense_to_sparse, to_torch_csc_tensor
import sparse


class TrajDataset(Dataset):
    def __init__(self, src_folder, train_num=0.8, valid_num=0.1, seed=None):
        super().__init__()
        assert valid_num < train_num

        self.src_folder = src_folder
        np.random.seed(seed)

        self.files = []
        self.train_files = []
        self.valid_files = []
        self.test_files = []

        self.train_length = 0
        self.valid_length = 0
        self.test_length = 0

        data = natsorted(os.listdir(self.src_folder))

        self.num_data = len(data)
        files_shuffle = np.random.permutation(self.num_data).tolist()
        train_len = round(self.num_data * train_num)
        valid_len = round(self.num_data * valid_num)

        self.train_files = [data[i] for i in files_shuffle[:train_len]]
        self.valid_files = [data[i] for i in files_shuffle[train_len : train_len + valid_len]]
        self.test_files = [data[i] for i in files_shuffle[train_len + valid_len :]]

        self.train_length = len(self.train_files)
        self.valid_length = len(self.valid_files)
        self.test_length = len(self.test_files)

        self.mode = "train"

    def __len__(self):
        if self.mode == "train":
            return self.train_length
        if self.mode == "valid":
            return self.valid_length
        if self.mode == "test":
            return self.test_length
        return self.num_data  # required


class SingleTrajDataset(TrajDataset):
    def __init__(self, src_folder, train_num=0.8, valid_num=0.1, seed=None):
        self.map_lim = 5
        super().__init__(src_folder, train_num, valid_num, seed)

        self.goal_mean, self.goal_std = (1.0, 0.0)
        self.vel_mean, self.vel_std = (1.0, 0.0)

    def data_processing(self, point_cloud, A, goal, quat, vel, acc, trajlabel):
        point_cloud = point_cloud.astype("float32")
        goal = goal.astype("float32")
        quat = quat.astype("float32")
        vel = vel.astype("float32")
        acc = acc.astype("float32")
        trajlabel = trajlabel.astype("float32")
        A = A.astype("float32")
        """
        if point_cloud.size != 0:
            A = radius_neighbors_graph(point_cloud, 0.5, mode="connectivity", include_self=True).toarray()
            D = np.diag(np.sum(A, axis=1)**(-1/2))
            A = D@A@D
        else:
            A = np.array([[0.0]])
        """
        return point_cloud, A, goal, quat, vel, acc, trajlabel

    def __getitem__(self, idx):
        assert idx <= self.num_data

        if self.mode == "train":
            files = self.train_files[idx]
        elif self.mode == "test":
            files = self.test_files[idx]
        elif self.mode == "valid":
            files = self.valid_files[idx]
        else:
            raise RuntimeError("Wrong mode!!! value accepted are 'train', 'test', 'valid'")

        DataFolder = self.src_folder + files + "/Input"
        LabelFolder = self.src_folder + files + "/Label"

        with open(DataFolder + "/point_cloud.npy", "rb") as f:
            point_cloud = np.load(f)

        with open(DataFolder + "/goal.npy", "rb") as f:
            goal = np.load(f)

        with open(DataFolder + "/quat.npy", "rb") as f:
            quat = np.load(f)

        with open(DataFolder + "/vel.npy", "rb") as f:
            vel = np.load(f)

        with open(DataFolder + "/acc.npy", "rb") as f:
            acc = np.load(f)

        with open(LabelFolder + "/traj.npy", "rb") as f:
            trajlabel = np.load(f)

        with open(DataFolder + "/A.npy", "rb") as f:
            A = np.load(f)

        return self.data_processing(point_cloud, A, goal, quat, vel, acc, trajlabel)

    def compute_mean_std(self):
        point_cloud_list, goal_list, quat_list, vel_list, trajlabel_list = [], [], [], [], []

        self.mode = "train"
        for i in range(len(self)):
            _, goal, _, vel, _ = self.__getitem__(i)
            # point_cloud_list.append(point_cloud)
            goal_list.append(goal)
            # quat_list.append(quat)
            vel_list.append(vel)
            # trajlabel_list.append(trajlabel)
        self.mode = "valid"
        for i in range(len(self)):
            _, goal, _, vel, _ = self.__getitem__(i)
            # point_cloud_list.append(point_cloud)
            goal_list.append(goal)
            # quat_list.append(quat)
            vel_list.append(vel)
            # trajlabel_list.append(trajlabel)
        self.mode = "test"
        for i in range(len(self)):
            _, goal, _, vel, _ = self.__getitem__(i)
            # point_cloud_list.append(point_cloud)
            goal_list.append(goal)
            # quat_list.append(quat)
            vel_list.append(vel)
            # trajlabel_list.append(trajlabel)

        # point_cloud = np.concatenate(point_cloud_list, axis=0)
        # self.point_cloud_mean, self.point_cloud_std = np.mean(point_cloud, axis=0), np.std(point_cloud, axis=0)
        goal = np.concatenate(goal_list)
        self.goal_mean, self.goal_std = np.mean(goal, axis=0), np.std(goal, axis=0)
        # quat = np.concatenate(quat_list)
        # self.quat_mean, point_cloud_std = np.mean(point_cloud, axis=0), np.std(point_cloud, axis=0)
        vel = np.concatenate(vel_list)
        self.vel_mean, self.vel_std = np.mean(vel, axis=0), np.std(vel, axis=0)

    def normalize(self, goal, vel, trajlabel):
        # goal = (goal - self.goal_mean) / self.goal_std
        # vel = (vel - self.vel_mean) / self.vel_std

        # trajlabel[:7] = (trajlabel[:7] - self.trajlabel_x_mean) / self.trajlabel_x_std
        # trajlabel[7:14] = (trajlabel[7:14] - self.trajlabel_y_mean) / self.trajlabel_y_std
        # trajlabel[14:21] = (trajlabel[14:21] - self.trajlabel_z_mean) / self.trajlabel_z_std
        trajlabel = trajlabel / self.map_lim
        return goal, vel, trajlabel

    def de_nromalize(self, trajlabel):
        # trajlabel[:7] = (trajlabel[:7] * self.trajlabel_x_std) + self.trajlabel_x_mean
        # trajlabel[7:14] = trajlabel[7:14] * self.trajlabel_y_std + self.trajlabel_y_mean
        # trajlabel[14:21] = trajlabel[14:21] * self.trajlabel_z_std + self.trajlabel_z_mean
        trajlabel = trajlabel * self.map_lim
        return trajlabel

    def loading(self, batch):
        num_data = [item[0].shape[0] for item in batch]
        max_num = max(num_data)
        point_cloud = []
        A = []
        goal = []
        quat = []
        vel = []
        acc = []
        trajlabel = []
        for item in batch:
            goal_np, vel_np, trajlabel_np = self.normalize(item[2], item[4], item[6])
            point_cloud.append(
                torch.cat(
                    (torch.Tensor(item[0]), torch.ones((max_num - item[0].shape[0], 3))), dim=0
                )
            )
            Ajx = np.zeros((max_num, max_num))
            Ajx[: item[1].shape[0], : item[1].shape[1]] = item[1]
            A.append(torch.Tensor(Ajx))
            goal.append(torch.Tensor(goal_np))
            quat.append(torch.Tensor(item[3]))
            vel.append(torch.Tensor(vel_np))
            acc.append(torch.Tensor(item[5]))
            trajlabel.append(torch.Tensor(trajlabel_np))

        return (
            torch.stack(point_cloud, dim=0),
            torch.stack(A, dim=0),
            torch.stack(goal, dim=0),
            torch.stack(quat, dim=0),
            torch.stack(vel, dim=0),
            torch.stack(acc, dim=0),
            torch.stack(trajlabel, dim=0),
        )


class MultiTrajDataset(TrajDataset):
    def __init__(self, src_folder, train_num=0.8, valid_num=0.1, seed=None):
        self.map_lim = 4.5
        super().__init__(src_folder, train_num, valid_num, seed)

        self.goal_mean, self.goal_std = (1.0, 0.0)
        self.vel_mean, self.vel_std = (1.0, 0.0)

    def data_processing(
        self, point_cloud, point_cloud_drone, Apt, A, goal, quat, vel, acc, state, trajlabel
    ):
        point_cloud = point_cloud.astype("float32")
        point_cloud_drone = point_cloud_drone.astype("float32")
        goal = goal.astype("float32")
        quat = quat.astype("float32")
        vel = vel.astype("float32")
        acc = acc.astype("float32")
        trajlabel = trajlabel.astype("float32")
        state = state.astype("float32")
        A = A.astype("float32")
        Apt = Apt.todense().astype("float32")

        point_cloud = np.concatenate(
            (
                point_cloud,
                point_cloud_drone,
            ),
            axis=1,
        )
        """
        Apt = []

        for i in range(point_cloud.shape[0]):
            Apti = radius_neighbors_graph(point_cloud[i, :, :], 0.05, mode="connectivity", include_self=True).toarray()
            D = np.diag(np.sum(Apti, axis=1) ** (-1 / 2))
            Apt.append(D@Apti@D)

        Apt = np.stack(Apt, axis=0)
        A = radius_neighbors_graph(state, 4, mode="connectivity", include_self=True).toarray()
        D = np.diag(np.sum(A, axis=1) ** (-1))
        A = D @ A
        """
        return point_cloud, Apt, A, goal, quat, vel, acc, trajlabel, state

        #return point_cloud, point_cloud_drone, Apt, A, goal, quat, vel, acc, trajlabel, state

    def __getitem__(self, idx):
        assert idx <= self.num_data

        if self.mode == "train":
            files = self.train_files[idx]
        elif self.mode == "test":
            files = self.test_files[idx]
        elif self.mode == "valid":
            files = self.valid_files[idx]
        else:
            raise RuntimeError("Wrong mode!!! value accepted are 'train', 'test', 'valid'")

        DataFolder = self.src_folder + files + "/Input"
        LabelFolder = self.src_folder + files + "/Label"

        with open(DataFolder + "/point_cloud.npy", "rb") as f:
            point_cloud = np.load(f)

        with open(DataFolder + "/point_cloud_drone.npy", "rb") as f:
            point_cloud_drone = np.load(f)

        with open(DataFolder + "/goal.npy", "rb") as f:
            goal = np.load(f)

        with open(DataFolder + "/quat.npy", "rb") as f:
            quat = np.load(f)

        with open(DataFolder + "/vel.npy", "rb") as f:
            vel = np.load(f)

        with open(DataFolder + "/acc.npy", "rb") as f:
            acc = np.load(f)

        with open(DataFolder + "/state.npy", "rb") as f:
            state = np.load(f)

        try:
            with open(DataFolder + "/A.npy", "rb") as f:
                A = np.load(f)

            with open(DataFolder + "/Apc.npy", "rb") as f:
                Apc = sparse.load_npz(f)
        except:
            A = np.array([])
            Apc = sparse.COO(np.array([]))

        with open(LabelFolder + "/traj.npy", "rb") as f:
            trajlabel = np.load(f)

        return self.data_processing(
            point_cloud, point_cloud_drone, Apc, A, goal, quat, vel, acc, state, trajlabel
        )

    def compute_mean_std(self):
        point_cloud_list, goal_list, quat_list, vel_list, trajlabel_list = [], [], [], [], []

        self.mode = "train"
        for i in range(len(self)):
            _, goal, _, vel, _ = self.__getitem__(i)
            # point_cloud_list.append(point_cloud)
            goal_list.append(goal)
            # quat_list.append(quat)
            vel_list.append(vel)
            # trajlabel_list.append(trajlabel)
        self.mode = "valid"
        for i in range(len(self)):
            _, goal, _, vel, _ = self.__getitem__(i)
            # point_cloud_list.append(point_cloud)
            goal_list.append(goal)
            # quat_list.append(quat)
            vel_list.append(vel)
            # trajlabel_list.append(trajlabel)
        self.mode = "test"
        for i in range(len(self)):
            _, goal, _, vel, _ = self.__getitem__(i)
            # point_cloud_list.append(point_cloud)
            goal_list.append(goal)
            # quat_list.append(quat)
            vel_list.append(vel)
            # trajlabel_list.append(trajlabel)

        # point_cloud = np.concatenate(point_cloud_list, axis=0)
        # self.point_cloud_mean, self.point_cloud_std = np.mean(point_cloud, axis=0), np.std(point_cloud, axis=0)
        goal = np.concatenate(goal_list)
        self.goal_mean, self.goal_std = np.mean(goal, axis=0), np.std(goal, axis=0)
        # quat = np.concatenate(quat_list)
        # self.quat_mean, point_cloud_std = np.mean(point_cloud, axis=0), np.std(point_cloud, axis=0)
        vel = np.concatenate(vel_list)
        self.vel_mean, self.vel_std = np.mean(vel, axis=0), np.std(vel, axis=0)

    def normalize(self, goal, vel, acc, trajlabel):
        # goal = (goal - self.goal_mean) / self.goal_std
        # vel = (vel - self.vel_mean) / self.vel_std

        # trajlabel[:7] = (trajlabel[:7] - self.trajlabel_x_mean) / self.trajlabel_x_std
        # trajlabel[7:14] = (trajlabel[7:14] - self.trajlabel_y_mean) / self.trajlabel_y_std
        # trajlabel[14:21] = (trajlabel[14:21] - self.trajlabel_z_mean) / self.trajlabel_z_std
        trajlabel = trajlabel / self.map_lim
        return goal, vel, acc, trajlabel

    def de_nromalize(self, trajlabel):
        # trajlabel[:7] = (trajlabel[:7] * self.trajlabel_x_std) + self.trajlabel_x_mean
        # trajlabel[7:14] = trajlabel[7:14] * self.trajlabel_y_std + self.trajlabel_y_mean
        # trajlabel[14:21] = trajlabel[14:21] * self.trajlabel_z_std + self.trajlabel_z_mean
        trajlabel = trajlabel * self.map_lim
        return trajlabel

    def loading(self, batch):
        max_num = max([item[0].shape[1] for item in batch])
        point_cloud = []
        Apc = []
        A = []
        goal = []
        quat = []
        vel = []
        acc = []
        trajlabel = []
        state = []
        for item in batch:
            goal_np, vel_np, acc_np, trajlabel_np = self.normalize(
                item[3], item[5], item[6], item[7]
            )
            point_cloud.append(
                torch.cat(
                    (
                        torch.Tensor(item[0]),
                        Tensor(item[0][:, -1:, :]).repeat(1, (max_num - item[0].shape[1]), 1),
                    ),
                    dim=1,
                )
            )
            Ajx = np.zeros((item[1].shape[0], max_num, max_num))
            Ajx[:, : item[1].shape[1], : item[1].shape[2]] = item[1]
            Apc.append(torch.Tensor(Ajx))
            A.append(torch.Tensor(item[2]))
            goal.append(torch.Tensor(goal_np))
            quat.append(torch.Tensor(item[4]))
            vel.append(torch.Tensor(vel_np))
            acc.append(torch.Tensor(acc_np))
            trajlabel.append(torch.Tensor(trajlabel_np))
            state.append(Tensor(item[-1]))

        return (
            torch.stack(point_cloud, dim=0),
            torch.stack(Apc, dim=0),
            torch.stack(A, dim=0),
            torch.stack(goal, dim=0),
            torch.stack(quat, dim=0),
            torch.stack(vel, dim=0),
            torch.stack(acc, dim=0),
            torch.stack(trajlabel, dim=0),
            torch.stack(state, dim=0),
        )


class CollisionDataset(TrajDataset):
    def __init__(self, src_folder, train_num=0.8, valid_num=0.1, seed=None):
        super().__init__(src_folder, train_num, valid_num, seed)

    def data_processing(self, point_cloud, success, trajlabel):
        point_cloud = point_cloud.astype("float32")
        success = success.astype("bool")
        trajlabel = trajlabel.astype("float32")

        return point_cloud, success, trajlabel

    def __getitem__(self, idx):
        assert idx < self.num_data

        if self.mode == "train":
            files = self.train_files[idx]
        elif self.mode == "test":
            files = self.test_files[idx]
        elif self.mode == "valid":
            files = self.valid_files[idx]
        else:
            raise RuntimeError("Wrong mode!!! value accepted are 'train', 'test', 'valid'")

        DataFolder = self.src_folder + files + "/Input"
        LabelFolder = self.src_folder + files + "/Label"

        with open(DataFolder + "/point_cloud.npy", "rb") as f:
            point_cloud = np.load(f)

        with open(LabelFolder + "/success.npy", "rb") as f:
            success = np.load(f)

        with open(LabelFolder + "/traj.npy", "rb") as f:
            trajlabel = np.load(f)

        return self.data_processing(point_cloud, success, trajlabel)

    def loading(self, batch):
        num_data = [item[0].shape[0] for item in batch]
        max_num = max(num_data)
        point_cloud = []
        success = []
        trajlabel = []
        for item in batch:
            point_cloud.append(
                torch.cat(
                    (torch.Tensor(item[0]), torch.ones((max_num - item[0].shape[0], 3))), dim=0
                )
            )
            success.append(Tensor(item[1]))
            trajlabel.append(Tensor(item[2]))

        point_cloud = torch.stack(point_cloud)
        return (
            point_cloud,
            torch.stack(success, dim=0),
            torch.stack(trajlabel, dim=0),
        )


class MultiCollisionDataset(TrajDataset):
    def __init__(self, src_folder, train_num=0.8, valid_num=0.1, seed=None):
        super().__init__(src_folder, train_num, valid_num, seed)

    def data_processing(self, point_cloud, point_cloud_drone, Apt, A, success, state, trajlabel):
        point_cloud = point_cloud.astype("float32")
        point_cloud_drone = point_cloud_drone.astype("float32")
        success = success.astype("bool")
        trajlabel = trajlabel.astype("float32")
        state = state.astype("float32")
        A = A.astype("float32")
        Apt = Apt.todense().astype("float32")

        point_cloud = np.concatenate(
            (
                point_cloud,
                point_cloud_drone,
            ),
            axis=1,
        )
        """
        Apt = []

        for i in range(point_cloud.shape[0]):
            Apti = radius_neighbors_graph(
                point_cloud[i, :, :], 0.5, mode="connectivity", include_self=True
            ).toarray()
            D = np.diag(np.sum(Apti, axis=1) ** (-1 / 2))
            Apt.append(D @ Apti @ D)

        Apt = np.stack(Apt, axis=0)
        A = radius_neighbors_graph(state, 4, mode="connectivity", include_self=True).toarray()
        D = np.diag(np.sum(A, axis=1) ** (-1))
        A = D @ A
        """
        return point_cloud, Apt, A, success, trajlabel

    def __getitem__(self, idx):
        assert idx < self.num_data

        if self.mode == "train":
            files = self.train_files[idx]
        elif self.mode == "test":
            files = self.test_files[idx]
        elif self.mode == "valid":
            files = self.valid_files[idx]
        else:
            raise RuntimeError("Wrong mode!!! value accepted are 'train', 'test', 'valid'")

        DataFolder = self.src_folder + files + "/Input"
        LabelFolder = self.src_folder + files + "/Label"

        with open(DataFolder + "/point_cloud.npy", "rb") as f:
            point_cloud = np.load(f)

        with open(LabelFolder + "/success.npy", "rb") as f:
            success = np.load(f)

        with open(LabelFolder + "/traj.npy", "rb") as f:
            trajlabel = np.load(f)

        with open(DataFolder + "/state.npy", "rb") as f:
            state = np.load(f)

        with open(DataFolder + "/point_cloud_drone.npy", "rb") as f:
            point_cloud_drone = np.load(f)

        try:
            with open(DataFolder + "/A.npy", "rb") as f:
                A = np.load(f)
            with open(DataFolder + "/Apc.npy", "rb") as f:
                Apc = sparse.load_npz(f)
        except:
            A = np.array([])
            Apc = sparse.COO(np.array([]))

        return self.data_processing(
            point_cloud, point_cloud_drone, Apc, A, success, state, trajlabel
        )

    def loading(self, batch):
        max_num = max([item[0].shape[1] for item in batch])
        point_cloud = []
        point_cloud = []
        success = []
        trajlabel = []
        Apc = []
        A = []
        dim = []
        for item in batch:
            point_cloud.append(
                torch.cat(
                    (
                        torch.Tensor(item[0]),
                        Tensor(item[0][:, -1:, :]).repeat(1, (max_num - item[0].shape[1]), 1),
                    ),
                    dim=1,
                )
            )
            success.append(Tensor(item[3]))
            trajlabel.append(Tensor(item[4]))
            Ajx = np.zeros((item[1].shape[0], max_num, max_num))
            Ajx[:, : item[1].shape[1], : item[1].shape[2]] = item[1]
            Apc.append(torch.Tensor(Ajx))
            A.append(torch.Tensor(item[2]))
            dim.append(item[3].shape[0])

        point_cloud = torch.stack(point_cloud)
        return (
            point_cloud,
            torch.stack(Apc, dim=0),
            torch.stack(A, dim=0),
            torch.concatenate(success, dim=0),
            torch.concatenate(trajlabel, dim=0),
            torch.tensor(dim),
        )


class Sampler:
    def __init__(self, indices, batch_size, seed=None):
        self.seed = seed
        self.classes = [
            [str(idx) + "-" + str(j) for j in range(0, i)] for idx, i in enumerate(indices)
        ]
        self.batch_size = batch_size

    @staticmethod
    def divide_chunks(l, n):
        # looping till
        # length l
        for i in range(0, len(l), n):
            yield l[i : i + n]

    def __iter__(self):
        classes = copy.deepcopy(self.classes)

        res = []
        for i in range(len(classes)):
            for j in range(0, len(self.classes[i]), self.batch_size):
                res.append(tuple(self.classes[i][j : j + self.batch_size]))

        random.shuffle(res)
        return iter(res)


if __name__ == "__main__":
    folder = os.getcwd() + "/mader/dataset_multi_collision/2024-05-11_16-04-32/"
    fd = MultiCollisionDataset(src_folder=folder, train_num=1.0, valid_num=0.0, seed=1)

    fd.mode = "train"
    train_loader = DataLoader(copy.deepcopy(fd), batch_size=25, shuffle=True, collate_fn=fd.loading)
    max_vel = np.array([0.0, 0.0, 0.0])
    max_acc = np.array([0.0, 0.0, 0.0])
    for i, (point_cloud, Apc, A, success, trajlabel) in enumerate(tqdm(fd)):
        #max_vel = np.max(np.stack([np.max(np.abs(vel), axis=0), max_vel]), axis=0)
        #max_acc = np.max(np.stack([np.max(np.abs(acc), axis=0), max_acc]), axis=0)

        DataFolder = folder + fd.train_files[i] + "/Input"

        with open(DataFolder + "/Apc.npy", "wb") as f:
            Apc = sparse.COO(Apc)
            sparse.save_npz(f, Apc)

        with open(DataFolder + "/A.npy", "wb") as f:
            np.save(f, A)

    #print(max_acc)
    #print(max_vel)
