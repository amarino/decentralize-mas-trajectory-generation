import torch
from torch import Tensor
import torch.nn as nn
from GNNlib import PointNet, PointNetpp, GraphLayer
from qpth.qp import QPFunction
from torch.autograd import Variable
from torch.nn.parameter import Parameter

from torch_geometric.nn import DMoNPooling, DenseGCNConv
from torch_sparse import SparseTensor
from torch.nn.utils.parametrizations import spectral_norm

import sys, os


# Disable
def blockPrint():
    sys.stdout = open(os.devnull, "w")


# Restore
def enablePrint():
    sys.stdout = sys.__stdout__


class CollisionTrajModel(nn.Module):
    def __init__(self, device=torch.device("cpu")) -> None:
        super(CollisionTrajModel, self).__init__()
        self.point_net = None

        self.fc1 = nn.Linear(128, 512)
        self.fc2 = nn.Linear(512, 256)
        self.fc3 = nn.Linear(256, 128)
        self.fc4 = nn.Linear(128, 64)
        self.fc5 = nn.Linear(64, 30)
        
        self.dropout = nn.Dropout(p=0.3)

        self.initialize_parameters()

        self.leaky_relu = nn.LeakyReLU()
        self.device = device

    def initialize_parameters(self) -> None:
        """initialize_parameters

        Init layers weights and biases
        """

        for name, param in self.named_parameters():
            if param.requires_grad:
                if len(param.shape) != 1:
                    nn.init.orthogonal_(param, gain=1)
            if "bias" in name:
                param.data.fill_(0)

    def forward(self, point_cloud: Tensor):
        # out_pn, m3x3, m64x64 = self.point_net(point_cloud)
        # out_pn, _, _ = self.point_net(point_cloud, A)

        # m3x3 = torch.ones(out_pn.shape[0], 3, 3).to(self.device)
        # m64x64 = torch.ones(out_pn.shape[0], 64, 64).to(self.device)
        # _, p, _, _, _, _ = self.clustering(point_cloud, A)
        out_pn, _, _, out_pmax = self.point_net(point_cloud)

        out = self.leaky_relu(self.fc1(out_pn))
        out = self.leaky_relu(self.fc2(out))
        out = self.leaky_relu(self.fc3(out))
        out = self.leaky_relu(self.fc4(out))
        out = self.fc5(out)

        return out, out_pmax


class MultiCollisionTrajModel(nn.Module):
    def __init__(self, device=torch.device("cpu")) -> None:
        super(MultiCollisionTrajModel, self).__init__()
        self.point_net = None

        self.fc1 = GraphLayer(2, 128, 256, direct_input=True, attention=True, autoencoder=True)
        self.fc2 = GraphLayer(2, 256, 128, direct_input=True, attention=True, autoencoder=True)
        self.fc3 = nn.Linear(128, 64)
        self.fc4 = nn.Linear(64, 32)
        self.fc5 = nn.Linear(32, 30)

        self.bc1 = nn.LayerNorm(256)
        self.bc2 = nn.LayerNorm(128)
        self.bc3 = nn.LayerNorm(64)
        self.bc4 = nn.LayerNorm(32)
        self.dropout = nn.Dropout(p=0.3)

        self.initialize_parameters()

        self.leaky_relu = nn.LeakyReLU()
        self.device = device

    def initialize_parameters(self) -> None:
        """initialize_parameters

        Init layers weights and biases
        """

        for name, param in self.named_parameters():
            if param.requires_grad:
                if len(param.shape) != 1:
                    nn.init.orthogonal_(param, gain=1)
            if "bias" in name:
                param.data.fill_(0)

    def forward(self, point_cloud: Tensor, Apc: Tensor, A: Tensor):
        batch = point_cloud.shape[0]
        N = point_cloud.shape[1]
        point_cloud = torch.flatten(point_cloud, start_dim=0, end_dim=1)
        Apc = torch.flatten(Apc, start_dim=0, end_dim=1)

        out_pn, _, _, out_pmax, disp_out = self.point_net(point_cloud)

        p = torch.unflatten(out_pn, 0, (batch, N))

        
        out = self.leaky_relu(self.fc1(p, A))
        out = self.leaky_relu(self.fc2(out, A))
        out = self.leaky_relu(self.fc3(out))
        out = self.leaky_relu(self.fc4(out))
        out = self.fc5(out)

        return out, out_pmax, disp_out


class MASTrajModel(nn.Module):
    def __init__(self, v_max, a_max, knots, qp_enabled=False, device=torch.device("cpu")) -> None:
        super(MASTrajModel, self).__init__()
        self.map_lim = 4.5
        self.point_net = PointNet(device, dim_out=51)
        self.nCls = 10
        self.nineq = 2 * self.nCls - 2
        self.neq = 3

        self.qp_enabled = qp_enabled

        self.clustering = DenseGCNConv(1, 128)  # DMoNPooling(12,6)
        self.clustering_pool = nn.Sequential(nn.SELU(), nn.Softmax(dim=-1))
        self.clustering_dropout = nn.Dropout(p=0.2)
        self.cluster_linear1 = nn.Linear(128, 32)
        self.cluster_linear2 = nn.Linear(32, 1)

        self.cluster_linear3 = nn.Linear(128, 64)
        self.cluster_linear4 = nn.Linear(64, 55)

        self.fc1 = GraphLayer(2, 64, 512, direct_input=True, attention=True, autoencoder=True)
        self.fc2 = GraphLayer(2, 512, 256, direct_input=True, attention=True, autoencoder=True)
        self.fc3 = GraphLayer(
            2, 256, 3 * self.nCls, direct_input=True, attention=True, autoencoder=True
        )

        # self.fc1 = nn.Linear(64, 512)
        # self.fc2 = nn.Linear(512, 256)
        # self.fc3 = nn.Linear(256, 3 * self.nCls)

        self.goal_mean = 0.0
        self.goal_std = 1.0

        self.vel_mean = 0.0
        self.vel_std = 1.0

        self.knots = torch.Tensor(knots)

        idx = torch.linspace(0, self.nCls, self.nCls, dtype=torch.int32)
        G1 = -torch.eye(self.nCls + 2, self.nCls + 2)
        G1 = 3 * torch.diagonal_scatter(G1, torch.ones(self.nCls + 1), 1)
        G1[: self.nCls, :] = (
            G1[: self.nCls, :]
            / (
                torch.index_select(self.knots, 0, idx + 3 + 1)
                - torch.index_select(self.knots, 0, idx + 1)
            )[:, None]
        )
        G1[-3:, :] = 0.0
        G2 = -torch.eye(self.nCls + 2, self.nCls + 2)
        G2 = torch.diagonal_scatter(G2, torch.ones(self.nCls + 1), 1)
        G2 = torch.matmul(G2, G1)
        G2[: self.nCls - 1, :] = (
            2
            * G2[: self.nCls - 1, :]
            / (
                torch.index_select(self.knots, 0, idx[: self.nCls - 1] + 3 + 1)
                - torch.index_select(self.knots, 0, idx[: self.nCls - 1] + 2)
            )[:, None]
        )
        G_x = torch.vstack([G1[: self.nCls - 1, : self.nCls], G2[: self.nCls - 1, : self.nCls]])
        G_x = G_x.repeat(2, 1)
        G_x[self.nineq :, :] = -G_x[self.nineq :, :]
        G = torch.zeros(self.nineq * 6, 3 * self.nCls)
        G[: 2 * self.nineq, : self.nCls] = G_x
        G[2 * self.nineq : 4 * self.nineq, self.nCls : 2 * self.nCls] = G_x
        G[4 * self.nineq :, 2 * self.nCls :] = G_x

        # eq constraints
        A = torch.zeros(3 * self.neq, 3 * self.nCls)
        A[0, 0] = 1.0
        A[1, : self.nCls] = G_x[0, :]
        A[2, : self.nCls] = G2[0, : self.nCls]
        A[self.neq : 2 * self.neq, self.nCls : 2 * self.nCls] = A[: self.neq, : self.nCls]
        A[2 * self.neq :, 2 * self.nCls :] = A[: self.neq, : self.nCls]

        self.Q = Variable(1e3 * (torch.eye(3 * self.nCls, 3 * self.nCls)).to(device))
        self.G = Variable(G.to(device))
        self.A = Variable(A.to(device))
        self.bv = Variable(torch.Tensor([1]).to(device))
        self.hv = Variable(torch.ones(self.nCls - 1).to(device))
        self.ha = Variable(torch.ones(self.nCls - 1).to(device))

        self.v_max = [v_max[0] / self.map_lim, v_max[1] / self.map_lim, v_max[2] / self.map_lim]
        self.a_max = [a_max[0] / self.map_lim, a_max[1] / self.map_lim, a_max[2] / self.map_lim]
        self.bn1 = nn.BatchNorm1d(42)

        self.initialize_parameters()

        self.leaky_relu = nn.ReLU()

        self.v_max_t = [v_max[0] / self.map_lim, v_max[1] / self.map_lim, v_max[2] / self.map_lim]
        self.a_max_t = [a_max[0] / self.map_lim, a_max[1] / self.map_lim, a_max[2] / self.map_lim]
        self.knots = self.knots.to(device)
        self.v_max_t = Tensor(self.v_max_t).to(device)
        self.a_max_t = Tensor(self.a_max_t).to(device)

        # collision constraints parameter
        self.compute_cc = False
        self.collision_constraint = False
        self.use_cc = False
        self.alpha = 0.01

        self.device = device

    def initialize_parameters(self) -> None:
        """initialize_parameters

        Init layers weights and biases
        """

        for name, param in self.named_parameters():
            if param.requires_grad:
                if len(param.shape) != 1:
                    nn.init.orthogonal_(param, gain=1)
            if "bias" in name:
                param.data.fill_(0)

    def normalize(self, goal, vel):
        goal = (goal - self.goal_mean) / self.goal_std
        vel = (vel - self.vel_mean) / self.vel_std
        return goal, vel

    def qp_traj(self, out, vel, acc, cc):
        batch = out.shape[0]

        pz = -1e3 * out
        hv = self.hv.reshape(1, self.nCls - 1).repeat(batch, 1)
        ha = self.ha.reshape(1, self.nCls - 1).repeat(batch, 1)

        vel = vel * self.v_max_t
        acc = acc * self.a_max_t
        Q = self.Q.reshape(1, 3 * self.nCls, 3 * self.nCls).repeat(batch, 1, 1)
        G = self.G.reshape(1, self.G.shape[0], self.G.shape[1]).repeat(batch, 1, 1)
        A = self.A.reshape(1, self.A.shape[0], self.A.shape[1]).repeat(batch, 1, 1)

        h = torch.hstack(
            [
                hv * self.v_max[0],
                ha * self.a_max[0],
                hv * self.v_max[0],
                ha * self.a_max[0],
                hv * self.v_max[1],
                ha * self.a_max[1],
                hv * self.v_max[1],
                ha * self.a_max[1],
                hv * self.v_max[2],
                ha * self.a_max[2],
                hv * self.v_max[2],
                ha * self.a_max[2],
            ]
        )

        b = self.bv * torch.hstack(
            [
                self.knots[:1].repeat(batch, 1),
                vel[:, :1],
                acc[:, :1],
                self.knots[:1].repeat(batch, 1),
                vel[:, 1:2],
                acc[:, 1:2],
                self.knots[:1].repeat(batch, 1),
                vel[:, 2:],
                acc[:, 2:],
            ]
        )

        if self.use_cc:
            G = torch.concatenate([G, cc.reshape(batch, 1, 3 * self.nCls)], axis=1)
            h = torch.concatenate([h, -ha[:, :1] * self.alpha], axis=1)

        blockPrint()

        out = QPFunction(verbose=False, notImprovedLim=10, maxIter=20)(Q, pz, G, h, A, b)

        enablePrint()

        return out

    def forward(
        self,
        point_cloud: Tensor,
        goal: Tensor,
        quat: Tensor,
        vel: Tensor,
        acc: Tensor,
        Apc: Tensor,
        A: Tensor,
    ):
        batch = point_cloud.shape[0]
        N = point_cloud.shape[1]

        if self.compute_cc:
            cc, out_pmax, disp_out = self.collision_constraint(point_cloud, Apc, A)
            m3x3 = None
            m64x64 = None
        else:
            point_cloud = torch.flatten(point_cloud, start_dim=0, end_dim=1)
            _, m3x3, m64x64, out_pmax, disp_out = self.point_net(point_cloud)
            cc = []

        Apc = torch.flatten(Apc, start_dim=0, end_dim=1)
        out_pmax = self.leaky_relu(self.cluster_linear1(out_pmax))
        out_pmax = self.leaky_relu(self.cluster_linear2(out_pmax))

        p = self.clustering(out_pmax, Apc)
        p = self.clustering_pool(p)
        p = torch.matmul(p.transpose(1, 2), out_pmax)
        p = torch.flatten(p, start_dim=1)

        p = self.leaky_relu(self.cluster_linear3(p))
        p = torch.tanh(self.cluster_linear4(p))

        p = torch.unflatten(p, 0, (batch, N))

        out = torch.cat([p, goal, vel, acc], dim=2)
        out = self.leaky_relu(self.fc1(out, A))
        out = self.leaky_relu(self.fc2(out, A))
        out = self.fc3(out, A)

        if self.qp_enabled:
            out = torch.flatten(out, start_dim=0, end_dim=1)
            vel = torch.flatten(vel, start_dim=0, end_dim=1)
            acc = torch.flatten(acc, start_dim=0, end_dim=1)
            out = self.qp_traj(out, vel, acc, cc)
            out = torch.unflatten(out, 0, (batch, N))

        return out, m3x3, m64x64, disp_out


class SASTrajModel(nn.Module):
    def __init__(self, qp_enabled=False, device=torch.device("cpu")) -> None:
        super(SASTrajModel, self).__init__()
        self.point_net = PointNet(device, dim_out=51)
        self.nCls = 10
        self.nineq = 2 * self.nCls - 2
        self.neq = 3

        self.qp_enabled = qp_enabled

        self.clustering = DenseGCNConv(1, 128)  # DMoNPooling(12,6)
        self.clustering_pool = nn.Sequential(nn.SELU(), nn.Softmax(dim=-1))
        self.clustering_dropout = nn.Dropout(p=0.2)
        self.cluster_linear1 = nn.Linear(128, 32)
        self.cluster_linear2 = nn.Linear(32, 1)

        self.cluster_linear3 = nn.Linear(128, 64)
        self.cluster_linear4 = nn.Linear(64, 51)

        self.fc1 = nn.Linear(64, 512)
        self.fc2 = nn.Linear(512, 256)
        self.fc3 = nn.Linear(256, 3 * self.nCls)

        self.goal_mean = 0.0
        self.goal_std = 1.0

        self.vel_mean = 0.0
        self.vel_std = 1.0

        self.knots = torch.Tensor(
            [
                0.0,
                0.0,
                0.0,
                0.0,
                0.29171598,
                0.45266272,
                0.6035503,
                0.76449704,
                0.91538462,
                1.07633136,
                1.22721893,
                1.38816568,
                1.7,
                1.7,
                1.7,
                1.7,
            ]
        )
        # v = 3 * (x[i + 1] - x[i]) / (knots[3 + i + 1] - knots[i + 1])
        # v1 = 3 * (x[i + 2] - x[i + 1]) / (knots[3 + i + 2] - knots[i + 2])
        # a = 2 * (v1 - v) / (knots[3 + i + 1] - knots[i + 2])

        # ineq constrains

        idx = torch.linspace(0, self.nCls, self.nCls, dtype=torch.int32)
        G1 = -torch.eye(self.nCls + 2, self.nCls + 2)
        G1 = 3 * torch.diagonal_scatter(G1, torch.ones(self.nCls + 1), 1)
        G1[: self.nCls, :] = (
            G1[: self.nCls, :]
            / (
                torch.index_select(self.knots, 0, idx + 3 + 1)
                - torch.index_select(self.knots, 0, idx + 1)
            )[:, None]
        )
        G1[-3:, :] = 0.0
        G2 = -torch.eye(self.nCls + 2, self.nCls + 2)
        G2 = torch.diagonal_scatter(G2, torch.ones(self.nCls + 1), 1)
        G2 = torch.matmul(G2, G1)
        G2[: self.nCls - 1, :] = (
            2
            * G2[: self.nCls - 1, :]
            / (
                torch.index_select(self.knots, 0, idx[: self.nCls - 1] + 3 + 1)
                - torch.index_select(self.knots, 0, idx[: self.nCls - 1] + 2)
            )[:, None]
        )
        G_x = torch.vstack([G1[: self.nCls - 1, : self.nCls], G2[: self.nCls - 1, : self.nCls]])
        G_x = G_x.repeat(2, 1)
        G_x[self.nineq :, :] = -G_x[self.nineq :, :]
        G = torch.zeros(self.nineq * 6, 3 * self.nCls)
        G[: 2 * self.nineq, : self.nCls] = G_x
        G[2 * self.nineq : 4 * self.nineq, self.nCls : 2 * self.nCls] = G_x
        G[4 * self.nineq :, 2 * self.nCls :] = G_x

        # eq constraints
        A = torch.zeros(3 * self.neq, 3 * self.nCls)
        A[0, 0] = 1.0
        A[1, : self.nCls] = G_x[0, :]
        A[2, : self.nCls] = G2[0, : self.nCls]
        A[self.neq : 2 * self.neq, self.nCls : 2 * self.nCls] = A[: self.neq, : self.nCls]
        A[2 * self.neq :, 2 * self.nCls :] = A[: self.neq, : self.nCls]

        self.Q = Variable(1e3 * (torch.eye(3 * self.nCls, 3 * self.nCls)).to(device))
        self.G = Variable(G.to(device))
        self.A = Variable(A.to(device))
        self.bv = Variable(torch.Tensor([1]).to(device))
        self.hv = Variable(torch.ones(self.nCls - 1).to(device))
        self.ha = Variable(torch.ones(self.nCls - 1).to(device))

        self.v_max = [3.5 / 5, 3.5 / 5, 3.5 / 5]
        self.a_max = [20 / 5, 20 / 5, 9.6 / 5]
        self.bn1 = nn.BatchNorm1d(42)

        self.initialize_parameters()

        self.leaky_relu = nn.LeakyReLU()

        self.v_max_t = [3.5 / 5, 3.5 / 5, 3.5 / 5]
        self.a_max_t = [20 / 5, 20 / 5, 9.6 / 5]
        self.knots = self.knots.to(device)
        self.v_max_t = Tensor(self.v_max_t).to(device)
        self.a_max_t = Tensor(self.a_max_t).to(device)

        # collision constraints parameter
        self.collision_constraint = False
        self.use_cc = False
        self.alpha = 0.01

        self.device = device

    def initialize_parameters(self) -> None:
        """initialize_parameters

        Init layers weights and biases
        """

        for name, param in self.named_parameters():
            if param.requires_grad:
                if len(param.shape) != 1:
                    nn.init.orthogonal_(param, gain=1)
            if "bias" in name:
                param.data.fill_(0)

    def normalize(self, goal, vel):
        goal = (goal - self.goal_mean) / self.goal_std
        vel = (vel - self.vel_mean) / self.vel_std
        return goal, vel

    def qp_traj(self, out, vel, acc, cc):
        batch = out.shape[0]

        pz = -1e3 * out
        hv = self.hv.reshape(1, self.nCls - 1).repeat(batch, 1)
        ha = self.ha.reshape(1, self.nCls - 1).repeat(batch, 1)

        vel = vel * self.v_max_t
        # vel[:, 0] = torch.clamp(vel[:, 0], min=-self.v_max[0] + 0.01, max=self.v_max[0] - 0.01)
        # vel[:, 1] = torch.clamp(vel[:, 1], -self.v_max[1] + 0.01, self.v_max[1] - 0.01)
        # vel[:, 2] = torch.clamp(vel[:, 2], -self.v_max[2] + 0.01, self.v_max[2] - 0.01)
        acc = acc * self.a_max_t
        # acc[:, 0] = torch.clamp(acc[:, 0], -self.a_max[0] + 0.01, self.a_max[0] - 0.01)
        # acc[:, 1] = torch.clamp(acc[:, 1], -self.a_max[1] + 0.01, self.a_max[1] - 0.01)
        # acc[:, 2] = torch.clamp(acc[:, 2], -self.a_max[2] + 0.01, self.a_max[2] - 0.01)

        Q = self.Q.reshape(1, 3 * self.nCls, 3 * self.nCls).repeat(batch, 1, 1)
        G = self.G.reshape(1, self.G.shape[0], self.G.shape[1]).repeat(batch, 1, 1)
        A = self.A.reshape(1, self.A.shape[0], self.A.shape[1]).repeat(batch, 1, 1)

        h = torch.hstack(
            [
                hv * self.v_max[0],
                ha * self.a_max[0],
                hv * self.v_max[0],
                ha * self.a_max[0],
                hv * self.v_max[1],
                ha * self.a_max[1],
                hv * self.v_max[1],
                ha * self.a_max[1],
                hv * self.v_max[2],
                ha * self.a_max[2],
                hv * self.v_max[2],
                ha * self.a_max[2],
            ]
        )

        b = self.bv * torch.hstack(
            [
                self.knots[:1].repeat(batch, 1),
                vel[:, :1],
                acc[:, :1],
                self.knots[:1].repeat(batch, 1),
                vel[:, 1:2],
                acc[:, 1:2],
                self.knots[:1].repeat(batch, 1),
                vel[:, 2:],
                acc[:, 2:],
            ]
        )

        # A = Variable(torch.Tensor())
        # b= Variable(torch.Tensor())

        if self.use_cc:
            G = torch.concatenate([G, cc.reshape(batch, 1, 3 * self.nCls)], axis=1)
            h = torch.concatenate([h, -ha[:, :1] * self.alpha], axis=1)

        blockPrint()

        out = QPFunction(verbose=False, maxIter=20)(Q, pz, G, h, A, b)

        enablePrint()

        return out

    def forward(
        self,
        point_cloud: Tensor,
        goal: Tensor,
        quat: Tensor,
        vel: Tensor,
        acc: Tensor,
        A: Tensor,
        compute_cc: bool = False,
    ):
        if compute_cc:
            cc, out_pmax = self.collision_constraint(point_cloud)
            m3x3 = None
            m64x64 = None
        else:
            _, m3x3, m64x64, out_pmax = self.point_net(point_cloud)
            cc = []

        out_pmax = self.leaky_relu(self.cluster_linear1(out_pmax))
        out_pmax = self.leaky_relu(self.cluster_linear2(out_pmax))

        p = self.clustering(out_pmax, A)
        p = self.clustering_pool(p)
        p = torch.matmul(p.transpose(1, 2), out_pmax)
        p = torch.flatten(p, start_dim=1)

        p = self.leaky_relu(self.cluster_linear3(p))
        p = torch.tanh(self.cluster_linear4(p))

        out = torch.cat([p, quat, goal, vel, acc], dim=1)
        out = self.leaky_relu(self.fc1(out))
        out = self.leaky_relu(self.fc2(out))
        out = self.fc3(out)

        if self.qp_enabled:
            out = self.qp_traj(out, vel, acc, cc)

        return out, m3x3, m64x64


def saliency_map(disp_out):
    for k, d in enumerate(disp_out):
        disp_out[k] = torch.mean(torch.abs(d), dim=2)

    pt_disp = disp_out[-1]
    for d in reversed(range(1, len(disp_out))):
        pt_disp = disp_out[d - 1] * pt_disp

    disp_min = torch.min(pt_disp, keepdim=True, dim=1)[0]
    disp_max = torch.max(pt_disp, keepdim=True, dim=1)[0]
    pt_disp = (pt_disp - disp_min) / (disp_max)

    return pt_disp


class LossFunction(object):
    def __init__(self, label=None) -> None:
        self.update_label(label)
        self.criterion = nn.SmoothL1Loss(beta=0.1)
        self.softplus = nn.Softplus(beta=100)

    def update_label(self, label=None) -> None:
        self.label = label
        if label is not None:
            self.projector = torch.matmul(label[:, :, :1], label[:, :, :1].mT)

    def update_L(self, L: torch.Tensor) -> None:
        self.L = L

    def compute_loss(self, prediction, trajlabel):
        loss1 = self.criterion(prediction[..., :10], trajlabel[..., :10])
        loss2 = self.criterion(prediction[..., 10:20], trajlabel[..., 10:20])
        loss3 = self.criterion(prediction[..., 20:], trajlabel[..., 20:])
        return loss1 + loss2 + loss3

    def compute_collision_loss(
        self, prediction: Tensor, trajlabel: Tensor, success: Tensor, dim: Tensor
    ):
        loss = 0.0

        N = prediction.shape[1]
        dat_size = prediction.shape[2]

        prediction = prediction.repeat_interleave(dim, dim=0)

        # prediction = torch.flatten(prediction, start_dim=0, end_dim=1)
        # trajlabel = torch.flatten(trajlabel, start_dim=0, end_dim=1)
        # success = torch.flatten(success, start_dim=0, end_dim=1)

        batch = trajlabel.shape[0]

        success = success.reshape(batch, N, 1, 1)
        success_true = success.int()
        success_false = torch.abs(success_true - 1)

        prediction = prediction.reshape(batch, N, 1, dat_size)
        trajlabel = trajlabel.reshape(batch, N, dat_size, 1)

        loss = torch.mean(
            torch.sum(torch.relu(torch.matmul(prediction, trajlabel)+0.01) * success_true, dim=1)
        ) + torch.mean(
            torch.sum(torch.relu(-torch.matmul(prediction, trajlabel)-0.01) * success_false, dim=1)
        )

        return loss
