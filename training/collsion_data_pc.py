import numpy as np
import os
import matplotlib.pyplot as plt
from LoadingDataset import SingleTrajDataset
from tqdm import tqdm
import mayavi
from mayavi import mlab
from mayavi.modules.grid_plane import GridPlane
from scipy.interpolate import BSpline
import scipy.interpolate as interpolate
from scipy.spatial.transform import Rotation
import os.path
from os import makedirs
import time

import torch
from qpth.qp import QPFunction, QPSolvers
from torch.autograd import Variable

import sys, os


# Disable
def blockPrint():
    sys.stdout = open(os.devnull, "w")


# Restore
def enablePrint():
    sys.stdout = sys.__stdout__


knots = np.array(
    [
        0.0,
        0.0,
        0.0,
        0.0,
        0.29171598,
        0.45266272,
        0.6035503,
        0.76449704,
        0.91538462,
        1.07633136,
        1.22721893,
        1.38816568,
        1.7,
        1.7,
        1.7,
        1.7,
    ]
)
knots = torch.Tensor(knots)

nCls = 10
nineq = 2 * nCls - 2
neq = 3

idx = torch.linspace(0, nCls, nCls, dtype=torch.int32)
G1 = -torch.eye(nCls + 2, nCls + 2)
G1 = 3 * torch.diagonal_scatter(G1, torch.ones(nCls + 1), 1)
G1[:nCls, :] = (
    G1[:nCls, :] / (torch.index_select(knots, 0, idx + 3 + 1) - torch.index_select(knots, 0, idx + 1))[:, None]
)
G1[-3:, :] = 0.0
G2 = -torch.eye(nCls + 2, nCls + 2)
G2 = torch.diagonal_scatter(G2, torch.ones(nCls + 1), 1)
G2 = torch.matmul(G2, G1)
G2[: nCls - 1, :] = (
    2
    * G2[: nCls - 1, :]
    / (torch.index_select(knots, 0, idx[: nCls - 1] + 3 + 1) - torch.index_select(knots, 0, idx[: nCls - 1] + 2))[
        :, None
    ]
)
G_x = torch.vstack([G1[: nCls - 1, :nCls], G2[: nCls - 1, :nCls]])
G_x = G_x.repeat(2, 1)
G_x[nineq:, :] = -G_x[nineq:, :]
G = torch.zeros(nineq * 6, 3 * nCls)
G[: 2 * nineq, :nCls] = G_x
G[2 * nineq : 4 * nineq, nCls : 2 * nCls] = G_x
G[4 * nineq :, 2 * nCls :] = G_x

# eq constraints
A = torch.zeros(3 * neq, 3 * nCls)
A[0, 0] = 1.0
A[1, :nCls] = G_x[0, :]
A[2, :nCls] = G2[0, :nCls]
A[neq : 2 * neq, nCls : 2 * nCls] = A[:neq, :nCls]
A[2 * neq :, 2 * nCls :] = A[:neq, :nCls]

Q = Variable(1e3 * torch.eye(3 * nCls, 3 * nCls))
G = Variable(G)
A = Variable(A)
bv = Variable(torch.Tensor([1]))
hv = Variable(torch.ones(nCls - 1))
ha = Variable(torch.ones(nCls - 1))

v_max = [3.5 / 5, 3.5 / 5, 3.5 / 5]
a_max = [20 / 5, 20 / 5, 9.6 / 5]


def qp_traj(out, vel, acc, cc):
    pz = -1e3 * out
    hx = torch.hstack(
        [
            hv * v_max[0],
            ha * a_max[0],
            hv * v_max[0],
            ha * a_max[0],
            hv * v_max[1],
            ha * a_max[1],
            hv * v_max[1],
            ha * a_max[1],
            hv * v_max[2],
            ha * a_max[2],
            hv * v_max[2],
            ha * a_max[2],
        ]
    )
    # A = torch.Tensor()
    # A = self.A.reshape(1,2,8).repeat(batch,1,1)
    # bv = self.bv.reshape(1,1).repeat(batch,1)
    # ba = self.ba.reshape(1,1).repeat(batch,1)
    b = torch.hstack(
        [
            bv * 0.0,
            bv * vel[0],
            bv * acc[0],
            bv * 0.0,
            bv * vel[1],
            bv * acc[1],
            bv * 0.0,
            bv * vel[2],
            bv * acc[2],
        ]
    )
    # bx = torch.hstack([bv*0.0,bv*vel])
    # bx = torch.Tensor()

    G1 = torch.concatenate([G, cc], axis=0)
    h1 = torch.concatenate([hx, -ha[:1] * 0.0], axis=0)

    blockPrint()
    out_x = QPFunction(verbose=1, maxIter=50, notImprovedLim=10)(Q, pz, G1, h1, A, b)
    enablePrint()
    # by = torch.hstack([bv*vel[:,1:2],ba*acc[:,1:2]])
    return out_x


current_dir = os.getcwd()
data_path = current_dir + "/mader"
dataset = SingleTrajDataset(current_dir + "/mader/dataset/2023-11-19_15-12-10/", train_num=2.0, valid_num=0.0, seed=1)
data_path = data_path + "/dataset_collision/" + time.strftime("%Y-%m-%d_%H-%M-%S/")
num = 1
success_num = 0
failed_num = 0

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
train_cc_collision = "/trained_models/sas_model_cc.pt"
torch.set_grad_enabled(False)
model_cc = torch.load(current_dir + train_cc_collision)
model_cc.eval()
model_cc.to(device)

makedirs(data_path, exist_ok=True)

knots = np.array(
    [
        0.0,
        0.0,
        0.0,
        0.0,
        0.29171598,
        0.45266272,
        0.6035503,
        0.76449704,
        0.91538462,
        1.07633136,
        1.22721893,
        1.38816568,
        1.7,
        1.7,
        1.7,
        1.7,
    ]
)
tt = np.linspace(0, 1.7, 100)
tt_short = np.linspace(0.0, 0.8, 300)
plot = True
multi = False

max_num = 0
for pc, _, _, _, _, _, _ in tqdm(dataset):
    max_num = max(pc.shape[0], max_num)

for j, (pc, _, goal, quat, vel, acc, trajlabel) in enumerate(dataset):
    pc_const = []
    pc_const = torch.cat((torch.Tensor(pc), torch.ones((max_num - pc.shape[0], 3))), dim=0)
    pc_const = pc_const.unsqueeze(0)
    pc_const = pc_const.to(device)
    pc_const, _, _, _, _ = model_cc(pc_const)
    pc_const = pc_const.cpu()
    # pc, goal, quat, vel, acc, trajlabel = dataset[4]
    goal = goal * 5
    # R = Rotation.from_quat(quat)
    pc = pc * 5
    # pc = R.apply(pc)
    vel = vel * v_max
    acc = acc * a_max

    trajlabel_1 = torch.Tensor(trajlabel)
    trajlabel_1 = qp_traj(trajlabel_1 / 5, vel, acc, pc_const) * 5
    trajlabel_1 = trajlabel_1[0, :].numpy()
    x_label = trajlabel[:10]
    x_label = np.concatenate([x_label, np.array([x_label[-1]] * 3)])
    y_label = trajlabel[10:20]
    y_label = np.concatenate([y_label, np.array([y_label[-1]] * 3)])
    z_label = trajlabel[20:]
    z_label = np.concatenate([z_label, np.array([z_label[-1]] * 3)])

    x_label1 = trajlabel_1[:10]
    x_label1 = np.concatenate([x_label1, np.array([x_label1[-1]] * 3)])
    y_label1 = trajlabel_1[10:20]
    y_label1 = np.concatenate([y_label1, np.array([y_label1[-1]] * 3)])
    z_label1 = trajlabel_1[20:]
    z_label1 = np.concatenate([z_label1, np.array([z_label1[-1]] * 3)])

    splinex_l = interpolate.BSpline(knots, x_label, 3, extrapolate=False)
    spliney_l = interpolate.BSpline(knots, y_label, 3, extrapolate=False)
    splinez_l = interpolate.BSpline(knots, z_label, 3, extrapolate=False)

    splinex_l1 = interpolate.BSpline(knots, x_label1, 3, extrapolate=False)
    spliney_l1 = interpolate.BSpline(knots, y_label1, 3, extrapolate=False)
    splinez_l1 = interpolate.BSpline(knots, z_label1, 3, extrapolate=False)

    if (
        abs(splinex_l.derivative(1)(0)) > 3.5
        and (spliney_l.derivative(1)(0)) > 3.5
        and abs(splinez_l.derivative(1)(0)) > 3.5
    ):
        print(f"{splinex_l.derivative(1)(0),spliney_l.derivative(1)(0),splinez_l.derivative(1)(0)}")

    color = (0.0, 1.0, 0.0)

    success = True
    for tt_x in tt_short:
        if pc.size != 0:
            pp = np.array([splinex_l(tt_x), spliney_l(tt_x), splinez_l(tt_x)])
            dist = np.linalg.norm(pc - pp, axis=1)
            if any(dist < 0.15):
                color = (1.0, 0.0, 0.0)
                success = False
                break
        else:
            break

    print(f"rm -rf {dataset.train_files[j]}")
    if plot:
        mlab.figure(fgcolor=(0, 0, 0), bgcolor=(1, 1, 1))
        mlab.points3d(pc[:, 0], pc[:, 1], pc[:, 2], scale_factor=0.05)

        mlab.points3d(
            goal[0],
            goal[1],
            goal[2],
            color=(1, 0, 0),
            scale_factor=0.05,
            resolution=100,
        )

        mlab.outline(color=(0, 0, 0), extent=np.array([-5, 5, -5, 5, -5, 5]))

        axes = mlab.axes(color=(0, 0, 0), nb_labels=5)
        axes.title_text_property.color = (0.0, 0.0, 0.0)
        axes.title_text_property.font_family = "times"
        axes.label_text_property.color = (0.0, 0.0, 0.0)
        axes.label_text_property.font_family = "times"
        # mlab.savefig("vector_plot_in_3d.pdf")
        # mlab.gcf().scene.parallel_projection = True  # Source: <<https://stackoverflow.com/a/32531283/2729627>>.

        mlab.orientation_axes()
        mlab.plot3d(splinex_l(tt), spliney_l(tt), splinez_l(tt), color=color, tube_radius=0.05)
        mlab.plot3d(splinex_l1(tt), spliney_l1(tt), splinez_l1(tt), color=(1, 0, 0), tube_radius=0.05)

    save = False

    if save:
        traj = np.concatenate([x_label[:-3] / 5, y_label[:-3] / 5, z_label[:-3] / 5])
        data_dir = "data" + str(num) + "/"
        makedirs(data_path + data_dir + "Input", exist_ok=True)
        makedirs(data_path + data_dir + "Label", exist_ok=True)

        with open(data_path + data_dir + "Input/point_cloud.npy", "wb") as f:
            np.save(f, pc / 5)

        with open(data_path + data_dir + "Label/traj.npy", "wb") as f:
            np.save(f, traj)

        with open(data_path + data_dir + "Label/success.npy", "wb") as f:
            np.save(f, success)

        num += 1

    n_save = 0
    if multi:
        for i in range(100):
            trajlabel_rand = trajlabel + 5 * np.random.random(30) - 2.5
            trajlabel_rand[0] = trajlabel[0]
            trajlabel_rand[10] = trajlabel[10]
            trajlabel_rand[20] = trajlabel[20]
            x_label = trajlabel_rand[:10]
            y_label = trajlabel_rand[10:20]
            z_label = trajlabel_rand[20:]
            # x_label[0] =  x_label[0] + (0.1*np.random.rand(1)-0.05)
            # y_label[0] =  y_label[0] + (0.1*np.random.rand(1)-0.05)
            # z_label[0] =  z_label[0] + (0.1*np.random.rand(1)-0.05)
            # px = 4.5 * np.random.random(9) - 2.25
            # py = 4.5 * np.random.random(9) - 2.25
            # pz = 4.5 * np.random.random(9) - 2.25
            # x_label[1:] = x_label[1:] + px
            # y_label[1:] = y_label[1:] + py
            # z_label[1:] = z_label[1:] + pz

            x_label = torch.Tensor(x_label)
            y_label = torch.Tensor(y_label)
            z_label = torch.Tensor(z_label)

            traj = torch.concatenate([x_label, y_label, z_label])
            traj = qp_traj(traj / 5, vel, acc, pc_const) * 5
            x_label = traj[0, :nCls].numpy()
            y_label = traj[0, nCls : 2 * nCls].numpy()
            z_label = traj[0, 2 * nCls :].numpy()

            # x_label = qp_traj(x_label/5, vel[0],acc[0])*5
            # x_label = x_label[0,:].numpy()

            # y_label = qp_traj(y_label/5, vel[1], acc[1])*5
            # y_label = y_label[0,:].numpy()

            # z_label = qp_traj(z_label/5, vel[2], acc[2])*5
            # z_label = z_label[0,:].numpy()

            x_label = np.concatenate([x_label, np.array([x_label[-1]] * 3)])
            y_label = np.concatenate([y_label, np.array([y_label[-1]] * 3)])
            z_label = np.concatenate([z_label, np.array([z_label[-1]] * 3)])
            splinex_l = interpolate.BSpline(knots, x_label, 3, extrapolate=False)
            spliney_l = interpolate.BSpline(knots, y_label, 3, extrapolate=False)
            splinez_l = interpolate.BSpline(knots, z_label, 3, extrapolate=False)

            if (
                np.max(np.abs(splinex_l.derivative(1)(tt))) > 3.5
                or np.max(np.abs(spliney_l.derivative(1)(tt))) > 3.5
                or np.max(np.abs(splinez_l.derivative(1)(tt))) > 3.5
                or np.max(np.abs(splinex_l.derivative(2)(tt))) > 20
                or np.max(np.abs(spliney_l.derivative(2)(tt))) > 20
                or np.max(np.abs(splinez_l.derivative(2)(tt))) > 9.6
            ):
                pass  # print(True)

            # mlab.savefig("vector_plot_in_3d.pdf")
            # mlab.gcf().scene.parallel_projection = True  # Source: <<https://stackoverflow.com/a/32531283/2729627>>.
            color = (0.0, 1.0, 0.0)
            success = True
            for tt_x in tt_short:
                if pc.size != 0:
                    pp = np.array([splinex_l(tt_x), spliney_l(tt_x), splinez_l(tt_x)])
                    dist = np.linalg.norm(pc - pp, axis=1)
                    if any(dist < 0.15):
                        color = (1.0, 0.0, 0.0)
                        success = False
                        break
                else:
                    break
            if plot:
                mlab.plot3d(splinex_l(tt), spliney_l(tt), splinez_l(tt), color=color, tube_radius=0.005)

            save = False
            if success:
                if n_save < 10:
                    if success_num < 35000:
                        success_num += 1
                        n_save += 1
                        save = False
            else:
                print(torch.matmul(pc_const, traj.mT / 5))
                if failed_num < 35000:
                    failed_num += 1
                    save = False

            if save:
                traj = np.concatenate([x_label[:-3] / 5, y_label[:-3] / 5, z_label[:-3] / 5])
                data_dir = "data" + str(num) + "/"
                makedirs(data_path + data_dir + "Input", exist_ok=True)
                makedirs(data_path + data_dir + "Label", exist_ok=True)

                with open(data_path + data_dir + "Input/point_cloud.npy", "wb") as f:
                    np.save(f, pc / 5)

                with open(data_path + data_dir + "Label/traj.npy", "wb") as f:
                    np.save(f, traj)

                with open(data_path + data_dir + "Label/success.npy", "wb") as f:
                    np.save(f, success)

                num += 1

    if plot:
        mlab.show()
