import numpy as np
import os
import matplotlib.pyplot as plt
from LoadingDataset import MultiTrajDataset
from tqdm import tqdm
import mayavi
from mayavi import mlab
from mayavi.modules.grid_plane import GridPlane
from scipy.interpolate import BSpline
import scipy.interpolate as interpolate
from scipy.spatial.transform import Rotation
import os.path
from os import makedirs
import time

import torch
from qpth.qp import QPFunction, QPSolvers
from torch.autograd import Variable

import sys, os


# Disable
def blockPrint():
    sys.stdout = open(os.devnull, "w")


# Restore
def enablePrint():
    sys.stdout = sys.__stdout__


save = True
num = 1
success_num = 0
failed_num = 0
plot = False
multi = True
lim = 4.5
"""
knots = np.array(
    [
        0.0,
        0.0,
        0.0,
        0.0,
        0.18181818,
        0.27272727,
        0.36363636,
        0.45454545,
        0.54545455,
        0.63636364,
        0.72727273,
        0.81818182,
        1.0,
        1.0,
        1.0,
        1.0,
    ]
)
"""
knots = [
    0.0,
    0.0,
    0.0,
    0.0,
    0.63180516,
    0.95272206,
    1.26361032,
    1.58452722,
    1.90544413,
    2.22636103,
    2.53724928,
    2.85816619,
    3.5,
    3.5,
    3.5,
    3.5,
]

knots = torch.Tensor(knots)

nCls = 10
nineq = 2 * nCls - 2
neq = 3

idx = torch.linspace(0, nCls, nCls, dtype=torch.int32)
G1 = -torch.eye(nCls + 2, nCls + 2)
G1 = 3 * torch.diagonal_scatter(G1, torch.ones(nCls + 1), 1)
G1[:nCls, :] = (
    G1[:nCls, :]
    / (torch.index_select(knots, 0, idx + 3 + 1) - torch.index_select(knots, 0, idx + 1))[:, None]
)
G1[-3:, :] = 0.0
G2 = -torch.eye(nCls + 2, nCls + 2)
G2 = torch.diagonal_scatter(G2, torch.ones(nCls + 1), 1)
G2 = torch.matmul(G2, G1)
G2[: nCls - 1, :] = (
    2
    * G2[: nCls - 1, :]
    / (
        torch.index_select(knots, 0, idx[: nCls - 1] + 3 + 1)
        - torch.index_select(knots, 0, idx[: nCls - 1] + 2)
    )[:, None]
)
G_x = torch.vstack([G1[: nCls - 1, :nCls], G2[: nCls - 1, :nCls]])
G_x = G_x.repeat(2, 1)
G_x[nineq:, :] = -G_x[nineq:, :]
G = torch.zeros(nineq * 6, 3 * nCls)
G[: 2 * nineq, :nCls] = G_x
G[2 * nineq : 4 * nineq, nCls : 2 * nCls] = G_x
G[4 * nineq :, 2 * nCls :] = G_x

# eq constraints
A = torch.zeros(3 * neq, 3 * nCls)
A[0, 0] = 1.0
A[1, :nCls] = G_x[0, :]
A[2, :nCls] = G2[0, :nCls]
A[neq : 2 * neq, nCls : 2 * nCls] = A[:neq, :nCls]
A[2 * neq :, 2 * nCls :] = A[:neq, :nCls]

Q = Variable(1e3 * (torch.eye(3 * nCls, 3 * nCls)))
G = Variable(G)
A = Variable(A)
bv = Variable(torch.Tensor([1]))
hv = Variable(torch.ones(nCls - 1))
ha = Variable(torch.ones(nCls - 1))

v_max = [1.0, 1.0, 1.0]
a_max = [2.0, 2.0, 2.0]

v_max = [v_max[0] / lim, v_max[1] / lim, v_max[2] / lim]
a_max = [a_max[0] / lim, a_max[1] / lim, a_max[2] / lim]


def qp_traj(out, vel, acc, cc):
    pz = -1e3 * out
    batch = out.shape[0]
    hv_b = hv.reshape(1, nCls - 1).repeat(batch, 1)
    ha_b = ha.reshape(1, nCls - 1).repeat(batch, 1)
    vel = torch.Tensor(vel)
    acc = torch.Tensor(acc)
    hx = torch.hstack(
        [
            hv_b * v_max[0],
            ha_b * a_max[0],
            hv_b * v_max[0],
            ha_b * a_max[0],
            hv_b * v_max[1],
            ha_b * a_max[1],
            hv_b * v_max[1],
            ha_b * a_max[1],
            hv_b * v_max[2],
            ha_b * a_max[2],
            hv_b * v_max[2],
            ha_b * a_max[2],
        ]
    )
    # A = torch.Tensor()
    # A = self.A.reshape(1,2,8).repeat(batch,1,1)
    # bv = self.bv.reshape(1,1).repeat(batch,1)
    # ba = self.ba.reshape(1,1).repeat(batch,1)
    b = bv * torch.hstack(
        [
            torch.zeros((batch, 1)),
            vel[:, :1],
            acc[:, :1],
            torch.zeros((batch, 1)),
            vel[:, 1:2],
            acc[:, 1:2],
            torch.zeros((batch, 1)),
            vel[:, 2:],
            acc[:, 2:],
        ]
    )
    # bx = torch.hstack([bv*0.0,bv*vel])
    # bx = torch.Tensor()

    # G1 = torch.concatenate([G, cc], axis=0)
    # h1 = torch.concatenate([hx, -ha[:1] * 0.0], axis=0)

    blockPrint()
    out_x = QPFunction(verbose=1, maxIter=20, notImprovedLim=10)(Q, pz, G, hx, A, b)
    enablePrint()
    # by = torch.hstack([bv*vel[:,1:2],ba*acc[:,1:2]])
    return out_x


current_dir = os.getcwd()
dataset = MultiTrajDataset(
    current_dir + "/mader/dataset_multi/2024-05-08_14-12-33/", train_num=2.0, valid_num=0.0, seed=1
)
if save:
    data_path = current_dir + "/mader"
    data_path = data_path + "/dataset_multi_collision/" + time.strftime("%Y-%m-%d_%H-%M-%S/")
    makedirs(data_path, exist_ok=True)


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
train_cc_collision = "/trained_models/sas_model_cc.pt"
torch.set_grad_enabled(False)
model_cc = torch.load(current_dir + train_cc_collision)
model_cc.eval()
model_cc.to(device)

"""
knots = np.array(
    [
        0.0,
        0.0,
        0.0,
        0.0,
        0.29171598,
        0.45266272,
        0.6035503,
        0.76449704,
        0.91538462,
        1.07633136,
        1.22721893,
        1.38816568,
        1.7,
        1.7,
        1.7,
        1.7,
    ]
)
"""
knots = np.array(
    [
        0.0,
        0.0,
        0.0,
        0.0,
        0.63180516,
        0.95272206,
        1.26361032,
        1.58452722,
        1.90544413,
        2.22636103,
        2.53724928,
        2.85816619,
        3.5,
        3.5,
        3.5,
        3.5,
    ]
)
t_final = 3.5
tt = np.linspace(0, t_final, 100)
tt_short = np.linspace(0.0, 3.0, 100)


for j, (pc, pc_drone, _, _, goal, quat, vel, acc, trajlabel, state) in enumerate(tqdm(dataset)):
    pc_const = []

    # if np.any(trajlabel > 5):
    #    plot = True
    # pc_const = torch.cat((torch.Tensor(pc), torch.ones((max_num - pc.shape[0], 3))), dim=0)
    # pc_const = pc_const.unsqueeze(0)
    # pc_const = pc_const.to(device)
    # pc_const, _, _, _, _ = model_cc(pc_const)
    # pc_const = pc_const.cpu()
    # pc, goal, quat, vel, acc, trajlabel = dataset[4]
    goal = goal * lim + state
    # R = Rotation.from_quat(quat)
    pc = pc * lim
    pc_drone = pc_drone * lim
    # pc = R.apply(pc)
    vel = vel * v_max
    acc = acc * a_max

    traj_save = []
    success_save = []

    success = []
    color = []
    splinex_l = []
    spliney_l = []
    splinez_l = []
    splinex_l1 = []
    spliney_l1 = []
    splinez_l1 = []

    trajlabel_1 = torch.Tensor(trajlabel)
    trajlabel_1 = qp_traj(trajlabel_1 / lim, vel, acc, pc_const) * lim
    trajlabel_1 = trajlabel_1.numpy()

    pcN = pc + state.reshape(pc.shape[0], 1, 3)
    pcN = pcN.reshape(pc.shape[0] * pc.shape[1], 3)
    pcN_drone = pc_drone + state.reshape(pc_drone.shape[0], 1, 3)
    pcN_drone = pcN_drone.reshape(pc_drone.shape[0] * pc_drone.shape[1], 3)
    dist_cc = 0.09 * np.linspace(1, 0.5, 100)
    dist_drone_cc = 0.09 * np.linspace(1, 0.5, 100)

    for i in range(trajlabel.shape[0]):
        x_label = trajlabel[i, :10] + state[i, 0]
        x_label = np.concatenate([x_label, np.array([x_label[-1]] * 3)])
        y_label = trajlabel[i, 10:20] + state[i, 1]
        y_label = np.concatenate([y_label, np.array([y_label[-1]] * 3)])
        z_label = trajlabel[i, 20:] + state[i, 2]
        z_label = np.concatenate([z_label, np.array([z_label[-1]] * 3)])

        x_label1 = trajlabel_1[i, :10] + state[i, 0]
        x_label1 = np.concatenate([x_label1, np.array([x_label1[-1]] * 3)])
        y_label1 = trajlabel_1[i, 10:20] + state[i, 1]
        y_label1 = np.concatenate([y_label1, np.array([y_label1[-1]] * 3)])
        z_label1 = trajlabel_1[i, 20:] + state[i, 2]
        z_label1 = np.concatenate([z_label1, np.array([z_label1[-1]] * 3)])

        splinex_l.append(interpolate.BSpline(knots, x_label, 3, extrapolate=False))
        spliney_l.append(interpolate.BSpline(knots, y_label, 3, extrapolate=False))
        splinez_l.append(interpolate.BSpline(knots, z_label, 3, extrapolate=False))

        splinex_l1.append(interpolate.BSpline(knots, x_label1, 3, extrapolate=False))
        spliney_l1.append(interpolate.BSpline(knots, y_label1, 3, extrapolate=False))
        splinez_l1.append(interpolate.BSpline(knots, z_label1, 3, extrapolate=False))

        """
        if (
            abs(splinex_l[-1].derivative(1)(0)) > v_max[0]
            and (spliney_l[-1].derivative(1)(0)) > v_max[1]
            and abs(splinez_l[-1].derivative(1)(0)) > v_max[2]
        ):
            print(
                f"{splinex_l[-1].derivative(1)(0),spliney_l[-1].derivative(1)(0),splinez_l[-1].derivative(1)(0)}"
            )
        """

        dist_pc = np.linalg.norm(pcN - state[i], axis=1, ord=2)
        dist_drones = np.linalg.norm(np.delete(state, i, 0) - state[i], axis=1, ord=2)
        if any(dist_pc < dist_cc[0]) or any(dist_drones < dist_drone_cc[0]):
            print(f"rm -rf {dataset.train_files[j]}")
            # plot = True

        color.append((0.0, 1.0, 0.0))
        success.append(True)
        for k, tt_x in enumerate(tt_short):
            if pc.size != 0:
                pp = np.array([splinex_l[-1](tt_x), spliney_l[-1](tt_x), splinez_l[-1](tt_x)])
                dist_pc = np.linalg.norm(pcN - pp, axis=1, ord=2)
                dist_drones = np.linalg.norm(np.delete(state, i, 0) - pp, axis=1, ord=2)
                if any(dist_pc < dist_cc[k]) or any(dist_drones < dist_drone_cc[k]):
                    color[-1] = (1.0, 0.0, 0.0)
                    success[-1] = False
                    break
            else:
                break

    """if any(np.array(success) == False):
        print(f"rm -rf {dataset.train_files[j]}")
        plot = False"""

    if plot:
        mlab.figure(fgcolor=(0, 0, 0), bgcolor=(1, 1, 1))
        mlab.points3d(pcN[:, 0], pcN[:, 1], pcN[:, 2], scale_factor=0.05)
        mlab.points3d(pcN_drone[:, 0], pcN_drone[:, 1], pcN_drone[:, 2], scale_factor=0.05)

        mlab.points3d(
            state[:, 0],
            state[:, 1],
            state[:, 2],
            color=(1, 0, 0),
            scale_factor=0.1,
            resolution=100,
        )

        mlab.outline(color=(0, 0, 0), extent=np.array([-10, 10, -10, 10, -10, 10]))

        axes = mlab.axes(color=(0, 0, 0), nb_labels=5)
        axes.title_text_property.color = (0.0, 0.0, 0.0)
        axes.title_text_property.font_family = "times"
        axes.label_text_property.color = (0.0, 0.0, 0.0)
        axes.label_text_property.font_family = "times"

        mlab.orientation_axes()
        for i in range(pc.shape[0]):
            mlab.plot3d(
                splinex_l[i](tt),
                spliney_l[i](tt),
                splinez_l[i](tt),
                color=color[i],
                tube_radius=0.05,
            )
            mlab.plot3d(
                splinex_l1[i](tt),
                spliney_l1[i](tt),
                splinez_l1[i](tt),
                color=(1, 0, 0),
                tube_radius=0.05,
            )

    if save:
        traj = trajlabel / lim
        success = np.array(success).astype(np.bool_)
        success_save.append(success)
        traj_save.append(traj)

    n_save = 0
    if multi:
        for i in range(50):
            success = []
            color = []
            trajlabel_rand = trajlabel.copy()
            for k in range(trajlabel.shape[0]):
                trajlabel_rand[k, :] += 5 * np.random.random(30) - 2.5
            trajlabel_rand[:, 0] = trajlabel[:, 0]
            trajlabel_rand[:, 10] = trajlabel[:, 10]
            trajlabel_rand[:, 20] = trajlabel[:, 20]
            x_label = trajlabel_rand[:, :10]
            y_label = trajlabel_rand[:, 10:20]
            z_label = trajlabel_rand[:, 20:]

            x_label = torch.Tensor(x_label)
            y_label = torch.Tensor(y_label)
            z_label = torch.Tensor(z_label)

            traj = torch.concatenate([x_label, y_label, z_label], dim=1)
            traj = qp_traj(traj / lim, vel, acc, pc_const) * lim

            x_label = traj[:, :nCls].numpy() + state[:, :1]
            y_label = traj[:, nCls : 2 * nCls].numpy() + state[:, 1:2]
            z_label = traj[:, 2 * nCls :].numpy() + state[:, 2:]

            x_label = np.concatenate([x_label, np.array([x_label[:, -1].tolist()] * 3).T], axis=1)
            y_label = np.concatenate([y_label, np.array([y_label[:, -1].tolist()] * 3).T], axis=1)
            z_label = np.concatenate([z_label, np.array([z_label[:, -1].tolist()] * 3).T], axis=1)
            splinex_l = interpolate.BSpline(knots, x_label, 3, extrapolate=False, axis=1)
            spliney_l = interpolate.BSpline(knots, y_label, 3, extrapolate=False, axis=1)
            splinez_l = interpolate.BSpline(knots, z_label, 3, extrapolate=False, axis=1)

            color = [(0.0, 1.0, 0.0) for _ in range(x_label.shape[0])]
            success = [True for _ in range(x_label.shape[0])]
            for k in range(x_label.shape[0]):
                pp = np.stack([splinex_l(tt_short), spliney_l(tt_short), splinez_l(tt_short)])
                pp = np.transpose(pp, (1, 0, 2))
                if pc.size != 0:
                    dist_pc = np.linalg.norm(
                        pcN.reshape(pcN.shape[0], 1, 3).repeat(100, axis=1) - pp[k, ...].T,
                        axis=2,
                        ord=2,
                    )
                    dist_drones = np.linalg.norm(
                        np.delete(state, k, 0)
                        .reshape(x_label.shape[0] - 1, 1, 3)
                        .repeat(100, axis=1)
                        - pp[k, ...].T,
                        axis=2,
                        ord=2,
                    )
                    if np.any(dist_pc < dist_cc) or np.any(dist_drones < dist_drone_cc):
                        color[k] = (1.0, 0.0, 0.0)
                        success[k] = False
                else:
                    break

                if plot:
                    mlab.plot3d(
                        splinex_l(tt)[k, :],
                        spliney_l(tt)[k, :],
                        splinez_l(tt)[k, :],
                        color=color[k],
                        tube_radius=0.005,
                    )

            save = False
            if np.all(success):
                if n_save < 10:
                    if success_num < 35000:
                        success_num += 1
                        n_save += 1
                        save = True
            else:
                # print(torch.matmul(pc_const, traj.mT / 5))
                if failed_num < 35000:
                    failed_num += 1
                    save = True

            if save:
                traj = traj.numpy() / lim
                success = np.array(success).astype(np.bool_)
                success_save.append(success)
                traj_save.append(traj)

    if len(traj_save) > 0:
        save = True
    else:
        save = False
    if save:
        data_dir = "data" + str(num) + "/"
        makedirs(data_path + data_dir + "Input", exist_ok=True)
        makedirs(data_path + data_dir + "Label", exist_ok=True)

        with open(data_path + data_dir + "Input/point_cloud.npy", "wb") as f:
            np.save(f, pc / lim)

        with open(data_path + data_dir + "Input/point_cloud_drone.npy", "wb") as f:
            np.save(f, pc_drone / lim)

        with open(data_path + data_dir + "Label/traj.npy", "wb") as f:
            traj_save = np.array(traj_save)
            np.save(f, traj_save)

        with open(data_path + data_dir + "Input/state.npy", "wb") as f:
            np.save(f, state)

        with open(data_path + data_dir + "Label/success.npy", "wb") as f:
            success_save = np.array(success_save)
            np.save(f, success_save)

        num += 1

    if plot:
        mlab.show()

    # plot = False
