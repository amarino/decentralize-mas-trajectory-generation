#!/usr/bin/env python3
import torch
from torch.utils.data import DataLoader

import os
from tqdm import tqdm
import matplotlib.pyplot as plt
import sys
import numpy as np
import copy
import random
from mayavi import mlab

from GNNRegularization import GNNRegularization as Regularizer
from MASTrajModel import LossFunction, MultiCollisionTrajModel
from LoadingDataset import Sampler, MultiCollisionDataset

from scipy.interpolate import BSpline
import scipy.interpolate as interpolate


class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("result.txt", "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass


class train_test_valid:
    def __init__(self, regularizer, loss_function, loss_, optimizer) -> None:
        self.regularizer = regularizer
        self.loss_function = loss_function
        self.loss_ = loss_
        self.optimizer = optimizer

    def __call__(self, model, **kwargs):
        point_cloud = kwargs["point_cloud"].to(device)
        A = kwargs["A"].to(device)
        #Apc = kwargs["Apc"].to(device)
        success = kwargs["success"].to(device)
        trajlabel = kwargs["trajlabel"].to(device)
        dim = kwargs["dim"].to(device)

        with torch.no_grad():
            self.regularizer.model = model

        self.optimizer.zero_grad()
        out, _, _ = model(point_cloud, torch.Tensor([[]]), A)

        # trajlabel = model.qp_traj(trajlabel,vel,acc)

        loss = self.loss_function(out, trajlabel, success, dim)
        regularization = self.regularizer()
        loss = loss + regularization

        return loss, out, regularization


sys.stdout = Logger()

# Device configuration
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Train folder
train_model = "/trained_models/mas_model_cc_crazy.pt"
current_dir = os.getcwd()
os.makedirs(current_dir + "/trained_models", exist_ok=True)
# Hyper-parameters
epochs = 200
batch = 13
random_size = 1
learning_rate = 1e-3
id = 0
mode = "train"

loss_function = LossFunction()
dataset = MultiCollisionDataset(
    current_dir + "/mader/dataset_multi_collision/2024-05-11_16-04-32/", train_num=0.7, valid_num=0.2
)

current_dir = os.getcwd()
train_mas = "/trained_models/mas_traj_model_BS_crazy.pt"

model_mas = torch.load(current_dir + train_mas)
model_mas.eval()
model_mas = model_mas.to(device)

# torch.autograd.set_detect_anomaly(True)
# constraints = WeightConstraints()

print("Training....")
print("###########  " + mode + "  ###############")

######################   Train    ####################
if mode == "train":
    show_loss = []
    show_loss_iter = []
    loss = 0.0

    model_mas_collision = MultiCollisionTrajModel(device=device).to(device)

    optimizer = torch.optim.AdamW(model_mas_collision.parameters(), lr=learning_rate, weight_decay=1e-8)

    model_mas_collision.point_net = model_mas.point_net

    regularizer = Regularizer(model_mas_collision, C=1)

    regularizer.weights = []
    regularizer.regularization = []

    # regularizer.id3x3 = torch.eye(3).to(device).repeat(batch, 1, 1)
    # regularizer.id64x64 = torch.eye(64).to(device).repeat(batch, 1, 1)

    ttv = train_test_valid(
        regularizer=regularizer,
        loss_function=loss_function.compute_collision_loss,
        loss_=loss_function,
        optimizer=optimizer,
    )

    dataset.mode = "valid"
    valid_loader = DataLoader(
        copy.deepcopy(dataset),
        batch_size=10,
        shuffle=True,
        collate_fn=dataset.loading,
    )

    dataset.mode = "train"
    train_loader = DataLoader(
        copy.deepcopy(dataset),
        batch_size=batch,
        shuffle=True,
        collate_fn=dataset.loading,
    )

    show_regularization = []

    model_mas_collision.train()
    for i in range(epochs):
        for point_cloud, Apt, A, success, trajlabel, dim in tqdm(train_loader):
            loss, _, regularization = ttv(
                model=model_mas_collision,
                Apc=Apt,
                A=A,
                point_cloud=point_cloud,
                success=success,
                trajlabel=trajlabel,
                dim=dim,
            )

            loss.backward()
            ttv.optimizer.step()
            ttv.regularizer.m3x3 = None
            ttv.regularizer.m64x64 = None
            # torch.cuda.empty_cache()
            show_loss_iter.append(loss.item())
            show_regularization.append(regularization.item())

        with torch.no_grad():
            if (i + 1) % 1 == 0:
                loss = sum(show_loss_iter) / len(show_loss_iter)
                show_loss.append(loss)
                print(f"Epoch [{i}/{epochs}], Loss: {loss:.4f}")
                regularization = sum(show_regularization) / len(show_regularization)
                print(f"Epoch [{i}/{epochs}], Regularization Loss: {regularization:.4f}")

            ###### validation #######

            if (i + 1) % 10 == 0:
                model_mas_collision.eval()
                show_loss_iter = []
                for point_cloud, Apt, A, success, trajlabel, dim in tqdm(valid_loader):
                    loss, out, _ = ttv(
                        model=model_mas_collision,
                        Apc=Apt,
                        A=A,
                        point_cloud=point_cloud,
                        success=success,
                        trajlabel=trajlabel,
                        dim=dim,
                    )
                    show_loss_iter.append(loss.item())

                loss = sum(show_loss_iter) / len(show_loss_iter)
                show_loss.append(loss)
                print(f"Epoch [{i}/{epochs}], Validation Loss: {loss:.4f}")
                model_mas_collision.train()

            if i > epochs:
                if loss < 4e-4:
                    break

        show_loss_iter = []
        show_regularization = []

    # model_ts = torch.jit.script(model_sas.cpu())  # Export to TorchScript
    torch.save(model_mas_collision.cpu(), current_dir + train_model)  # Save

########################### TEST ##############################
model_mas_collision = MultiCollisionTrajModel(device)

current_dir = os.getcwd()
train_mas_collision = "/trained_models/sas_model_cc.pt"

model_mas_collision = torch.load(current_dir + train_mas_collision)
model_mas_collision.eval()

model_mas_collision.to(device)
knots = np.array(
    [
        0.0,
        0.0,
        0.0,
        0.0,
        0.29171598,
        0.45266272,
        0.6035503,
        0.76449704,
        0.91538462,
        1.07633136,
        1.22721893,
        1.38816568,
        1.7,
        1.7,
        1.7,
        1.7,
    ]
)


with torch.no_grad():
    ##########################################
    ##########                      ##########
    ##########     Test model sas   ##########
    ##########                      ##########
    ##########################################
    if mode == "test":
        dataset.mode = "test"
        test_loader = DataLoader(copy.deepcopy(dataset), batch_size=1, shuffle=True, collate_fn=dataset.loading)

        optimizer_sas = torch.optim.AdamW(model_mas_collision.parameters(), lr=learning_rate, weight_decay=1e-5)

        ttv = train_test_valid(
            regularizer=Regularizer(model_mas_collision, C=1),
            loss_function=loss_function.compute_collision_loss,
            loss_=loss_function,
            optimizer=optimizer_sas,
        )

        for pc, Apt, A, success, trajlabel, dim in tqdm(test_loader):
            if pc.shape[1] == 0:
                pc = torch.ones((1, 10, 3))
            loss, out, _ = ttv(
                model=model_mas_collision, Apc=Apt, A=A, point_cloud=pc, success=success, trajlabel=trajlabel, dim=dim
            )

            success = success.numpy()
            pc = pc[0, :, :].cpu().numpy()
            trajlabel = trajlabel.cpu().numpy() * 5
            pc = pc * 5
            # pc = R.apply(pc)

            if success:
                color = (0.0, 1.0, 0.0)
            else:
                color = (1.0, 0.0, 0.0)

            plot = False
            if loss > 0.01:
                print(f"success: {success}, loss:{loss.item():.4f}")
                plot = True

            x_label = trajlabel[0, :10]
            x_label = np.concatenate([x_label, np.array([x_label[-1]] * 3)])
            y_label = trajlabel[0, 10:20]
            y_label = np.concatenate([y_label, np.array([y_label[-1]] * 3)])
            z_label = trajlabel[0, 20:]
            z_label = np.concatenate([z_label, np.array([z_label[-1]] * 3)])

            splinex_l = interpolate.BSpline(knots, x_label, 3, extrapolate=False)
            spliney_l = interpolate.BSpline(knots, y_label, 3, extrapolate=False)
            splinez_l = interpolate.BSpline(knots, z_label, 3, extrapolate=False)

            if plot:
                mlab.figure(fgcolor=(0, 0, 0), bgcolor=(1, 1, 1))
                mlab.points3d(pc[:, 0], pc[:, 1], pc[:, 2], scale_factor=0.05)

                mlab.outline(color=(0, 0, 0), extent=np.array([-5, 5, -5, 5, -5, 5]))

                axes = mlab.axes(color=(0, 0, 0), nb_labels=5)
                axes.title_text_property.color = (0.0, 0.0, 0.0)
                axes.title_text_property.font_family = "times"
                axes.label_text_property.color = (0.0, 0.0, 0.0)
                axes.label_text_property.font_family = "times"
                # mlab.savefig("vector_plot_in_3d.pdf")
                # mlab.gcf().scene.parallel_projection = True  # Source: <<https://stackoverflow.com/a/32531283/2729627>>.

                tt = np.linspace(0, 1.7, 100)
                mlab.orientation_axes()
                mlab.plot3d(splinex_l(tt), spliney_l(tt), splinez_l(tt), color=color, tube_radius=0.05)
                mlab.show()
