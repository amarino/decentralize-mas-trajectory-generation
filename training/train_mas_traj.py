#!/usr/bin/env python3
import torch
from torch.optim.lr_scheduler import CosineAnnealingWarmRestarts, ExponentialLR
from torch.utils.data import DataLoader

import os
import time
from tqdm import tqdm
import matplotlib.pyplot as plt
import sys
import numpy as np
import copy
from mayavi import mlab

from GNNRegularization import GNNRegularization as Regularizer
from MASTrajModel import SASTrajModel, LossFunction, MASTrajModel, saliency_map
from LoadingDataset import Sampler, SingleTrajDataset, MultiTrajDataset

from scipy.interpolate import BSpline
import scipy.interpolate as interpolate


class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("result.txt", "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass


class train_test_valid:
    def __init__(self, regularizer, loss_function, loss_, optimizer) -> None:
        self.regularizer = regularizer
        self.loss_function = loss_function
        self.loss_ = loss_
        self.optimizer = optimizer

    def __call__(self, model, **kwargs):
        self.optimizer.zero_grad()
        point_cloud = kwargs["point_cloud"].to(device)
        A = kwargs["A"].to(device)
        Apc = kwargs["Apc"].to(device)
        goal = kwargs["goal"].to(device)
        quat = kwargs["quat"].to(device)
        vel = kwargs["vel"].to(device)
        acc = kwargs["acc"].to(device)
        trajlabel = kwargs["trajlabel"].to(device)

        with torch.no_grad():
            self.regularizer.model = model

        out, m3x3, m64x64, disp_out = model(point_cloud, goal, quat, vel, acc, Apc, A)

        self.regularizer.m3x3 = m3x3
        self.regularizer.m64x64 = m64x64

        loss = self.loss_function(out, trajlabel)
        regularization = self.regularizer()
        loss = loss + regularization

        return loss, out, regularization, disp_out


sys.stdout = Logger()

# Device configuration
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Train folder
train_model = "/trained_models/mas_traj_model_BS_crazy.pt"
current_dir = os.getcwd()
os.makedirs(current_dir + "/trained_models", exist_ok=True)
# Hyper-parameters
epochs = 200
batch = 13
random_size = 1
learning_rate = 1e-3
id = 0
mode = "test"
lim = 4.5

knots = [
    0.0,
    0.0,
    0.0,
    0.0,
    0.63180516,
    0.95272206,
    1.26361032,
    1.58452722,
    1.90544413,
    2.22636103,
    2.53724928,
    2.85816619,
    3.5,
    3.5,
    3.5,
    3.5,
]

v_max = [1.0, 1.0, 1.0]
a_max = [2.0, 2.0, 2.0]


loss_function = LossFunction()
dataset = MultiTrajDataset(
    current_dir + "/mader/dataset_multi/2024-05-08_14-12-33/",
    train_num=0.7,
    valid_num=0.2,
)
current_dir = os.getcwd()
train_mas_collision = "/trained_models/mas_model_cc.pt"

model_mas_collision = torch.load(current_dir + train_mas_collision)
model_mas_collision.eval()
model_mas_collision = model_mas_collision.to(device)

# torch.autograd.set_detect_anomaly(True)
# constraints = WeightConstraints()

print("Training....")
print("###########  " + mode + "  ###############")

######################   Train    ####################
if mode == "train":
    show_loss = []
    show_loss_iter = []
    loss = 0.0

    model_mas = MASTrajModel(v_max, a_max, knots, qp_enabled=False, device=device).to(device)

    optimizer = torch.optim.AdamW(model_mas.parameters(), lr=learning_rate, weight_decay=1e-7)
    regularizer = Regularizer(model_mas, C=1)

    # regularizer.id3x3 = torch.eye(3).to(device).repeat(batch, 1, 1)
    # regularizer.id64x64 = torch.eye(64).to(device).repeat(batch, 1, 1)

    model_mas.collision_constraint = model_mas_collision
    # model_mas.compute_cc = True

    regularizer.weights = [1e-4]
    regularizer.regularization = [regularizer.point_net]

    ttv = train_test_valid(
        regularizer=regularizer,
        loss_function=loss_function.compute_loss,
        loss_=loss_function,
        optimizer=optimizer,
    )

    dataset.mode = "valid"
    valid_loader = DataLoader(
        copy.deepcopy(dataset),
        batch_size=10,
        shuffle=True,
        collate_fn=dataset.loading,
    )

    dataset.mode = "train"
    train_loader = DataLoader(
        copy.deepcopy(dataset),
        batch_size=batch,
        shuffle=True,
        collate_fn=dataset.loading,
    )

    show_regularization = []

    model_mas.train()
    DEBUG = False
    for i in range(epochs):
        for point_cloud, Apc, A, goal, quat, vel, acc, trajlabel, _ in tqdm(train_loader):
            loss, _, regularization, _ = ttv(
                model=model_mas,
                Apc=Apc,
                A=A,
                point_cloud=point_cloud,
                goal=goal,
                quat=quat,
                vel=vel,
                acc=acc,
                trajlabel=trajlabel,
            )

            loss.backward()
            ttv.optimizer.step()
            ttv.regularizer.m3x3 = None
            ttv.regularizer.m64x64 = None

            show_loss_iter.append(loss.item())
            show_regularization.append(regularization.item())
            
        with torch.no_grad():
            if (i + 1) % 1 == 0:
                loss = sum(show_loss_iter) / len(show_loss_iter)
                show_loss.append(loss)
                print(f"Epoch [{i}/{epochs}], Loss: {loss:.4f}")
                regularization = sum(show_regularization) / len(show_regularization)
                print(f"Epoch [{i}/{epochs}], Regularization Loss: {regularization:.4f}")

            ###### validation #######

            if (i + 1) % 10 == 0:
                model_mas.eval()
                show_loss_iter = []
                for point_cloud, Apc, A, goal, quat, vel, acc, trajlabel, _ in tqdm(valid_loader):
                    loss, out, _, _ = ttv(
                        model=model_mas,
                        Apc=Apc,
                        A=A,
                        point_cloud=point_cloud,
                        goal=goal,
                        quat=quat,
                        vel=vel,
                        acc=acc,
                        trajlabel=trajlabel,
                    )
                    show_loss_iter.append(loss.item())

                if DEBUG:
                    out = out.cpu().numpy()
                    trajlabel = trajlabel.cpu().numpy()
                    for i in range(out.shape[0]):
                        t = model_mas.knots.cpu().numpy()
                        out[i, :] = dataset.de_nromalize(out[i, :])
                        trajlabel[i, :] = dataset.de_nromalize(trajlabel[i, :])

                        cx = out[i, :10]
                        cx = np.concatenate([cx, np.array([cx[-1]] * 3)])
                        cy = out[i, 10:20]
                        cy = np.concatenate([cy, np.array([cy[-1]] * 3)])
                        cz = out[i, 20:]
                        cz = np.concatenate([cz, np.array([cz[-1]] * 3)])

                        x_label = trajlabel[i, :10]
                        x_label = np.concatenate([x_label, np.array([x_label[-1]] * 3)])
                        y_label = trajlabel[i, 10:20]
                        y_label = np.concatenate([y_label, np.array([y_label[-1]] * 3)])
                        z_label = trajlabel[i, 20:]
                        z_label = np.concatenate([z_label, np.array([z_label[-1]] * 3)])

                        splinex = interpolate.BSpline(t, cx, 3, extrapolate=False)
                        spliney = interpolate.BSpline(t, cy, 3, extrapolate=False)
                        splinez = interpolate.BSpline(t, cz, 3, extrapolate=False)

                        splinex_l = interpolate.BSpline(t, x_label, 3, extrapolate=False)
                        spliney_l = interpolate.BSpline(t, y_label, 3, extrapolate=False)
                        splinez_l = interpolate.BSpline(t, z_label, 3, extrapolate=False)

                        tt = np.linspace(0, 1.7, 100)

                        fig = plt.figure()
                        ax = plt.axes(projection="3d")
                        ax.plot3D(splinex(tt), spliney(tt), splinez(tt), "red")
                        ax.plot3D(splinex_l(tt), spliney_l(tt), splinez_l(tt), "green")

                        plt.grid()
                        plt.show()

                loss = sum(show_loss_iter) / len(show_loss_iter)
                show_loss.append(loss)
                print(f"Epoch [{i}/{epochs}], Validation Loss: {loss:.4f}")
                model_mas.train()

            if i > 5:
                model_mas.qp_enabled = False
            if i > epochs:
                if loss < 4e-4:
                    break

        show_loss_iter = []
        show_regularization = []

    model_mas.goal_mean = dataset.goal_mean
    model_mas.goal_std = dataset.goal_std
    model_mas.vel_mean = dataset.vel_mean
    model_mas.vel_std = dataset.vel_std
    # model_ts = torch.jit.script(model_sas.cpu())  # Export to TorchScript
    torch.save(model_mas.cpu(), current_dir + train_model)  # Save

########################### TEST ##############################
model_mas = MASTrajModel(v_max, a_max, knots, device=device).to(device)

current_dir = os.getcwd()
train_mas = "/trained_models/mas_traj_model_BS_crazy.pt"

model_mas = torch.load(current_dir + train_mas)
model_mas.eval()

current_dir = os.getcwd()
train_mas_collision = "/trained_models/mas_model_cc_crazy.pt"

model_mas_collision = torch.load(current_dir + train_mas_collision)
model_mas_collision.eval()
model_mas_collision = model_mas_collision.to(device)
model_mas.collision_constraint = model_mas_collision
model_mas.qp_enabled = True
model_mas.use_cc = True
model_mas.compute_cc = True
model_mas.alpha = 0.0

torch.save(model_mas.cpu(), current_dir + train_mas)

model_mas.to(device)

plot = False

with torch.no_grad():
    ##########################################
    ##########                      ##########
    ##########     Test model mas   ##########
    ##########                      ##########
    ##########################################
    if mode == "test":
        dataset.mode = "test"
        test_loader = DataLoader(
            copy.deepcopy(dataset), batch_size=10, shuffle=False, collate_fn=dataset.loading
        )

        optimizer_sas = torch.optim.AdamW(
            model_mas.parameters(), lr=learning_rate, weight_decay=1e-5
        )

        ttv = train_test_valid(
            regularizer=Regularizer(model_mas, C=1),
            loss_function=loss_function.compute_loss,
            loss_=loss_function,
            optimizer=optimizer_sas,
        )

        for point_cloud, Apc, A, goal, quat, vel, acc, trajlabel, state in tqdm(test_loader):

            start = time.time()
            loss, out, _, disp_out = ttv(
                model=model_mas,
                A=A,
                Apc=Apc,
                point_cloud=point_cloud,
                goal=goal,
                quat=quat,
                vel=vel,
                acc=acc,
                trajlabel=trajlabel,
            )
            print(time.time() - start)
            print(f"loss:{loss.item():.4f}")
            out = out.cpu().numpy()
            trajlabel = trajlabel.cpu().numpy()
            state = state.numpy()

            batch = out.shape[0]
            N = out.shape[1]

            # vis
            # pt_disp = saliency_map(disp_out)

            # pt_disp = torch.unflatten(pt_disp, 0, (batch, N))
            # pt_disp = torch.flatten(pt_disp, start_dim=1)
            # pt_disp = pt_disp.cpu().numpy()

            t = model_mas.knots.cpu().numpy()
            out = dataset.de_nromalize(out)
            trajlabel = dataset.de_nromalize(trajlabel)

            for i in range(out.shape[0]):
                splinex = []
                splinex_l = []
                spliney = []
                spliney_l = []
                splinez = []
                splinez_l = []
                for k in range(out.shape[1]):
                    cx = out[i, k, :10] + state[i, k, 0]
                    cx = np.concatenate([cx, np.array([cx[-1]] * 3), np.array([0.0] * 3)])
                    cy = out[i, k, 10:20] + state[i, k, 1]
                    cy = np.concatenate([cy, np.array([cy[-1]] * 3), np.array([0.0] * 3)])
                    cz = out[i, k, 20:] + state[i, k, 2]
                    cz = np.concatenate([cz, np.array([cz[-1]] * 3), np.array([0.0] * 3)])

                    x_label = trajlabel[i, k, :10] + state[i, k, 0]
                    x_label = np.concatenate(
                        [x_label, np.array([x_label[-1]] * 3), np.array([0.0] * 3)]
                    )
                    y_label = trajlabel[i, k, 10:20] + state[i, k, 1]
                    y_label = np.concatenate(
                        [y_label, np.array([y_label[-1]] * 3), np.array([0.0] * 3)]
                    )
                    z_label = trajlabel[i, k, 20:] + state[i, k, 2]
                    z_label = np.concatenate(
                        [z_label, np.array([z_label[-1]] * 3), np.array([0.0] * 3)]
                    )

                    # print(f"state {x_label[0], y_label[0], z_label[0]}")
                    # print(f"label x {x_label}")
                    # print(f"label y {y_label}")
                    # print(f"label z {z_label}")

                    # print(f"pred x {cx}")
                    # print(f"pred y {cy}")
                    # print(f"pred z {cz}")

                    splinex.append(BSpline(t, cx, 3, extrapolate=False))
                    spliney.append(BSpline(t, cy, 3, extrapolate=False))
                    splinez.append(BSpline(t, cz, 3, extrapolate=False))

                    splinex_l.append(BSpline(t, x_label, 3, extrapolate=False))
                    spliney_l.append(BSpline(t, y_label, 3, extrapolate=False))
                    splinez_l.append(BSpline(t, z_label, 3, extrapolate=False))

                if plot:
                    color = (1.0, 0.0, 0.0)
                    pc = point_cloud[i, :, :, :].cpu().numpy() * lim + state[i, :, :].reshape(
                        point_cloud.shape[1], 1, 3
                    )

                    pc = pc.reshape(pc.shape[0] * pc.shape[1], -1)
                    mlab.figure(fgcolor=(0, 0, 0), bgcolor=(1, 1, 1))
                    pts = mlab.pipeline.scalar_scatter(pc[:, 0], pc[:, 1], pc[:, 2])

                    rgba = np.zeros((pc.shape[0], 4), dtype=np.uint8)
                    rgba[:, -1] = 255 * pt_disp[i, :]  # no transparency

                    """
                    color = (1.0, 0.0, 0.0)
                    pc = point_cloud[i, :, :, :].cpu().numpy() * lim + state[i, :, :].reshape(point_cloud.shape[1], 1, 3)

                    pc_adj = []
                    pc_disp_adj = []
                    for p in range(pc.shape[0]):
                        pc_adj_i = {}
                        for j in range(i, pc.shape[0]):
                            if p != j:
                                for k in range(pc.shape[1]):
                                    dist = np.linalg.norm(pc[p, :, :] - pc[j, k, :], axis=1)
                                    dist_w = np.where(dist < 2e-1)[0]
                                    for d in dist_w:
                                        pc_adj_i[d] = dist[d]
                        if len(list(pc_adj_i.keys())) == 0:
                            pc_adj_i[0] = 0
                        pc_adj.append(pc[p, list(pc_adj_i.keys())[:], :])
                        pc_disp_adj.append(pt_disp[i, p, list(pc_adj_i.keys())[:]])

                    pc_adj = np.concatenate(pc_adj, axis=0)
                    pc_disp_adj = np.concatenate(pc_disp_adj, axis=0)
                    # pc_adj = pc_adj.reshape(pc_adj.shape[0] * pc_adj.shape[1], -1)
                    # pc_disp_adj = pc_disp_adj.reshape(pc_disp_adj.shape[0] * pc_disp_adj.shape[1])
                    mlab.figure(fgcolor=(0, 0, 0), bgcolor=(1, 1, 1))
                    pts = mlab.pipeline.scalar_scatter(pc_adj[:, 0], pc_adj[:, 1], pc_adj[:, 2])

                    rgba = np.zeros((pc_adj.shape[0], 4), dtype=np.uint8)
                    rgba[:, -1] = 255 * pc_disp_adj[:]  # no transparency
                    # rgba[:, 0] = 255 * pt_disp[i, :]
                    """
                    pts.add_attribute(rgba, "colors")  # assign the colors to each point
                    pts.data.point_data.set_active_scalars("colors")
                    g = mlab.pipeline.glyph(pts)
                    g.glyph.glyph.scale_factor = 0.1  # set scaling for all the points
                    g.glyph.scale_mode = "data_scaling_off"  # make all the points same size

                    mlab.outline(color=(0, 0, 0), extent=np.array([-5, 5, -5, 5, -5, 5]))

                    axes = mlab.axes(color=(0, 0, 0), nb_labels=5)
                    axes.title_text_property.color = (0.0, 0.0, 0.0)
                    axes.title_text_property.font_family = "times"
                    axes.label_text_property.color = (0.0, 0.0, 0.0)
                    axes.label_text_property.font_family = "times"
                    # mlab.savefig("vector_plot_in_3d.pdf")
                    # mlab.gcf().scene.parallel_projection = True  # Source: <<https://stackoverflow.com/a/32531283/2729627>>.

                    tt = np.linspace(0, 1.7, 100)
                    mlab.orientation_axes()

                    for k in range(len(splinex_l)):
                        mlab.plot3d(
                            splinex_l[k](tt),
                            spliney_l[k](tt),
                            splinez_l[k](tt),
                            color=(0.0, 0.0, 1.0),
                            tube_radius=0.05,
                        )
                        mlab.plot3d(
                            splinex[k](tt),
                            spliney[k](tt),
                            splinez[k](tt),
                            color=(0.0, 1.0, 0.0),
                            tube_radius=0.05,
                        )

                    mlab.show()

                    fig = plt.figure()

                    for k in range(len(splinex_l)):
                        plt.plot(tt, splinex[k].derivative(1)(tt), label="pred vx")
                        plt.plot(tt, spliney[k].derivative(1)(tt), label="pred vy")
                        plt.plot(tt, splinez[k].derivative(1)(tt), label="pred vz")

                        plt.plot(tt, splinex_l[k].derivative(1)(tt), label=" vx")
                        plt.plot(tt, spliney_l[k].derivative(1)(tt), label=" vy")
                        plt.plot(tt, splinez_l[k].derivative(1)(tt), label=" vz")

                    plt.grid()
                    plt.legend()
                    plt.show()
