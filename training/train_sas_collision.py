#!/usr/bin/env python3
import torch
import torch.nn as nn
from torch.optim.lr_scheduler import CosineAnnealingWarmRestarts, ExponentialLR
from torch.utils.data import DataLoader

import os
from tqdm import tqdm
import matplotlib.pyplot as plt
import sys
import numpy as np
import copy
import random
from mayavi import mlab

from GNNRegularization import GNNRegularization as Regularizer
from MASTrajModel import LossFunction, CollisionTrajModel
from LoadingDataset import Sampler, CollisionDataset

from scipy.interpolate import BSpline
import scipy.interpolate as interpolate


class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("result.txt", "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass


class train_test_valid:
    def __init__(self, regularizer, loss_function, loss_, optimizer) -> None:
        self.regularizer = regularizer
        self.loss_function = loss_function
        self.loss_ = loss_
        self.optimizer = optimizer

    def __call__(self, model, **kwargs):
        point_cloud = kwargs["point_cloud"].to(device)
        success = kwargs["success"].to(device)
        trajlabel = kwargs["trajlabel"].to(device)

        with torch.no_grad():
            self.regularizer.model = model

        self.optimizer.zero_grad()
        out, _ = model(point_cloud)

        # trajlabel = model.qp_traj(trajlabel,vel,acc)

        loss = self.loss_function(out, trajlabel, success)
        regularization = self.regularizer()
        loss = loss + regularization

        return loss, out, regularization


sys.stdout = Logger()

# Device configuration
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Train folder
train_model = "/trained_models/sas_model_cc.pt"
current_dir = os.getcwd()
os.makedirs(current_dir + "/trained_models", exist_ok=True)
# Hyper-parameters
epochs = 400
batch = 500
random_size = 1
learning_rate = 1e-3
id = 0
mode = "test"

loss_function = LossFunction()
dataset = CollisionDataset(current_dir + "/mader/dataset_collision/2023-11-28_15-33-09/", train_num=0.7, valid_num=0.2)

current_dir = os.getcwd()
train_sas = "/trained_models/sas_traj_model_BS.pt"

model_sas = torch.load(current_dir + train_sas)
model_sas.eval()
model_sas = model_sas.to(device)

# torch.autograd.set_detect_anomaly(True)
# constraints = WeightConstraints()

print("Training....")
print("###########  " + mode + "  ###############")

######################   Train    ####################
if mode == "train":
    show_loss = []
    show_loss_iter = []
    loss = 0.0

    model_sas_collision = CollisionTrajModel(device=device).to(device)

    optimizer = torch.optim.Adam(model_sas_collision.parameters(), lr=learning_rate, weight_decay=1e-7)

    model_sas_collision.point_net = model_sas.point_net

    regularizer = Regularizer(model_sas_collision, C=1)

    regularizer.weights = []
    # regularizer.regularization = []

    # regularizer.id3x3 = torch.eye(3).to(device).repeat(batch, 1, 1)
    # regularizer.id64x64 = torch.eye(64).to(device).repeat(batch, 1, 1)

    ttv = train_test_valid(
        regularizer=regularizer,
        loss_function=loss_function.compute_collision_loss,
        loss_=loss_function,
        optimizer=optimizer,
    )

    dataset.mode = "valid"
    valid_loader = DataLoader(
        copy.deepcopy(dataset),
        batch_size=10,
        shuffle=True,
        collate_fn=dataset.loading,
    )

    dataset.mode = "train"
    train_loader = DataLoader(
        copy.deepcopy(dataset),
        batch_size=batch,
        shuffle=True,
        collate_fn=dataset.loading,
    )

    show_regularization = []

    model_sas_collision.train()
    for i in range(epochs):
        for point_cloud, success, trajlabel in tqdm(train_loader):
            loss, _, regularization = ttv(
                model=model_sas_collision,
                point_cloud=point_cloud,
                success=success,
                trajlabel=trajlabel,
            )

            loss.backward()
            ttv.optimizer.step()
            ttv.regularizer.m3x3 = None
            ttv.regularizer.m64x64 = None
            # torch.cuda.empty_cache()
            show_loss_iter.append(loss.item())
            show_regularization.append(regularization.item())

        with torch.no_grad():
            if (i + 1) % 1 == 0:
                loss = sum(show_loss_iter) / len(show_loss_iter)
                show_loss.append(loss)
                print(f"Epoch [{i}/{epochs}], Loss: {loss:.4f}")
                regularization = sum(show_regularization) / len(show_regularization)
                print(f"Epoch [{i}/{epochs}], Regularization Loss: {regularization:.4f}")

            ###### validation #######

            if (i + 1) % 10 == 0:
                model_sas_collision.eval()
                show_loss_iter = []
                for point_cloud, success, trajlabel in tqdm(valid_loader):
                    loss, out, _ = ttv(
                        model=model_sas_collision,
                        point_cloud=point_cloud,
                        success=success,
                        trajlabel=trajlabel,
                    )
                    show_loss_iter.append(loss.item())

                loss = sum(show_loss_iter) / len(show_loss_iter)
                show_loss.append(loss)
                print(f"Epoch [{i}/{epochs}], Validation Loss: {loss:.4f}")
                model_sas_collision.train()

            if i > epochs:
                if loss < 4e-4:
                    break

        show_loss_iter = []
        show_regularization = []

    # model_ts = torch.jit.script(model_sas.cpu())  # Export to TorchScript
    torch.save(model_sas_collision.cpu(), current_dir + train_model)  # Save

########################### TEST ##############################
model_sas = CollisionTrajModel(device)

current_dir = os.getcwd()
train_sas_collision = "/trained_models/sas_model_cc.pt"

model_sas = torch.load(current_dir + train_sas_collision)
model_sas.eval()

model_sas.to(device)
knots = np.array(
    [
        0.0,
        0.0,
        0.0,
        0.0,
        0.29171598,
        0.45266272,
        0.6035503,
        0.76449704,
        0.91538462,
        1.07633136,
        1.22721893,
        1.38816568,
        1.7,
        1.7,
        1.7,
        1.7,
    ]
)


with torch.no_grad():
    ##########################################
    ##########                      ##########
    ##########     Test model sas   ##########
    ##########                      ##########
    ##########################################
    if mode == "test":
        dataset.mode = "test"
        test_loader = DataLoader(copy.deepcopy(dataset), batch_size=1, shuffle=True, collate_fn=dataset.loading)

        optimizer_sas = torch.optim.AdamW(model_sas.parameters(), lr=learning_rate, weight_decay=1e-5)

        ttv = train_test_valid(
            regularizer=Regularizer(model_sas, C=1),
            loss_function=loss_function.compute_collision_loss,
            loss_=loss_function,
            optimizer=optimizer_sas,
        )

        for pc, success, trajlabel in tqdm(test_loader):
            if pc.shape[1] == 0:
                pc = torch.ones((1, 10, 3))
            loss, out, _ = ttv(
                model=model_sas,
                point_cloud=pc,
                success=success,
                trajlabel=trajlabel,
            )

            success = success.numpy()
            pc = pc[0, :, :].cpu().numpy()
            trajlabel = trajlabel.cpu().numpy() * 5
            pc = pc * 5
            # pc = R.apply(pc)

            if success:
                color = (0.0, 1.0, 0.0)
            else:
                color = (1.0, 0.0, 0.0)

            plot = False
            if loss > 0.01:
                print(f"success: {success}, loss:{loss.item():.4f}")
                plot = True

            x_label = trajlabel[0, :10]
            x_label = np.concatenate([x_label, np.array([x_label[-1]] * 3)])
            y_label = trajlabel[0, 10:20]
            y_label = np.concatenate([y_label, np.array([y_label[-1]] * 3)])
            z_label = trajlabel[0, 20:]
            z_label = np.concatenate([z_label, np.array([z_label[-1]] * 3)])

            splinex_l = interpolate.BSpline(knots, x_label, 3, extrapolate=False)
            spliney_l = interpolate.BSpline(knots, y_label, 3, extrapolate=False)
            splinez_l = interpolate.BSpline(knots, z_label, 3, extrapolate=False)

            if plot:
                mlab.figure(fgcolor=(0, 0, 0), bgcolor=(1, 1, 1))
                mlab.points3d(pc[:, 0], pc[:, 1], pc[:, 2], scale_factor=0.05)

                mlab.outline(color=(0, 0, 0), extent=np.array([-5, 5, -5, 5, -5, 5]))

                axes = mlab.axes(color=(0, 0, 0), nb_labels=5)
                axes.title_text_property.color = (0.0, 0.0, 0.0)
                axes.title_text_property.font_family = "times"
                axes.label_text_property.color = (0.0, 0.0, 0.0)
                axes.label_text_property.font_family = "times"
                # mlab.savefig("vector_plot_in_3d.pdf")
                # mlab.gcf().scene.parallel_projection = True  # Source: <<https://stackoverflow.com/a/32531283/2729627>>.

                tt = np.linspace(0, 1.7, 100)
                mlab.orientation_axes()
                mlab.plot3d(splinex_l(tt), spliney_l(tt), splinez_l(tt), color=color, tube_radius=0.05)
                mlab.show()
