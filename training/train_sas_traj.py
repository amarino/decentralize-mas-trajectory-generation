#!/usr/bin/env python3
import torch
from torch.optim.lr_scheduler import CosineAnnealingWarmRestarts, ExponentialLR
from torch.utils.data import DataLoader

import os
from tqdm import tqdm
import matplotlib.pyplot as plt
import sys
import numpy as np
import copy
from mayavi import mlab

from GNNRegularization import GNNRegularization as Regularizer
from MASTrajModel import SASTrajModel, LossFunction
from LoadingDataset import Sampler, SingleTrajDataset

from scipy.interpolate import BSpline
import scipy.interpolate as interpolate


class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("result.txt", "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass


class train_test_valid:
    def __init__(self, regularizer, loss_function, loss_, optimizer) -> None:
        self.regularizer = regularizer
        self.loss_function = loss_function
        self.loss_ = loss_
        self.optimizer = optimizer

    def __call__(self, model, **kwargs):
        point_cloud = kwargs["point_cloud"].to(device)
        A = kwargs["A"].to(device)
        goal = kwargs["goal"].to(device)
        quat = kwargs["quat"].to(device)
        vel = kwargs["vel"].to(device)
        acc = kwargs["acc"].to(device)
        trajlabel = kwargs["trajlabel"].to(device)

        with torch.no_grad():
            self.regularizer.model = model

        self.optimizer.zero_grad()
        out, m3x3, m64x64 = model(point_cloud, goal, quat, vel, acc, A, True)

        self.regularizer.m3x3 = m3x3
        self.regularizer.m64x64 = m64x64

        loss = self.loss_function(out, trajlabel)
        regularization = self.regularizer()
        loss = loss + regularization

        return loss, out, regularization


sys.stdout = Logger()

# Device configuration
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Train folder
train_model = "/trained_models/sas_traj_model_BS.pt"
current_dir = os.getcwd()
os.makedirs(current_dir + "/trained_models", exist_ok=True)
# Hyper-parameters
epochs = 200
batch = 50
random_size = 1
learning_rate = 1e-3
id = 0
mode = "test"

loss_function = LossFunction()
dataset = SingleTrajDataset(current_dir + "/mader/dataset/2023-11-19_15-12-10/", train_num=0.7, valid_num=0.2)
current_dir = os.getcwd()
train_sas_collision = "/trained_models/sas_model_cc.pt"

model_sas_collision = torch.load(current_dir + train_sas_collision)
model_sas_collision.eval()
model_sas_collision = model_sas_collision.to(device)

# torch.autograd.set_detect_anomaly(True)
# constraints = WeightConstraints()

print("Training....")
print("###########  " + mode + "  ###############")

######################   Train    ####################
if mode == "train":
    show_loss = []
    show_loss_iter = []
    loss = 0.0

    model_sas = SASTrajModel(qp_enabled=False, device=device).to(device)

    optimizer = torch.optim.Adam(model_sas.parameters(), lr=learning_rate, weight_decay=1e-9)
    regularizer = Regularizer(model_sas, C=1)

    # regularizer.id3x3 = torch.eye(3).to(device).repeat(batch, 1, 1)
    # regularizer.id64x64 = torch.eye(64).to(device).repeat(batch, 1, 1)

    model_sas.collision_constraint = model_sas_collision

    regularizer.weights = [1e-4]
    regularizer.regularization = [regularizer.point_net]

    ttv = train_test_valid(
        regularizer=regularizer,
        loss_function=loss_function.compute_loss,
        loss_=loss_function,
        optimizer=optimizer,
    )

    dataset.mode = "valid"
    valid_loader = DataLoader(
        copy.deepcopy(dataset),
        batch_size=10,
        shuffle=True,
        collate_fn=dataset.loading,
    )

    dataset.mode = "train"
    train_loader = DataLoader(
        copy.deepcopy(dataset),
        batch_size=batch,
        shuffle=True,
        collate_fn=dataset.loading,
    )

    show_regularization = []

    model_sas.train()
    DEBUG = False
    for i in range(epochs):
        for point_cloud, A, goal, quat, vel, acc, trajlabel in tqdm(train_loader):
            loss, _, regularization = ttv(
                model=model_sas,
                A=A,
                point_cloud=point_cloud,
                goal=goal,
                quat=quat,
                vel=vel,
                acc=acc,
                trajlabel=trajlabel,
            )

            loss.backward()
            ttv.optimizer.step()
            ttv.regularizer.m3x3 = None
            ttv.regularizer.m64x64 = None

            show_loss_iter.append(loss.item())
            show_regularization.append(regularization.item())

        with torch.no_grad():
            if (i + 1) % 1 == 0:
                loss = sum(show_loss_iter) / len(show_loss_iter)
                show_loss.append(loss)
                print(f"Epoch [{i}/{epochs}], Loss: {loss:.4f}")
                regularization = sum(show_regularization) / len(show_regularization)
                print(f"Epoch [{i}/{epochs}], Regularization Loss: {regularization:.4f}")

            ###### validation #######

            if (i + 1) % 10 == 0:
                model_sas.eval()
                show_loss_iter = []
                for point_cloud, A, goal, quat, vel, acc, trajlabel in tqdm(valid_loader):
                    loss, out, _ = ttv(
                        model=model_sas,
                        A=A,
                        point_cloud=point_cloud,
                        goal=goal,
                        quat=quat,
                        vel=vel,
                        acc=acc,
                        trajlabel=trajlabel,
                    )
                    show_loss_iter.append(loss.item())

                if DEBUG:
                    out = out.cpu().numpy()
                    trajlabel = trajlabel.cpu().numpy()
                    for i in range(out.shape[0]):
                        t = model_sas.knots.cpu().numpy()
                        out[i, :] = dataset.de_nromalize(out[i, :])
                        trajlabel[i, :] = dataset.de_nromalize(trajlabel[i, :])

                        cx = out[i, :10]
                        cx = np.concatenate([cx, np.array([cx[-1]] * 3)])
                        cy = out[i, 10:20]
                        cy = np.concatenate([cy, np.array([cy[-1]] * 3)])
                        cz = out[i, 20:]
                        cz = np.concatenate([cz, np.array([cz[-1]] * 3)])

                        x_label = trajlabel[i, :10]
                        x_label = np.concatenate([x_label, np.array([x_label[-1]] * 3)])
                        y_label = trajlabel[i, 10:20]
                        y_label = np.concatenate([y_label, np.array([y_label[-1]] * 3)])
                        z_label = trajlabel[i, 20:]
                        z_label = np.concatenate([z_label, np.array([z_label[-1]] * 3)])

                        splinex = interpolate.BSpline(t, cx, 3, extrapolate=False)
                        spliney = interpolate.BSpline(t, cy, 3, extrapolate=False)
                        splinez = interpolate.BSpline(t, cz, 3, extrapolate=False)

                        splinex_l = interpolate.BSpline(t, x_label, 3, extrapolate=False)
                        spliney_l = interpolate.BSpline(t, y_label, 3, extrapolate=False)
                        splinez_l = interpolate.BSpline(t, z_label, 3, extrapolate=False)

                        tt = np.linspace(0, 1.7, 100)

                        fig = plt.figure()
                        ax = plt.axes(projection="3d")
                        ax.plot3D(splinex(tt), spliney(tt), splinez(tt), "red")
                        ax.plot3D(splinex_l(tt), spliney_l(tt), splinez_l(tt), "green")

                        plt.grid()
                        plt.show()

                loss = sum(show_loss_iter) / len(show_loss_iter)
                show_loss.append(loss)
                print(f"Epoch [{i}/{epochs}], Validation Loss: {loss:.4f}")
                model_sas.train()

            if i > 10:
                model_sas.qp_enabled = False
            if i > epochs:
                if loss < 4e-4:
                    break

        show_loss_iter = []
        show_regularization = []

    model_sas.goal_mean = dataset.goal_mean
    model_sas.goal_std = dataset.goal_std
    model_sas.vel_mean = dataset.vel_mean
    model_sas.vel_std = dataset.vel_std
    # model_ts = torch.jit.script(model_sas.cpu())  # Export to TorchScript
    torch.save(model_sas.cpu(), current_dir + train_model)  # Save

########################### TEST ##############################
model_sas = SASTrajModel(device)

current_dir = os.getcwd()
train_sas = "/trained_models/sas_traj_model_BS.pt"

model_sas = torch.load(current_dir + train_sas)
model_sas.eval()

current_dir = os.getcwd()
train_sas_collision = "/trained_models/sas_model_cc.pt"

model_sas_collision = torch.load(current_dir + train_sas_collision)
model_sas_collision.eval()
model_sas_collision = model_sas_collision.to(device)
model_sas.collision_constraint = model_sas_collision
model_sas.qp_enabled = True
model_sas.use_cc = True

torch.save(model_sas.cpu(), current_dir + train_sas)

model_sas.to(device)
plot = True

with torch.no_grad():
    ##########################################
    ##########                      ##########
    ##########     Test model sas   ##########
    ##########                      ##########
    ##########################################
    if mode == "test":
        dataset.mode = "test"
        test_loader = DataLoader(copy.deepcopy(dataset), batch_size=10, shuffle=True, collate_fn=dataset.loading)

        optimizer_sas = torch.optim.AdamW(model_sas.parameters(), lr=learning_rate, weight_decay=1e-5)

        ttv = train_test_valid(
            regularizer=Regularizer(model_sas, C=1),
            loss_function=loss_function.compute_loss,
            loss_=loss_function,
            optimizer=optimizer_sas,
        )

        for point_cloud, A, goal, quat, vel, acc, trajlabel in tqdm(test_loader):
            loss, out, _ = ttv(
                model=model_sas,
                A=A,
                point_cloud=point_cloud,
                goal=goal,
                quat=quat,
                vel=vel,
                acc=acc,
                trajlabel=trajlabel,
            )

            print(f"loss:{loss.item():.4f}")
            out = out.cpu().numpy()
            trajlabel = trajlabel.cpu().numpy()

            t = model_sas.knots.cpu().numpy()
            out = dataset.de_nromalize(out)
            trajlabel = dataset.de_nromalize(trajlabel)

            for i in range(out.shape[0]):
                cx = out[i, :10]
                cx = np.concatenate([cx, np.array([cx[-1]] * 3), np.array([0.0] * 3)])
                cy = out[i, 10:20]
                cy = np.concatenate([cy, np.array([cy[-1]] * 3), np.array([0.0] * 3)])
                cz = out[i, 20:]
                cz = np.concatenate([cz, np.array([cz[-1]] * 3), np.array([0.0] * 3)])

                x_label = trajlabel[i, :10]
                x_label = np.concatenate([x_label, np.array([x_label[-1]] * 3), np.array([0.0] * 3)])
                y_label = trajlabel[i, 10:20]
                y_label = np.concatenate([y_label, np.array([y_label[-1]] * 3), np.array([0.0] * 3)])
                z_label = trajlabel[i, 20:]
                z_label = np.concatenate([z_label, np.array([z_label[-1]] * 3), np.array([0.0] * 3)])

                print(f"state {x_label[0], y_label[0], z_label[0]}")
                # print(f"label x {x_label}")
                # print(f"label y {y_label}")
                # print(f"label z {z_label}")

                # print(f"pred x {cx}")
                # print(f"pred y {cy}")
                # print(f"pred z {cz}")

                splinex = BSpline(t, cx, 3, extrapolate=False)
                spliney = BSpline(t, cy, 3, extrapolate=False)
                splinez = BSpline(t, cz, 3, extrapolate=False)

                splinex_l = BSpline(t, x_label, 3, extrapolate=False)
                spliney_l = BSpline(t, y_label, 3, extrapolate=False)
                splinez_l = BSpline(t, z_label, 3, extrapolate=False)
                color = (1.0, 0.0, 0.0)

                if plot:
                    pc = point_cloud[i, :, :].cpu().numpy() * 5
                    mlab.figure(fgcolor=(0, 0, 0), bgcolor=(1, 1, 1))
                    mlab.points3d(pc[:, 0], pc[:, 1], pc[:, 2], scale_factor=0.05)

                    mlab.outline(color=(0, 0, 0), extent=np.array([-5, 5, -5, 5, -5, 5]))

                    axes = mlab.axes(color=(0, 0, 0), nb_labels=5)
                    axes.title_text_property.color = (0.0, 0.0, 0.0)
                    axes.title_text_property.font_family = "times"
                    axes.label_text_property.color = (0.0, 0.0, 0.0)
                    axes.label_text_property.font_family = "times"
                    # mlab.savefig("vector_plot_in_3d.pdf")
                    # mlab.gcf().scene.parallel_projection = True  # Source: <<https://stackoverflow.com/a/32531283/2729627>>.

                    tt = np.linspace(0, 1.7, 100)
                    mlab.orientation_axes()

                    mlab.plot3d(splinex_l(tt), spliney_l(tt), splinez_l(tt), color=(1.0, 0.0, 0.0), tube_radius=0.05)
                    mlab.plot3d(splinex(tt), spliney(tt), splinez(tt), color=(0.0, 1.0, 0.0), tube_radius=0.05)

                    mlab.show()

                fig = plt.figure()
                plt.plot(tt, splinex.derivative(1)(tt), label="pred vx")
                plt.plot(tt, spliney.derivative(1)(tt), label="pred vy")
                plt.plot(tt, splinez.derivative(1)(tt), label="pred vz")

                plt.plot(tt, splinex_l.derivative(1)(tt), label=" vx")
                plt.plot(tt, spliney_l.derivative(1)(tt), label=" vy")
                plt.plot(tt, splinez_l.derivative(1)(tt), label=" vz")
                plt.grid()
                plt.legend()
                plt.show()
